@ValidationAPITest
Feature: Testing Validation API

  @NegativeScenariosValidation
  Scenario Outline: Verify for all negative scenarios validate API is working perfectly
    Given Validate the ccAvenue request with "accesscode" and encrypted data with "<mer_id>" "<bc_id>" "<acc_code>" "<agg_id>" "<req_type>" "<bc_param1>" "<bid>" "<pin4>" "<pin6>" "<mobilenumber>"
    When ccAvenue calls "validateAPI" with "get" http request
    Then the API call got success with status code 204 and check "<ErrorCode>" and "<ErrorMessage>"

    Examples: Validation negative cases
      | mer_id               | bc_id    | acc_code     | agg_id | req_type | bc_param1                             | bid   | ErrorCode | ErrorMessage                                                                                     | pin4   | pin6     | mobilenumber |
      |                      | CCA00001 | Lu2IwUj8tXLD | IA06   | T        | Request for validating token number 1 | icici | AG101     | Missing Required Parameters                                                                      |   7777 |   418931 |   9892414099 |
      | CC01IA06AGT000000001 |          | Lu2IwUj8tXLD | IA06   | T        | Request for validating token number 1 | icici | AG101     | Missing Required Parameters                                                                      |   7777 |   418931 |   9892414099 |
      | CC01IA06AGT000000001 | CCA00001 |              | IA06   | T        | Request for validating token number 1 | icici | AG101     | Missing Required Parameters                                                                      |   7777 |   418931 |   9892414099 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD |        | T        | Request for validating token number 1 | icici | AG101     | Missing Required Parameters                                                                      |   7777 |   418931 |   9892414099 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   | T        | Request for validating token number 1 |       | AG101     | Missing Required Parameters                                                                      |   7777 |   418931 |   9892414099 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   |          | Request for validating token number 1 | icici | AG101     | Missing Required Parameters                                                                      |   7777 |   418931 |   9892414099 |
      |                00000 | CCA00001 | Lu2IwUj8tXLD | IA06   | T        | Request for validating token number 1 | icici | AG103     | Incosistent Details                                                                              |   7777 |   418931 |   9892414099 |
