@dispense
Feature: Despense E-ATM API's

  @NegativeScenariosDispense
  Scenario Outline: Verify if all negative scenarios are successfully in despense API
    Given ccAvenue request with "accesscode" and encrypted data with "<mer_id>" "<bc_id>" "<acc_code>" "<agg_id>" "<req_type>" "<bc_param1>" "<bid>" "<original_bc_ref_no>" "<original_eatm_ref_num>"
    When ccAvenue will call "despenseAPI" with "POST" http request
    Then the API call get success with status code 200 and check "<ErrorCode>" and "<ErrorMessage>"

    Examples: Validation
      | mer_id               | bc_id    | acc_code     | agg_id | req_type | bc_param1                             | bid   | ErrorCode | ErrorMessage                | original_bc_ref_no | original_eatm_ref_num |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   | C        | Confirmed                             | icici | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 |          | Lu2IwUj8tXLD | IA06   | C        | Confirmed                             | icici | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 |              | IA06   | C        | Confirmed                             | icici | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD |        | C        | Confirmed                             | icici | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   |          | Confirmed                             | icici | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   | C        | Confirmed                             |       | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   | C        | Confirmed                             | icici | AG101     | Missing Required Parameters |                    |                  5909 |
      | CC01IA06AGT000000001 |    00000 | Lu2IwUj8tXLD | IA06   | C        | Confirmed                             | icici | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   | C        | Confirmed                             | icici | DISP001   | BC Parameters Invalid       | CC62135315         |                       |
      |                      |          |              |        |          |                                       |       | DISP001   | BC Parameters Invalid       |                    |                       |
      |                00000 | CCA00001 | Lu2IwUj8tXLD | IA06   | C        | Confirmed                             | icici | AG103     | Incosistent Details         | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 |         0000 | IA06   | C        | Confirmed                             | icici | AG102     | Inconsistent AccessCode     | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD |   0000 | C        | Confirmed                             | icici | AG107     | Incorrect Access Code.      | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   |     0000 | Confirmed                             | icici | AG110     | Incorrect Request Type.     | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   | C        | Confirmed                             |  0000 | AG201     | Invalid Bank Idnetifier     | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   | C        | Confirmed                             | icici | DISP010   | Could not update the DB     |              00000 |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   | C        | Confirmed                             | icici | DISP010   | Could not update the DB     | CC62135315         |                  0000 |
      |                 0000 |     0000 |         0000 |   0000 |     0000 | Confirmed                             |  0000 | AG102     | Inconsistent AccessCode     |               0000 |                  0000 |
      | CC01IA06AGT0#@000001 | CCA00001 | Lu2IwUj8tXLD | IA06   | C        | Confirmed                             | icici | AG103     | Incosistent Details         | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 | CCA0#$01 | Lu2IwUj8tXLD | IA06   | C        | Confirmed                             | icici | AG112     | Incorrect BCID.             | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2#$Uj8tXLD | IA06   | C        | Confirmed                             | icici | AG102     | Inconsistent AccessCode     | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | I#$6   | C        | Confirmed                             | icici | AG107     | Incorrect Access Code.      | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   | C        | Confirmed                             | i#@ci | AG201     | Invalid Bank Idnetifier     | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   | C        | Confirmed                             | icici | DISP010   | Could not update the DB     | CC62#$5315         |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   | C        | Confirmed                             | icici | DISP001   | BC Parameters Invalid       | CC62135315         | 5#$9                  |
      |                      |          | Lu2IwUj8tXLD | IA06   | C        | Confirmed                             | icici | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      |                      | CCA00001 |              | IA06   | C        | Confirmed                             | icici | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      |                      | CCA00001 | Lu2IwUj8tXLD |        | C        | Confirmed                             | icici | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      |                      | CCA00001 | Lu2IwUj8tXLD | IA06   |          | Confirmed                             | icici | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      |                      | CCA00001 | Lu2IwUj8tXLD | IA06   | C        | Confirmed                             |       | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      |                      | CCA00001 | Lu2IwUj8tXLD | IA06   | C        | Confirmed                             | icici | AG101     | Missing Required Parameters |                    |                  5909 |
      |                      | CCA00001 | Lu2IwUj8tXLD | IA06   | C        | Confirmed                             | icici | DISP001   | BC Parameters Invalid       | CC62135315         |                       |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   | C        |                                  0000 | icici | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   | C        |                                       | icici | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 |          |              | IA06   | C        | Request for validating token number 1 | icici | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 |          | Lu2IwUj8tXLD |        | C        | Request for validating token number 1 | icici | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 |          | Lu2IwUj8tXLD | IA06   |          | Request for validating token number 1 | icici | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 |          | Lu2IwUj8tXLD | IA06   | C        | Request for validating token number 1 |       | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 |          | Lu2IwUj8tXLD | IA06   | C        | Request for validating token number 1 | icici | AG101     | Missing Required Parameters |                    |                  5909 |
      | CC01IA06AGT000000001 |          | Lu2IwUj8tXLD | IA06   | C        | Request for validating token number 1 | icici | DISP001   | BC Parameters Invalid       | CC62135315         |                       |
      | CC01IA06AGT000000001 | CCA00001 |              |        | C        | Request for validating token number 1 | icici | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 |              | IA06   |          | Request for validating token number 1 | icici | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 |              | IA06   | C        | Request for validating token number 1 |       | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 |              | IA06   | C        | Request for validating token number 1 | icici | AG101     | Missing Required Parameters |                    |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 |              | IA06   | C        | Request for validating token number 1 | icici | DISP001   | BC Parameters Invalid       | CC62135315         |                       |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD |        |          | Request for validating token number 1 | icici | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD |        | C        | Request for validating token number 1 |       | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD |        | C        | Request for validating token number 1 | icici | AG101     | Missing Required Parameters |                    |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD |        | C        | Request for validating token number 1 | icici | DISP001   | BC Parameters Invalid       | CC62135315         |                       |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   |          | Request for validating token number 1 |       | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   |          | Request for validating token number 1 | icici | AG101     | Missing Required Parameters |                    |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   |          | Request for validating token number 1 | icici | DISP001   | BC Parameters Invalid       | CC62135315         |                       |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   | C        | Request for validating token number 1 |       | AG101     | Missing Required Parameters |                    |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   | C        | Request for validating token number 1 |       | DISP001   | BC Parameters Invalid       | CC62135315         |                       |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   | C        | Request for validating token number 1 | icici | DISP001   | BC Parameters Invalid       |                    |                       |

  @BCRefNoNullDispense
  Scenario Outline: Verify for all negative scenarios Dispense API is working perfectly with Bc refrence number null
    Given ccAvenue request with "accesscode" and encrypted data with  "<mer_id>" "<bc_id>" "<acc_code>" "<agg_id>" "<bc_ref_num>" "<req_type>" "<bc_param1>" "<bid>" "<original_bc_ref_no>" "<original_eatm_ref_num>"
    When ccAvenue will call "despenseAPI" with "post" http request
    Then the API call get success with status code 200 and check "<ErrorCode>" and "<ErrorMessage>"

    Examples: Null value fro Bc_ref_Num
      | mer_id               | bc_id    | acc_code     | agg_id | bc_ref_num | req_type | bc_param1                             | bid   | ErrorCode | ErrorMessage                | original_bc_ref_no | original_eatm_ref_num |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   |            | C        | Request for validating token number 1 | icici | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   |        000 | C        | Request for validating token number 1 | icici | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 |          | Lu2IwUj8tXLD | IA06   |            | C        | Request for validating token number 1 | icici | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      |                      | CCA00001 |              |        |            |          | Request for validating token number 1 |       | DISP001   | BC Parameters Invalid       |                    |                       |
      | CC01IA06AGT000000001 | CCA00001 |              | IA06   |            | C        | Request for validating token number 1 | icici | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      |                      |          | Lu2IwUj8tXLD |        |            |          | Request for validating token number 1 |       | DISP001   | BC Parameters Invalid       |                    |                       |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD |        |            | C        | Request for validating token number 1 | icici | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      |                      |          |              | IA06   |            |          | Request for validating token number 1 |       | DISP001   | BC Parameters Invalid       |                    |                       |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD |        |            | C        | Request for validating token number 1 | icici | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      |                      |          |              |        |            | C        | Request for validating token number 1 |       | DISP001   | BC Parameters Invalid       |                    |                       |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   |            | C        | Request for validating token number 1 |       | AG101     | Missing Required Parameters | CC62135315         |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   |            | C        | Request for validating token number 1 | icici | AG101     | Missing Required Parameters |                    |                  5909 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   |            | C        | Request for validating token number 1 | icici | DISP001   | BC Parameters Invalid       | CC62135315         |                       |
      |                      |          |              |        |            |          | Request for validating token number 1 | icici | DISP001   | BC Parameters Invalid       |                    |                       |
      |                      |          |              |        |            |          | Request for validating token number 1 |       | DISP001   | BC Parameters Invalid       | CC62135315         |                       |
      |                      |          |              |        |            |          | Request for validating token number 1 |       | AG101     | Missing Required Parameters |                    |                  5909 |
      |                      |          |              |        | CC01910244 |          | Request for validating token number 1 |       | DISP001   | BC Parameters Invalid       |                    |                       |

  @InvalidEncryptedDataDispense
  Scenario Outline: Verify for all negative scenarios validate API is working perfectly with invalid encrypted data in query parameter
    Given ccAvenue request with "<acc_code>" and encrypted data with "<enc_data>"
    When ccAvenue will call "despenseAPI" with "POST" http request
    Then the API call get success with status code 200 and check "<ErrorCode>" and "<ErrorMessage>"

    Examples: Null value fro Bc_ref_Num
      | acc_code     | enc_data                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       | ErrorCode | ErrorMessage          |
      | Lu2IwUj8tXLD |                                                                                                                                                                                                                                                                                                                                                        000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 | DISP001   | BC Parameters Invalid |
      | Lu2IwUj8tXLD | 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 | DISP001   | BC Parameters Invalid |
      | Lu2IwUj8tXLD |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | DISP001   | BC Parameters Invalid |

 # @InvalidAccessCodeDispense
  #Scenario Outline: Verify for all negative scenarios validate API is working perfectly with invalid access code in query parameter
   # Given ccAvenue request with invalid "<access_code>" and encrypted data with "<mer_id>" "<bc_id>" "<acc_code>" "<agg_id>" "<req_type>" "<bc_param1>" "<bid>" "<original_bc_ref_no>" "<original_eatm_ref_num>"
    #When ccAvenue will call "despenseAPI" with "POST" http request
    #Then the API call get success with status code 200 and response "null"

    #Examples: Invalid access code
     # | mer_id               | bc_id    | acc_code     | agg_id | req_type | bc_param1                             | bid   | ErrorCode | ErrorMessage  | original_bc_ref_no | original_eatm_ref_num | access_code |
      #| CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   | C        | Request for validating token number 1 | icici | AG110     | Data mismatch | CC62135315         |                  5909 |             |
      #| CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   | C        | Request for validating token number 1 | icici | AG110     | Data mismatch | CC62135315         |                  5909 |   111111111 |
