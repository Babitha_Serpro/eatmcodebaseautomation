@positiveScenario
Feature: Testing Validation Positgive API

  @PositiveScenariosValidation
  Scenario Outline: Verify for all positive scenarios validate API is working perfectly
    Given Validate the ccAvenue positive request with "accesscode" and encrypted data with "<mer_id>" "<bc_id>" "<acc_code>" "<agg_id>" "<req_type>" "<bc_param1>" "<bid>"
    When ccAvenue calls "validateAPI" with "get" http request
    Then the API call got success with status code 204 and check "<ErrorCode>" and "<ErrorMessage>"

    Examples: Validation
      | mer_id               | bc_id    | acc_code     | agg_id | req_type | bc_param1                             | bid   | ErrorCode | ErrorMessage |
     # | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   | T        | Request for validating token number 1 | icici | null      | null         |

  @PositiveScenariosReversal
  Scenario Outline: Verify for all positive scenarios reversal API is working perfectly
    Given Reverse the ccAvenue positive request with "accesscode" and encrypted data with "<mer_id>" "<bc_id>" "<acc_code>" "<agg_id>" "<req_type>" "<bc_param1>" "<bid>" "<original_bc_ref_no>" "<original_req_timestamp>" "<rev_reason_cd>" "<rev_reason>"
    When ccAvenue calls "reversalAPI" with "get" http request
    Then the API call got success with status code 204 and check "<ErrorCode>" and "<ErrorMessage>"

    Examples: Reversal
      | mer_id               | bc_id    | acc_code     | agg_id | req_type | bc_param1                             | bid   | ErrorCode | ErrorMessage | original_bc_ref_no | original_req_timestamp | rev_reason_cd | rev_reason   |
     # | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   | R        | Request for validation token number 1 | icici | null      | null         |           12345678 |         20200628234800 | O             | Money is not |

  @PositiveScenariosDispense
  Scenario Outline: Verify for all positive scenarios dispense API is working perfectly
    Given ccAvenue positive request with "accesscode" and encrypted data with  "<mer_id>" "<bc_id>" "<acc_code>" "<agg_id>" "<bc_ref_num>" "<req_type>" "<bc_param1>" "<bid>" "<original_bc_ref_no>" "<original_eatm_ref_num>"
    When ccAvenue will call "despenseAPI" with "post" http request
    Then the API call get success with status code 200 and check "<ErrorCode>" and "<ErrorMessage>"

    Examples: Dispense
      | mer_id               | bc_id    | acc_code     | agg_id | bc_ref_num | req_type | bc_param1                             | bid   | ErrorCode | ErrorMessage                | original_bc_ref_no | original_eatm_ref_num |
      #| CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   |            | C        | Request for validating token number 1 | icici | AG101     | Missing Required Parameters |                    |                       |
