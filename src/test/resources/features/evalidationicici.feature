@ValidationAPIICITest
Feature: Testing Validation with ICICI API 

  @NegativeScenariosICValidation
  Scenario Outline: Verify for all negative scenarios from validate API gives output from icici end
    Given Validate the ccAvenue request with "accesscode" and encrypted data with "<mer_id>" "<bc_id>" "<acc_code>" "<agg_id>" "<req_type>" "<bc_param1>" "<bid>" "<pin4>" "<pin6>" "<mobilenumber>"
    When ccAvenue calls "validateAPI" with "get" http request
    Then the API call got success with status code 204 and check "<ErrorCode>" and "<ErrorMessage>"
     
 Examples: Validation negative cases
      | mer_id               | bc_id    | acc_code     | agg_id | req_type | bc_param1                             | bid   | ErrorCode | ErrorMessage                                                                                     | pin4   | pin6     | mobilenumber |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   | T        | Request for validating token number 1 | icici | AC71      | Data mismatch i.e pin4/pin6/mobile number during transaction or Record Not found during reversal |   0000 |   418931 |   9892414099 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   | T        | Request for validating token number 1 | icici | AC71      | Data mismatch i.e pin4/pin6/mobile number during transaction or Record Not found during reversal |   7777 |   000000 |   9892414099 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   | T        | Request for validating token number 1 | icici | AC71      | Data mismatch i.e pin4/pin6/mobile number during transaction or Record Not found during reversal |   7777 |   418931 |   0000000000 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   | T        |                                  0000 | icici | AC71      | Data mismatch i.e pin4/pin6/mobile number during transaction or Record Not found during reversal |   1234 |   995855 |   7032953510 |
      | CC01IA06AGT000000001 | CCA00001 | Lu2IwUj8tXLD | IA06   | T        |                                       | icici | AC71      | Data mismatch i.e pin4/pin6/mobile number during transaction or Record Not found during reversal |   1234 |   995855 |   7032953510 |
     