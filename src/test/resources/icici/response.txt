<XML>
	<MessageType MXL="4" MNL="4" PL="4" DT="numeric" LSP="0" PRE="mandatory"  DEFAULT="1210">1210</MessageType>
	<ProcCode MXL="6" MNL="6" DT="numeric" PL="6" LSP="0" IND="3" PRE="mandatory"  DEFAULT="011000">920000</ProcCode>
	<Amount MXL="12" MNL="12" DT="numeric" PL="12" LSP="0" IND="4"  PRE="mandatory">000000200000</Amount>
	<SystemTraceNumber MXL="6" MNL="6" DT="numeric" PL="6" LSP="0" IND="11"  PRE="mandatory">031770</SystemTraceNumber>
	<DateTime MXL="12" MNL="12" DT="numeric" PL="12" LSP="0" IND="12"   PRE="mandatory">200329120029</DateTime>
	<RRNNumber MXL="12" MNL="12" DT="numeric" PL="12" LSP="0" IND="37"  PRE="mandatory">6716</RRNNumber>
	<ActCode MXL="3" MNL="3" DT="numeric" PL="3" LSP="0" IND="39"  PRE="mandatory">000</ActCode>
	<TerminalId MXL="16" MNL="16" DT="alphabet" PL="16" LSP="0" IND="41"  PRE="mandatory">SACWF587</TerminalId>
	<MobileNo IND="102" LSP="2" MNL="1" MXL="99" PL="10" PRE="mandatory">9472568968</MobileNo>
	<Pin>500376|9472</Pin>
	<ChannelId MXL="999" MNL="1" DT="alphabet" PL="3" LSP="3" IND="123"  PRE="mandatory">CLT</ChannelId>
	<Reserved MXL="999" MNL="1" DT="alphabet" PL="350" LSP="3" IND="126" PRE="optional"/>
</XML>
