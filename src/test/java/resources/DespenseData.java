package resources;

import java.util.ArrayList;
import org.json.simple.JSONObject;

import com.ccavenue.security.AesCryptUtil;


public class DespenseData {
	private static org.apache.log4j.Logger logger = org.apache.log4j.LogManager.getLogger(DespenseData.class);
	String code = "Lu2IwUj8tXLD";
	String bc_ref_Number = Utils.bc_ref_no();

	public JSONObject addData(String access_code, String mer_id, String bc_id, String acc_code, String agg_id,String bc_ref_Number, String req_type, String bc_param1, String
			  bid, String original_bc_ref_no, String original_eatm_ref_num) { 

				  JSONObject jsonObject = new JSONObject();

			  access_code = this.code;
			  
			  String text = "mer_id=" + mer_id + "&bc_id=" + bc_id + "&acc_code=" +
			  acc_code + "&agg_id=" + agg_id + "&req_type=" + req_type + "&bc_ref_no=" +
			  bc_ref_Number + "&bc_param1=" + bc_param1 + "&bid=" + bid +
			  "&original_bc_ref_no=" + original_bc_ref_no + "&original_eatm_ref_num=" +
			  original_eatm_ref_num; 
			  logger.debug("Text------>" + text); 
			  AesCryptUtil aesUtil = new AesCryptUtil("SkMwAv4YwUpbQvHr");
			  String enc =  aesUtil.encrypt(text); 

			  logger.debug("EncryptedData---->" + enc);
			 // System.out.println("EncryptedData---->" + enc);

			  
		      jsonObject.put("enc_req", enc);
			  jsonObject.put("acc_code", access_code);
			  

			  logger.debug("jsonObject.toJSONString()---->" +   jsonObject);


			  return jsonObject;
			  
			  }
	
	
	  public String decrptData(String s1) { 
		  AesCryptUtil aesUtil = new AesCryptUtil("uruI4MqsDLlFGyZC"); 
		  String text = s1; 
		  String dec =aesUtil.decrypt(text);
		  return dec;
	 
	 }
	 
}
