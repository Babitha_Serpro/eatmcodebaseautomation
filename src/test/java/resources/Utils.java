package resources;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import com.eatm.bean.ValidateBean;
import com.eatm.dao.ValidateDao;


import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.specification.RequestSpecification;

public class Utils {
	private static org.apache.log4j.Logger logger = org.apache.log4j.LogManager.getLogger(Utils.class);
public static RequestSpecification req;
public List<String> bc_ref_no_list = new ArrayList<String>();

	public RequestSpecification requestSpecification() throws Exception {
		
		if(req==null) {
		PrintStream log = new PrintStream(new FileOutputStream("logging.txt"));
		
		req = new RequestSpecBuilder().setBaseUri(getGlobalValue("baseUrl"))
				.addFilter(RequestLoggingFilter.logRequestTo(log))
				.addFilter(ResponseLoggingFilter.logResponseTo(log))
				.build();
		return req;
		}
		return req;
		
	}
	
	public static String  getGlobalValue(String key) throws Exception {
		
		Properties pro = new Properties();
		FileInputStream fis = new FileInputStream("src\\test\\java\\resources\\global.properties");
		pro.load(fis);
		return pro.getProperty(key);
		
	}
	
	public static char num[] = { '0', '1', '2', '3', '4', '5','8','6','7','9' };
	   public static char randomNum() {
	      return num[(int) Math.floor(Math.random() * 10)];
	   } 
	   
	   
	   public static String bc_ref_no()  {
			StringBuilder bc_ref_num = null;

			int len = 6;
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.append("CC");
			for (int i = 0; i < len; i++) {
				bc_ref_num = strBuilder.append(randomNum());
			}
			List<ValidateBean> bc_ref_List = null;
			
				bc_ref_List = ValidateDao.getBcRefNumber(bc_ref_num);
			
			int count = bc_ref_List.size();
			for (int k = 0; k < count; k++) {
				//logger.debug("----------------------->k"+k);
				strBuilder.append("CC");
				for (int i = 0; i < len; i++) {
					bc_ref_num = strBuilder.append(randomNum());
					//logger.debug("Inside--->---->"+i);
				}
				//logger.debug("Inside-->"+bc_ref_num.toString());
				return bc_ref_num.toString();
			}
			//logger.debug("Direct---->"+bc_ref_num.toString());
			return bc_ref_num.toString();
		} 
}
