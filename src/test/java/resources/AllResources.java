package resources;

public enum AllResources {

		
	validateAPI("/token/Validate"),
	despenseAPI("/token/Dispensed"),
	reversalAPI("/token/Reverse");
	
	
	private String resource;

	AllResources(String resource){
		this.resource=resource;
	}
	public String getResource() {
		return resource;
	}

	
}
