package com.eatm.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.eatm.bean.ValidateBean;
import resources.DBConnectivity;

public class ReversalDao{
	private static org.apache.log4j.Logger logger = org.apache.log4j.LogManager.getLogger(ReversalDao.class);
	static Connection connection;
	static PreparedStatement ps;
	

	public static ValidateBean getResult(String bcNo, String pin4, String pin6, String mobNo)  {
		ValidateBean vb = new ValidateBean();
		
		String sql ="";

		if (bcNo.equals("")) {
					
			sql = " SELECT * FROM OB.TOKEN_VALIDATION_RESP WHERE request_id IN (SELECT MAX(REQUEST_ID) FROM OB.VALIDATION_REVERSAL_REQ \n";

			if (pin4.length() != 0 && pin6.length() == 0) {

				sql = sql + "where PIN_4 = '" + pin4 + "' \r\n";

			}else if (pin6.length() != 0 && pin4.length() == 0) {
				
				sql = sql + " where PIN_6 = '" + pin6 + "'\r\n";
			} else if(pin4.length() != 0 && pin6.length() != 0) {
				
				sql = sql + "where PIN_4 = '" + pin4 + "' \r\n"
			          +" AND PIN_6 = '" + pin6 + "' \r\n";
			}

			sql = sql + ")AND REQUEST_TYPE = 'R'";
			
		}  else {
			sql = "SELECT  * FROM TOKEN_VALIDATION_RESP WHERE REQUEST_ID IN (SELECT REQUEST_ID AS EATM_REFERENCE_NUMBER FROM OB.VALIDATION_REVERSAL_REQ WHERE\r\n"
					+ "BC_REF_NUM = '" + bcNo + "'\r\n";

			if ((pin4.equals("") && pin6.equals("")) || (pin4.equals("") && pin6.equals("") && mobNo.equals(""))
					|| (pin4.equals("") && mobNo.equals("")) || (pin6.equals("") && mobNo.equals(""))
					|| (pin4.equals("")) || (mobNo.equals("")) || (pin6.equals(""))) {
				sql = sql 
						+ ")AND REQUEST_TYPE = 'R';";

			} else {
				sql = sql 
						+ "AND PIN_4 = '" + pin4 + "'\r\n" 
						+ "AND PIN_6 = '" + pin6 + "')\r\n"
						+ "AND REQUEST_TYPE = 'R';";
			}
		}
		logger.debug(sql);
		 connection = DBConnectivity.getConnection();
		 try {
			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {

				vb.setResponse_id(rs.getInt("RESPONSE_ID"));
				vb.setRequest_id(rs.getInt("REQUEST_ID"));
				vb.setErr_code(rs.getString("ERROR_CODE"));
				vb.setError_message(rs.getString("ERROR_MESSAGE"));
				vb.setVal_success(rs.getInt("VALIDATION_SUCCESS"));
				vb.setAmount(rs.getDouble("AMOUNT"));
				
				
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			  if(ps != null) {
				  try {
					ps.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			  }
			  if(connection != null)
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			 }
		
		
		
		return vb;
		
		
	}
}
