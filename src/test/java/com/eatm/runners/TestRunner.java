package com.eatm.runners;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = "json:target/jsonReports/cucumber-report.json",
//         features = "src/test/resources/features", 
//			features= {"src/test/resources/features/avalidation.feature"},
         features= {"src/test/resources/features/avalidation.feature","src/test/resources/features/breversal.feature","src/test/resources/features/dispense.feature"},
         glue = {"com.eatm.stepDefinitions" }, 
         monochrome = true,
         strict = true
     
         // tags="@ValidationAPITest"
	    //	tags="@ReversalAPITesting"
	    //  tags = "@dispense"
        //  tags= "@ReversalAPIICITesting"
        // tags="@ValidationAPIICITest"
		// tags = "@NegativeReversalScenarios"
        // tags=" @BCRefNoNullreversal"
        // tags="@BCRefNoNullValidation"
)
public class TestRunner {

}
