package com.eatm.stepDefinitions;

import com.ccavenue.security.AesCryptUtil;
import com.eatm.bean.ValidateBean;
import com.eatm.dao.ValidateDao;
import resources.AllResources;
import resources.Utils;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;


import static io.restassured.RestAssured.given;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.builder.ResponseSpecBuilder;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class ValidateDefinition extends Utils {

	private static org.apache.log4j.Logger logger = org.apache.log4j.LogManager.getLogger(ValidateDefinition.class);
	
	ResponseSpecification resspec;
	RequestSpecification request;
	Response response;
	String access_code = "Lu2IwUj8tXLD";
	String bc_ref_num = bc_ref_no();
	
	String Pin4 = null;
	String Pin6 = null;
	String MobNumber = null;
    static String validateBc_ref_num=null;
    ValidateDao dao = new ValidateDao();
   
    
  //Negative Scenario validation
  	@Given("Validate the ccAvenue request with {string} and encrypted data with {string} {string} {string} {string} {string} {string} {string} {string} {string} {string}")
  	public void validate_the_ccAvenue_request_with_and_encrypted_data_with(String accessCode, String merid, String bcid,
  			String acccode, String aggid, String reqtype, String bcparam1, String bid, String pin4, String pin6,
  			String mobilenumber) throws Exception {

  		accessCode = this.access_code;
  		Pin4 = pin4;
  		Pin6 = pin6;
  		MobNumber = mobilenumber;
  		
  		
  		String text = "mer_id=" + merid + "&bc_id=" + bcid + "&acc_code=" + acccode + "&agg_id=" + aggid + "&req_type="
  				+ reqtype + "&bc_ref_no=" + bc_ref_num + "&bc_param1=" + bcparam1 + "&bid=" + bid + "&pin4=" + pin4
  				+ "&pin6=" + pin6 + "&mnum=" + mobilenumber;
  		logger.debug("Given text----->"+text);
  		AesCryptUtil aesUtil = new AesCryptUtil("SkMwAv4YwUpbQvHr");
  		String enc = aesUtil.encrypt(text);
  		logger.debug("Access Code--->" + acccode);
  		logger.debug("Encrypted Data---->" + enc);
  		request = given().spec(requestSpecification()).queryParam("acc_code", accessCode).queryParam("enc_req", enc);

  	}
	//BC reference number null validation
	@Given("Validate the ccAvenue request with {string} and encrypted data with {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string}")
	public void validate_the_ccAvenue_request_with_and_encrypted_data_with(String accessCode, String merid, String bcid,
			String acccode, String aggid, String bc_ref_Number,String reqtype, String bcparam1, String bid,String pin4,String pin6,String mobilenumber) throws Exception {
		accessCode = this.access_code;
		Pin4 = pin4;
		Pin6 = pin6;
		MobNumber = mobilenumber;
		bc_ref_num=bc_ref_Number;
		System.out.println("Bc ref number :" +bc_ref_Number);
		String text = "mer_id=" + merid + "&bc_id=" + bcid + "&acc_code=" + acccode + "&agg_id=" + aggid + "&req_type="
				+ reqtype + "&bc_ref_no=" + bc_ref_Number + "&bc_param1=" + bcparam1 + "&bid=" + bid + "&pin4=" + pin4
				+ "&pin6=" + pin6 + "&mnum=" + mobilenumber;
		logger.debug("Given text----->"+text);
		AesCryptUtil aesUtil = new AesCryptUtil("SkMwAv4YwUpbQvHr");
		String enc = aesUtil.encrypt(text);
		logger.debug("Access Code--->" + acccode);
		logger.debug("Encrypted Data---->" + enc);
		request = given().spec(requestSpecification()).queryParam("acc_code", accessCode).queryParam("enc_req", enc);
		
	   
	}

	//Invalid encrypted data
	@Given("Validate the ccAvenue request with {string} and encrypted data with {string}")
	public void validate_the_ccAvenue_request_with_and_encrypted_data_with(String acc_code, String enc) throws Exception {
		logger.debug("Access Code--->" + acc_code);
		logger.debug("Encrypted Data---->" + enc);
		request = given().spec(requestSpecification()).queryParam("acc_code", acc_code).queryParam("enc_req", enc);

	}
	
	//invalid enc data
		@Then("API call got success with status code {int} and check {string} and {string} for invalid enctyptedData")
		public void api_call_got_success_with_status_code_and_check_and_for_invalid_enctyptedData(Integer int1, String expected_errorCode, String expected_errorMsg) throws Exception {
			ValidateBean result = new ValidateBean();
			logger.debug("expected_errorCode----->" + expected_errorCode);

			bc_ref_num="";
			Pin4="";
			Pin6="";
			MobNumber="";
			logger.debug(response.getStatusCode());
			if (response.getStatusCode() == 204) {
				TimeUnit.SECONDS.sleep(2);
				result = ValidateDao.getResult(bc_ref_num, Pin4, Pin6, MobNumber);				
				logger.debug("Validation Database ErrorCode ---> "+result.getErr_code());
				logger.debug("Validation Database ErrorMessage--> "+ result.getError_message());
				assertEquals(expected_errorCode, result.getErr_code());
				assertEquals(expected_errorMsg, result.getError_message().trim());
				if(expected_errorCode!=result.getErr_code()){
					logger.debug("Error Message is: " +result.getError_message().trim());
				}
			}
			assertEquals(response.getStatusCode(), 204);
		}
	
	@When("ccAvenue calls {string} with {string} http request")
	public void ccavenue_calls_with_http_request(String resource, String method) {

		AllResources resourceAPI = AllResources.valueOf(resource);
		resspec = new ResponseSpecBuilder().expectStatusCode(204).build();
		if (method.equalsIgnoreCase("POST")) {
			response = request.when().post(resourceAPI.getResource());
		} else if (method.equalsIgnoreCase("GET")) {
			response = request.when().get(resourceAPI.getResource());
		} else if (method.equalsIgnoreCase("DELETE")) {
			response = request.when().delete(resourceAPI.getResource());
		} else if (method.equalsIgnoreCase("PUT")) {
			response = request.when().put(resourceAPI.getResource());
		}
	}

	@Then("the API call got success with status code {int} and check {string} and {string}")
	public void the_API_call_got_success_with_status_code_and_check_and(Integer int1, String expected_errorCode,
			String expected_errorMsg) throws Exception {
		ValidateBean result = new ValidateBean();
		logger.debug("expected_errorCode----->" + expected_errorCode);

		
		logger.debug("Status code-->"+response.getStatusCode());
		if (response.getStatusCode() == 204) {
			TimeUnit.SECONDS.sleep(2);
			result = ValidateDao.getResult(bc_ref_num, Pin4, Pin6, MobNumber);			
			logger.debug("Validation Database ErrorCode ---> "+result.getErr_code());
			logger.debug("Validation Database ErrorMessage--> "+ result.getError_message());
			assertEquals(expected_errorCode, result.getErr_code());
			assertEquals(expected_errorMsg, result.getError_message().trim());

		}else {
		assertEquals(response.getStatusCode(), 204);
		}
	}
	

	//Positive Scenario validate api
			@Given("Validate the ccAvenue positive request with {string} and encrypted data with {string} {string} {string} {string} {string} {string} {string}")
			public void validate_the_ccAvenue_positive_request_with_and_encrypted_data_with(String accessCode, String merid, String bcid,
					String acccode, String aggid, String reqtype, String bcparam1, String bid) throws Exception {
				ValidateBean bc_num_bean =  new ValidateBean();	
				accessCode = this.access_code;
				String pin4 = getGlobalValue("pin4");
				String pin6 = getGlobalValue("pin6");
				String MobNo = getGlobalValue("mnum");
				Pin4 = pin4;
				Pin6 = pin6;
				MobNumber = MobNo;
				validateBc_ref_num=bc_ref_num;
				bc_num_bean.setBc_ref_no(validateBc_ref_num);
				
				logger.debug("pin4="+pin4);
				logger.debug("pin6="+pin6);
				logger.debug("mnum="+MobNo);
				String text = "mer_id=" + merid + "&bc_id=" + bcid + "&acc_code=" + acccode + "&agg_id=" + aggid + "&req_type="
						+ reqtype + "&bc_ref_no=" + bc_ref_num + "&bc_param1=" + bcparam1 + "&bid=" + bid + "&pin4=" + pin4
						+ "&pin6=" + pin6 + "&mnum=" + MobNo;
				
				logger.debug("Given text----->"+text);
				AesCryptUtil aesUtil = new AesCryptUtil("SkMwAv4YwUpbQvHr");
				String enc = aesUtil.encrypt(text);
				logger.debug("Access Code--->" + acccode);
				logger.debug("Encrypted Data---->" + enc);
				request = given().spec(requestSpecification()).queryParam("acc_code", accessCode).queryParam("enc_req", enc);
				
			}
			

			//Positive scenario reversal api
	@Given("Reverse the ccAvenue positive request with {string} and encrypted data with {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string}")
	public void reverse_the_ccAvenue_positive_request_with_and_encrypted_data_with(String accesscode,String merid, String bcid, String acccode, String aggid, String reqtype, 
			String bcparam1, String bid, String originalbcrefno, String originalreqtimestamp, String revreasoncd, String revreason) throws Exception {
			String pin4 = getGlobalValue("pin4");
			String pin6 = getGlobalValue("pin6");
			String MobNo = getGlobalValue("mnum");
			Pin4 = pin4;
			Pin6 = pin6;
			MobNumber = MobNo;
			String text = "mer_id=" + merid + "&bc_id=" + bcid + "&acc_code=" + acccode + "&agg_id=" + aggid + "&req_type="
				+ reqtype + "&bc_ref_no=" + bc_ref_num + "&bc_param1=" + bcparam1 + "&bid=" + bid + "&pin4=" + pin4
				+ "&pin6=" + pin6 + "&mnum=" + MobNo + "&original_bc_ref_no=" +validateBc_ref_num + "&original_req_timestamp=" +originalreqtimestamp + "&rev_reason_cd=" +revreasoncd + "&rev_reason=" +revreason;
		
			logger.debug("Given text----->"+text);
			AesCryptUtil aesUtil = new AesCryptUtil("SkMwAv4YwUpbQvHr");
		
		
			String enc = aesUtil.encrypt(text);
			logger.debug("Access Code--->" + acccode);
			logger.debug("Encrypted Data---->" + enc);
			request = given().spec(requestSpecification()).queryParam("acc_code", access_code).queryParam("enc_req", enc);
		}
	
	
	//invalid access code validation
	@Given("Validate the ccAvenue request with invalid {string} and encrypted data with {string} {string} {string} {string} {string} {string} {string} {string} {string} {string}")
	public void validate_the_ccAvenue_request_with_invalid_and_encrypted_data_with(String accessCode, String merid, String bcid,
  			String acccode, String aggid, String reqtype, String bcparam1, String bid, String pin4, String pin6,
  			String mobilenumber) throws Exception {
		//logger.debug("Access code-->" +accessCode);
		Pin4 = pin4;
  		Pin6 = pin6;
  		MobNumber = mobilenumber;
  		
  		
  		String text = "mer_id=" + merid + "&bc_id=" + bcid + "&acc_code=" + acccode + "&agg_id=" + aggid + "&req_type="
  				+ reqtype + "&bc_ref_no=" + bc_ref_num + "&bc_param1=" + bcparam1 + "&bid=" + bid + "&pin4=" + pin4
  				+ "&pin6=" + pin6 + "&mnum=" + mobilenumber;
  		logger.debug("Given text----->"+text);
  		AesCryptUtil aesUtil = new AesCryptUtil("SkMwAv4YwUpbQvHr");
  		String enc = aesUtil.encrypt(text);
  		logger.debug("Access Code--->" + accessCode);
  		logger.debug("Encrypted Data---->" + enc);
  		request = given().spec(requestSpecification()).queryParam("acc_code", accessCode).queryParam("enc_req", enc);

	}
	

}
