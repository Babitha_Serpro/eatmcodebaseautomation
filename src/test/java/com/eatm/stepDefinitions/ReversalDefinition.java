package com.eatm.stepDefinitions;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;


import com.ccavenue.security.AesCryptUtil;
import com.eatm.bean.ValidateBean;
import com.eatm.dao.ReversalDao;
import com.eatm.dao.ValidateDao;
import resources.AllResources;
import resources.Utils;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class ReversalDefinition extends Utils{
	private static org.apache.log4j.Logger logger = org.apache.log4j.LogManager.getLogger(ReversalDefinition.class);
	
	ResponseSpecification resspec;
	RequestSpecification request;
	Response response;
	String access_code = "Lu2IwUj8tXLD";
	String bc_ref_num = bc_ref_no();

	String Pin4 = null;
	String Pin6 = null;
	String MobNumber = null;
		
		@Given("Validate the ccAvenue request with {string} and encrypted data with {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string}")
		public void validate_the_ccAvenue_request_with_and_encrypted_data_with(String accessCode,String merid, String bcid, String acccode, String aggid, String reqtype, String bcparam1, String bid, String pin4, String pin6, String mobilenumber, String originalbcrefno, String originaleatmrefnum, String originalreqtimestamp, String revreasoncd, String revreason) throws Exception {
		    
		accessCode = this.access_code;
		Pin4 = pin4;
		Pin6 = pin6;
		MobNumber = mobilenumber;
		
		String text = "mer_id=" + merid + "&bc_id=" + bcid + "&acc_code=" + acccode + "&agg_id=" + aggid + "&req_type="
				+ reqtype + "&bc_ref_no=" + bc_ref_num + "&bc_param1=" + bcparam1 + "&bid=" + bid + "&pin4=" + pin4
				+ "&pin6=" + pin6 + "&mnum=" + mobilenumber + "&original_bc_ref_no=" +originalbcrefno + "&original_eatm_ref_num=" +originaleatmrefnum+ "&original_req_timestamp=" +originalreqtimestamp + "&rev_reason_cd=" +revreasoncd + "&rev_reason=" +revreason;
		
		logger.debug("text---->"+text);
		AesCryptUtil aesUtil = new AesCryptUtil("SkMwAv4YwUpbQvHr");
		String enc = aesUtil.encrypt(text);
		logger.debug("Access Code--->" + acccode);
		logger.debug("Encrypted Data---->" + enc);
		request = given().spec(requestSpecification()).queryParam("acc_code", accessCode).queryParam("enc_req", enc);
	}
	
	@When("ccAvenue calls {string} with {string} http request to do reversal")
	public void ccavenue_calls_with_http_request_to_do_reversal(String resource, String method) {
	    // Write code here that turns the phrase above into concrete actions
		AllResources resourceAPI = AllResources.valueOf(resource);
		resspec = new ResponseSpecBuilder().expectStatusCode(204).build();
		if (method.equalsIgnoreCase("POST")) {
			response = request.when().post(resourceAPI.getResource());
		} else if (method.equalsIgnoreCase("GET")) {
			response = request.when().get(resourceAPI.getResource());
		} else if (method.equalsIgnoreCase("DELETE")) {
			response = request.when().delete(resourceAPI.getResource());
		} else if (method.equalsIgnoreCase("PUT")) {
			response = request.when().put(resourceAPI.getResource());
		}
	}
	
	
	@Then("the ReversalAPI call got success with status code {int} and check {string} and {string}")
	public void the_ReversalAPI_call_got_success_with_status_code_and_check_and(Integer int1, String expected_errorCode, String expected_errorMessage) throws Exception {
		ValidateBean result = new ValidateBean();
		logger.debug("expected_errorCode----->" + expected_errorCode);

		logger.debug(response.getStatusCode());
		if (response.getStatusCode() == 204) {
			TimeUnit.SECONDS.sleep(2);
			result = ReversalDao.getResult(bc_ref_num, Pin4, Pin6, MobNumber);
			logger.debug("Reversal Eror code:-->"+result.getErr_code());
			logger.debug("Reversal Eror msg:-->"+result.getError_message());
			
			assertEquals(expected_errorCode, result.getErr_code());
			assertEquals(expected_errorMessage, result.getError_message().trim());
			

		}
		assertEquals(response.getStatusCode(), 204);
	}
	
	
	@Given("Validate the ccAvenue reversal request with {string} and encrypted data with {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string}")
	public void validate_the_ccAvenue_reversal_request_with_and_encrypted_data_with(String accessCode,String merid, String bcid, String acccode, String aggid,String bcrefno, String reqtype, String bcparam1, String bid, String pin4, String pin6, String mobilenumber, String originalbcrefno, String originaleatmrefnum, String originalreqtimestamp, String revreasoncd, String revreason) throws Exception {
		accessCode = this.access_code;
		bc_ref_num="";
		Pin4 = pin4;
		Pin6 = pin6;
		MobNumber = mobilenumber;
		
		String text = "mer_id=" + merid + "&bc_id=" + bcid + "&acc_code=" + acccode + "&agg_id=" + aggid + "&bc_ref_num=" +bcrefno+ "&req_type="
				+ reqtype + "&bc_ref_no=" + bc_ref_num + "&bc_param1=" + bcparam1 + "&bid=" + bid + "&pin4=" + pin4
				+ "&pin6=" + pin6 + "&mnum=" + mobilenumber + "&original_bc_ref_no=" +originalbcrefno + "&original_eatm_ref_num=" +originaleatmrefnum+ "&original_req_timestamp=" +originalreqtimestamp + "&rev_reason_cd=" +revreasoncd + "&rev_reason=" +revreason;
		
		logger.debug("text---->"+text);
		AesCryptUtil aesUtil = new AesCryptUtil("SkMwAv4YwUpbQvHr");
		String enc = aesUtil.encrypt(text);
		logger.debug("Access Code--->" + acccode);
		logger.debug("Encrypted Data---->" + enc);
		request = given().spec(requestSpecification()).queryParam("acc_code", accessCode).queryParam("enc_req", enc);
	}
	
	//invaid encrypted data
		@Given("Validate the ccAvenue request with {string} and encrypted data with {string} for reversal")
		public void validate_the_ccAvenue_request_with_and_encrypted_data_with_for_reversal(String acc_code, String enc) throws Exception {
			System.out.println("Access Code--->" + acc_code);
			System.out.println("Encrypted Data---->" + enc);
			request = given().spec(requestSpecification()).queryParam("acc_code", acc_code).queryParam("enc_req", enc);

		}
		//invalid encrypted data
		@Then("the ReversalAPI call got success with status code {int} and check {string} and {string} for invalid encrypted data")
		public void the_ReversalAPI_call_got_success_with_status_code_and_check_and_for_invalid_encrypted_data(Integer int1, String expected_errorCode, String expected_errorMsg) throws Exception {
			ValidateBean result = new ValidateBean();
			System.out.println("expected_errorCode----->" + expected_errorCode);

			bc_ref_num="";
			Pin4="";
			Pin6="";
			MobNumber="";
			System.out.println(response.getStatusCode());
			if (response.getStatusCode() == 204) {
				TimeUnit.SECONDS.sleep(2);
				result = ReversalDao.getResult(bc_ref_num, Pin4, Pin6, MobNumber);
				assertEquals(expected_errorCode, result.getErr_code());
				assertEquals(expected_errorMsg, result.getError_message().trim());
				if(expected_errorCode!=result.getErr_code()){
					System.out.println("Error Message is: " +result.getError_message().trim());
				}
			}
			assertEquals(response.getStatusCode(), 204);
		}



}
