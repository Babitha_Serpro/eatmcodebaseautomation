package com.eatm.stepDefinitions;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.ccavenue.security.AesCryptUtil;
import com.eatm.bean.ValidateBean;
import com.eatm.dao.ValidateDao;
import resources.AllResources;
import resources.DespenseData;
import resources.Utils;
import com.fasterxml.jackson.core.JsonProcessingException;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class DispenseDefinition extends Utils {
	private static org.apache.log4j.Logger logger = org.apache.log4j.LogManager.getLogger(DispenseDefinition.class);
	
	ResponseSpecification resspec;
	RequestSpecification reqspec;
	Response response;
	DespenseData data = new DespenseData();
	String code = "Lu2IwUj8tXLD";
	String bc_ref_Number = Utils.bc_ref_no();
	ValidateDao dao = new ValidateDao();

	// NegativeScenario
	@Given("ccAvenue request with {string} and encrypted data with {string} {string} {string} {string} {string} {string} {string} {string} {string}")
	public void ccavenue_request_with_and_encrypted_data_with(String access_code, String mer_id, String bc_id,
			String acc_code, String agg_id, String req_type, String bc_param1, String bid, String original_bc_ref_no,
			String original_eatm_ref_num) throws Exception {
		reqspec = given().header("Content-Type", "application/json").spec(requestSpecification())
				.body(data.addData(access_code, mer_id, bc_id, acc_code, agg_id, bc_ref_Number, req_type, bc_param1,
						bid, original_bc_ref_no, original_eatm_ref_num));
		logger.debug("Given Data:  ------->" + data.addData(access_code, mer_id, bc_id, acc_code, agg_id,
				bc_ref_Number, req_type, bc_param1, bid, original_bc_ref_no, original_eatm_ref_num));
	}

	// when bc_ref_num is null
	@Given("ccAvenue request with {string} and encrypted data with  {string} {string} {string} {string} {string} {string} {string} {string} {string} {string}")
	public void ccavenue_request_with_and_encrypted_data_with(String access_code, String mer_id, String bc_id,
			String acc_code, String agg_id, String bc_ref_num_desp, String req_type, String bc_param1, String bid,
			String original_bc_ref_no, String original_eatm_ref_num) throws Exception {
		// Write code here that turns the phrase above into concrete actions
		logger.debug("bc_ref_num" + bc_ref_num_desp);

		if (!bc_ref_num_desp.equals("")) {
			if (bc_ref_num_desp.equals("000"))
				bc_ref_num_desp = "000";
			else
				bc_ref_num_desp = bc_ref_Number;
		}
		reqspec = given().header("Content-Type", "application/json").spec(requestSpecification())
				.body(data.addData(access_code, mer_id, bc_id, acc_code, agg_id, bc_ref_num_desp, req_type, bc_param1,
						bid, original_bc_ref_no, original_eatm_ref_num));
	}

	// Invalid encrypted data
	@Given("ccAvenue request with {string} and encrypted data with {string}")
	public void ccavenue_request_with_and_encrypted_data_with(String access_code, String enc) throws Exception {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("acc_code", access_code);
		jsonObject.put("enc_req", enc);
		reqspec = given().header("Content-Type", "application/json").spec(requestSpecification()).body(jsonObject);
		logger.debug("Given Data:  ------->" + jsonObject);
	}

	// Invalid access code
	@Given("ccAvenue request with invalid {string} and encrypted data with {string} {string} {string} {string} {string} {string} {string} {string} {string}")
	public void ccavenue_request_with_invalid_and_encrypted_data_with(String access_code, String mer_id, String bc_id,
			String acc_code, String agg_id, String req_type, String bc_param1, String bid, String original_bc_ref_no,
			String original_eatm_ref_num) throws Exception {

		String bc_ref_Number = Utils.bc_ref_no();
		logger.debug("bc ref num:" + bc_ref_Number);
		List<String> data = new ArrayList<String>();
		JSONObject jsonObject = new JSONObject();

		String text = "mer_id=" + mer_id + "&bc_id=" + bc_id + "&acc_code=" + acc_code + "&agg_id=" + agg_id
				+ "&req_type=" + req_type + "&bc_ref_no=" + bc_ref_Number + "&bc_param1=" + bc_param1 + "&bid=" + bid
				+ "&original_bc_ref_no=" + original_bc_ref_no + "&original_eatm_ref_num=" + original_eatm_ref_num;
		// logger.debug("Text------>" + text);

		AesCryptUtil aesUtil = new AesCryptUtil("SkMwAv4YwUpbQvHr");
		String enc = aesUtil.encrypt(text);
		// logger.debug("EncryptedData---->" + enc);
		logger.debug("Access code---->" + access_code);
		// logger.debug("AccessCode---->" + data.get(0));
		// logger.debug("Listdata : "+data);
		jsonObject.put("acc_code", access_code);
		jsonObject.put("enc_req", enc);
		// logger.debug("jsondata : "+jsonObject);
		reqspec = given().header("Content-Type", "application/json").spec(requestSpecification()).body(jsonObject);
		logger.debug("Given Data:  ------->" + jsonObject);
	}

	// Positive scenario
	@Given("ccAvenue positive request with {string} and encrypted data with  {string} {string} {string} {string} {string} {string} {string} {string} {string} {string}")
	public void ccavenue_positive_request_with_and_encrypted_data_with(String access_code, String mer_id, String bc_id,
			String acc_code, String agg_id, String bc_ref_num_desp, String req_type, String bc_param1, String bid,
			String original_bc_ref_no, String original_eatm_ref_num) throws Exception {

		ValidateBean bc_num_bean = new ValidateBean();
		original_bc_ref_no = ValidateDefinition.validateBc_ref_num;
		logger.debug("Original bc_ref num---->" + original_bc_ref_no);
		// original_bc_ref_no = "CC74505465";
		bc_num_bean = dao.get_original_eatm_ref_num(original_bc_ref_no);

		logger.debug("Original bc_ref num---->" + bc_num_bean);
		original_eatm_ref_num = String.valueOf(bc_num_bean.getRequest_id());

		reqspec = given().header("Content-Type", "application/json").spec(requestSpecification())
				.body(data.addData(access_code, mer_id, bc_id, acc_code, agg_id, bc_ref_Number, req_type, bc_param1,
						bid, original_bc_ref_no, original_eatm_ref_num));
		logger.debug("Given Data:  ------->" + data.addData(access_code, mer_id, bc_id, acc_code, agg_id,
				bc_ref_Number, req_type, bc_param1, bid, original_bc_ref_no, original_eatm_ref_num));
	}

	@When("ccAvenue will call {string} with {string} http request")
	public void ccavenue_will_call_with_http_request(String resource, String method) {
		AllResources resourceAPI = AllResources.valueOf(resource);
		resspec = new ResponseSpecBuilder().expectStatusCode(200).expectContentType(ContentType.JSON).build();
		if (method.equalsIgnoreCase("POST")) {
			// logger.debug("Request Specification:" +reqspec);
			response = reqspec.when().post(resourceAPI.getResource());
		} else if (method.equalsIgnoreCase("GET")) {
			response = reqspec.when().get(resourceAPI.getResource());
		}
	}

	@Then("the API call get success with status code {int} and check {string} and {string}")
	public void the_API_call_get_success_with_status_code_and_check_and(Integer int1, String error_code,
			String error_message) throws Exception {

		assertEquals(200, response.getStatusCode());

		String rs = response.asString();
		logger.debug("Encrypted REsponse:" + rs + "\n");

		String[] words = rs.split(":");
		logger.debug("Response array:" + Arrays.toString(words));
		logger.debug(words[1]);
		String rs1 = words[1];

		rs1 = rs1.replaceAll("[^a-zA-Z0-9]", " ");
		// logger.debug(rs1);

		logger.debug("Rs1-->" + rs1);
		rs1 = rs1.trim();
		String result = data.decrptData(rs1);
		// logger.debug("Result----->"+result);
		String[] resultArray = result.split("&");
		String[] newResult = null;
		for (int i = 0; i < resultArray.length; i++) {
			// logger.debug("Result Array------->"+resultArray[i]);
			newResult = resultArray[i].split("=");
			logger.debug("key--->" + newResult[0]);
			logger.debug("value--->" + newResult[1]);
			String errorcode = newResult[1].trim();
			if (newResult[0].equalsIgnoreCase("err_code")) {

				
				logger.debug("Dispence Error Code-->"+newResult[1].trim());
				assertEquals(error_code, newResult[1].trim());
			}
			if (newResult[0].equalsIgnoreCase("err_message")) {
				
				logger.debug("Dispence Error Message-->"+newResult[1].trim());
				assertEquals(error_message, newResult[1].trim());
			}

		}

	}

	@Then("the API call get success with status code {int} and response {string}")
	public void the_API_call_get_success_with_status_code_and_response(Integer status_code, String enc_resp) throws Exception {
		// Write code here that turns the phrase above into concrete actions
		assertEquals(200, response.getStatusCode());
		enc_resp = "null ";
		String rs = response.asString();
		String[] words = rs.split(":");
		String rs1 = words[1];
		logger.debug("Encrypted response----->" + rs);
		rs1 = rs1.replaceAll("[^a-zA-Z0-9]", " ");
		
		logger.debug("Dispence ResultSet -->"+rs1);
		assertEquals(enc_resp, rs1);
	}

}
