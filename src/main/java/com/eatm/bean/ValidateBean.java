package com.eatm.bean;

public class ValidateBean {

	private double amount;
	private String err_code;
	private String error_message;
	private String pin4;
	private String pin6;
	private String mobno;
	public String bc_ref_no;
	private int val_success;
	private int request_id;
	private int response_id;
	private String plain_text;
	private String description;
	
	

	public String getErr_code() {
		return err_code;
	}
	public void setErr_code(String err_code) {
		this.err_code = err_code;
	}
	public String getError_message() {
		return error_message;
	}
	public void setError_message(String error_message) {
		this.error_message = error_message;
	}
	public int getVal_success() {
		return val_success;
	}
	public void setVal_success(int val_success) {
		this.val_success = val_success;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public int getRequest_id() {
		return request_id;
	}
	public void setRequest_id(int request_id) {
		this.request_id = request_id;
	}
	public int getResponse_id() {
		return response_id;
	}
	public void setResponse_id(int response_id) {
		this.response_id = response_id;
	}
	public String getPlain_text() {
		return plain_text;
	}
	public void setPlain_text(String plain_text) {
		this.plain_text = plain_text;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPin4() {
		return pin4;
	}
	public void setPin4(String pin4) {
		this.pin4 = pin4;
	}
	public String getPin6() {
		return pin6;
	}
	public void setPin6(String pin6) {
		this.pin6 = pin6;
	}
	public String getMobno() {
		return mobno;
	}
	public void setMobno(String mobno) {
		this.mobno = mobno;
	}
	public String getBc_ref_no() {
		return bc_ref_no;
	}
	public void setBc_ref_no(String bc_ref_no) {
		this.bc_ref_no = bc_ref_no;
	}

	
	
	
}
