package com.cca.ob.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HttpExecutor {
	
	private Logger logger=LogManager.getLogger(HttpExecutor.class);
	
	 public void doGet(String endPoint) throws Exception {
		   final CloseableHttpClient httpClient = HttpClients.createDefault();
	        HttpGet request = new HttpGet(endPoint);
	        logger.info("About to hit the endpoint "+endPoint);
	        System.out.println("About to hit the endpoint "+endPoint);
	        
	        try{
	        	CloseableHttpResponse response = httpClient.execute(request);
	            // Get HttpResponse Status
	            //System.out.println(response.getStatusLine().toString());
		        logger.info("After executing status line = "+response.getStatusLine().toString());
	            HttpEntity entity = response.getEntity();
	            Header headers = entity.getContentType();
	            System.out.println(headers);

	            if (entity != null) {
	                // return it as a String
	                String result = EntityUtils.toString(entity);
	                //System.out.println(result);
	                logger.info("After executing Response  = "+result);
	            }

	        }finally {
	        	httpClient.close();
	        }

	    }
	 
	 public HttpResponse doPost(String endPoint,Map<String,String> postMap) throws Exception{
		 	HttpPost post = new HttpPost(endPoint);
		 	//System.out.println("Calling endpt "+endPoint+"\t body "+postMap);
		 	logger.info("Calling endpt "+endPoint+"\t body "+postMap);
	        // add request parameter, form parameters
	        List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
	        
	        for(Map.Entry<String,String> entry:postMap.entrySet()) {
	        	urlParameters.add(new BasicNameValuePair(entry.getKey(),entry.getValue()));
	        }
	        post.setEntity(new UrlEncodedFormEntity(urlParameters));
	        CloseableHttpClient httpClient=null;
	        CloseableHttpResponse response=null;
	        try {
	        	httpClient = HttpClients.createDefault();
                response = httpClient.execute(post) ;
                
                
                //@Todo
                //Remove logging response
	            //System.out.println("Response is "+EntityUtils.toString(response.getEntity()));
	            //logger.info("Response is "+EntityUtils.toString(response.getEntity()));
	            //return EntityUtils.toString(response.getEntity());
	            //response.getStatusLine().
                int statusCode = response.getStatusLine().getStatusCode();
                System.out.println("The Status code from the response is "+statusCode);
                InputStream iStream=response.getEntity().getContent();
                return new HttpResponse(getResponse(iStream), statusCode);
	        }finally {	
	        	if(httpClient!=null) {
	        		httpClient.close();
	        	}
	        	if(response!=null) {
	        		response.close();
	        	}
	        }
	 }
	 public HttpResponse postPayload(String endPoint,String payload,Map<String,String> headerMap) throws Exception{
		 	HttpPost httpPost = new HttpPost(endPoint);
		 	//System.out.println("Calling endpt "+endPoint+"\t body "+postMap);
		 	logger.info("Calling endpt "+endPoint+"\t body "+payload);
		 	
		 	StringEntity entity = new StringEntity(payload);
		    httpPost.setEntity(entity);
		    if(headerMap!=null) {
		    	for(Map.Entry<String, String> headerEntry: headerMap.entrySet()) {
		    		httpPost.setHeader(headerEntry.getKey(),headerEntry.getValue());
		    	}
		    }
		   
		    
		    for(Header header:httpPost.getAllHeaders()) {
		    	logger.info("Header values  "+header.getName()+"="+header.getValue() );
		    }
		    CloseableHttpClient httpClient =null;
		    CloseableHttpResponse response=null;
	        try {
	        	httpClient = HttpClients.createDefault();
	        	response = httpClient.execute(httpPost) ;
	        	int statusCode = response.getStatusLine().getStatusCode();
	        	
	        	InputStream iStream=response.getEntity().getContent();
	        	//String respString=getResponseAsString(iStream);
	        	//System.out.println("--------------"+response.getEntity().getContentEncoding());
	            //String resp= EntityUtils.toString(response.getEntity(),Charset.forName("UTF-8"));
	            return new HttpResponse(getResponse(iStream), statusCode);
	        }finally {	
	        	if(httpClient!=null) {
	        		httpClient.close();
	        	}
	        	if(response!=null) {
	        		response.close();
	        	}
	        }
	 }
	 
	 
	 private String getResponse(InputStream inputStream) throws IOException {
		 byte[] byteStream=getResponsebytes( inputStream);
		 return new String(byteStream,StandardCharsets.UTF_8);
		 
	 }
	 
	 private byte[] getResponsebytes(InputStream inputStream) throws IOException {
		 ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		    int nRead;
		    byte[] data = new byte[1024];
		    while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
		        buffer.write(data, 0, nRead);
		    }
		 
		    buffer.flush();
		    return buffer.toByteArray();
	 }
}
