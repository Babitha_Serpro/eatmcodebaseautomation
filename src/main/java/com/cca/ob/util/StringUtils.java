package com.cca.ob.util;

import java.util.regex.Pattern;

public class StringUtils {
	private static Pattern pattern = Pattern.compile("-?\\d+?");
	public static boolean isNumeric(String numericString) {
		if (numericString == null) {
	        return false; 
	    }
	    return pattern.matcher(numericString).matches();
	}
}
