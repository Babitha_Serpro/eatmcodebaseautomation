package com.cca.ob.util;

public class OTPGenerator {

	public static String generate(int length) {
		long intNo=(int)(Math.random()* Math.pow(10,length));
		String otp= String.valueOf(intNo);
		
		return getLeadingZeros(otp,length);
	}
	
	private static String getLeadingZeros(String rawOTP,int expLen) {
		while(rawOTP.length()<expLen) {
			rawOTP="0"+rawOTP;
		}
		return rawOTP;
	}
}
