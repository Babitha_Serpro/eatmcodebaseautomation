package com.cca.ob.util;

import java.util.Map;

import com.cca.ob.bc.validators.TokenValidationStatusBean;
import com.cca.ob.constants.BCConstants;

public class TokenProcessUtils {
	public static void populateValidationStatus(Map<String,String> responseMap,TokenValidationStatusBean validationBean,boolean isReversal) {
		if(validationBean!=null && validationBean.isValidationStatus() &&  validationBean.getAmount()>0) {
			responseMap.put(BCConstants.RESP_PARAM_AMOUNT,String.valueOf(validationBean.getAmount()));
			if(isReversal) {
				responseMap.put(BCConstants.RESP_PARAM_REVERSAL_SUCCESS, "true");
				responseMap.remove(BCConstants.RESP_PARAM_DISPENSE_CASH);
			}else {
				responseMap.put(BCConstants.RESP_PARAM_DISPENSE_CASH, "true");
				responseMap.remove(BCConstants.RESP_PARAM_REVERSAL_SUCCESS);
			}
		}

		
	}
}
