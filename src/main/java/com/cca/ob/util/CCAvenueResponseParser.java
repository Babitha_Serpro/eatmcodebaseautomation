package com.cca.ob.util;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class CCAvenueResponseParser {
	
	public static final String ORDER_ID="order_id";
	public static final String TRACKING_ID="tracking_id";
	public static final String BANK_REF_NO="bank_ref_no";
	public static final String ORDER_STATUS="order_status";
	public static final String FAILURE_MSG="failure_message";
	public static final String STATUS_CODE="status_code";
	public static final String STATUS_MESSAGE="status_message";
	public static final String CURRENCY="currency";
	public static final String AMOUNT="amount";
	public static final String PAYMENT_MODE="payment_mode";
	public static final String WITHDRAWL_TYPE="merchant_param1";
	
	public static final String PARAM_SEPERATOR="&";
	private static Logger logger=LogManager.getLogger(CCAvenueResponseParser.class);
	
	private String response;
	public CCAvenueResponseParser(String response) {
		this.response=response;
	}
	
	public String getValue(String key) {
		int start=response.indexOf(key+"=");
		if(start==-1) {
			logger.info("Could not find the key "+key+" in the Gateway Callback Response");
			return null;
		}
		start=start+key.length()+1;
		int endIndex=response.indexOf(PARAM_SEPERATOR,start);
		if(endIndex==-1) {
			logger.info("Could not find the Value for the key "+key+" in the Gateway Callback Response");
			return null;
		}
		if(start==endIndex) {
			logger.debug("Empty Value for the key "+key+" in the Gateway Callback Response");
			return null;
		}
		String val= response.substring(start, endIndex);
		if("null".equals(val)) {
			return null;
		}
		return val;
	}
}
