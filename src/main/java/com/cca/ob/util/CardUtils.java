package com.cca.ob.util;

import com.cca.ob.constants.OBConstants;
import com.cca.ob.exception.OBException;

public class CardUtils {
	public static String maskCard(String cardNumber) {
		return "xxxxxxxxxxxx" + cardNumber.substring(cardNumber.length() - 4);
	}
	public static String maskCard(String cardNumber,boolean decrypt) {
		try {
			return maskCard(new EncryptionUtils().decrypt(cardNumber));
		} catch (Exception e) {
			throw new OBException("The card number could not be decrypted", e);
		}
	}
	
	public static boolean isValidCard(String cardNumber) {
		if (cardNumber == null || cardNumber.length() != 16 || !StringUtils.isNumeric(cardNumber)) {
			return false;
		}
		return true;
	}
	public static boolean isValidExpiry(String expMonth,String expYr) {
		if(expMonth==null || expYr==null || expMonth.length()!=2 ) {
			return false;
		}
		int intExpYr=0;
		int intExpMonth=0;
		try {
			if(expYr.length()==2) {
				expYr=OBConstants.YYYY_PREFIX+expYr;
			}else if(expYr.length()!=4){
				return false;
			}
			
			intExpYr=Integer.parseInt(expYr);	
			intExpMonth=Integer.parseInt(trimLeadingZeros(expMonth));
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		
		if(intExpMonth<1 || intExpMonth>12) {
			return false;
		}
		if(intExpYr>DateUtils.getCurrentYear()) {
			return true;
		}else if(intExpYr==DateUtils.getCurrentYear() && intExpMonth>DateUtils.getCurrentMonth()) {
			return true;
		}
		return false;
	}
	
	
	public static boolean isCvvVAlid(String cvv) {
		if (cvv == null || cvv.length() != 3 || !StringUtils.isNumeric(cvv)) {
			return false;
		}
		return true;
	}
	private static String trimLeadingZeros(String astr) {
		String strPattern = "^0+(?!$)";  
		return astr.replaceAll(strPattern, "");
	}
}
