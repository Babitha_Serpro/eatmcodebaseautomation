package com.cca.ob.util;

public class TokenUtils {
	private static final int tokenLength=6;
	
	public static String getZeroPaddedToken(long tokenNum) {
		StringBuilder builder=new StringBuilder();
		String tokenString=String.valueOf(tokenNum);
		int padding=tokenLength-tokenString.length();
		for(int i=0;i<padding;i++) {
			builder.append(0);
		}
		builder.append(tokenString);
		return builder.toString();
	}
}
