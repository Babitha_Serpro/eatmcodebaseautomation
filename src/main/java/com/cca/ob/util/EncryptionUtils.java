package com.cca.ob.util;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Base64.Encoder;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import com.cca.ob.constants.OBConstants;
import com.cca.ob.secure.KeyUtil;

public class EncryptionUtils {
	
	private Cipher cipher ;
	private SecretKey secretKey;
	public EncryptionUtils() throws NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException{
		this.cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		this.secretKey =KeyUtil.getSecretKey();
	}

	public EncryptionUtils(Cipher cipher,SecretKey secretKey){
		this.cipher=cipher;
		this.secretKey = secretKey;
	}
	
	private byte[] encrypt(byte[] data) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		//return data;
		return doFinal(Cipher.ENCRYPT_MODE, data);
	}
	public String encrypt(String data) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		byte[] result=encrypt(data.getBytes(OBConstants.UTF_8));
		Encoder encoder=Base64.getEncoder();
        return encoder.encodeToString(result);
	}	
	private byte[] decrypt(byte[] data) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		//return data;
		return doFinal(Cipher.DECRYPT_MODE, data);
	}
	public String decrypt(String data) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		byte[] decordedValue = Base64.getDecoder().decode(data);
		byte[] result=decrypt(decordedValue);
		return new String(result,OBConstants.UTF_8);
	}
	private byte[] doFinal(int mode,byte[] payload) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		cipher.init(mode, secretKey);
	    return cipher.doFinal(payload);
	}
}
