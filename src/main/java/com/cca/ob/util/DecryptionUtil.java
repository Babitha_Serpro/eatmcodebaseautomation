package com.cca.ob.util;

import java.nio.charset.StandardCharsets;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import com.cca.ob.service.MerchantDataService;

public class DecryptionUtil {
//	@Value("${secretKey}")
//	private static String secretKey="8ebd81f066ce4492d38bd0bdbba9b05d0a88306f9407a5553603aa90f80d41e2";
	private static Logger logger=LogManager.getLogger(MerchantDataService.class);

	public static String decrypt(String input, String secretKey) {
		byte[] output = null;
		try {
			java.util.Base64.Decoder decoder = java.util.Base64.getDecoder();
			SecretKeySpec skey = new SecretKeySpec(hexStringToByteArray(secretKey), "AES");
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, skey);
			output = cipher.doFinal(decoder.decode(input));
		} catch (Exception e) {
			logger.error("Invalid Encryption data "+input,e);
			return null;
		}
		return new String(output);
	}

	public synchronized static byte[] hexStringToByteArray(String s)
	{
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2)
		{
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) <<
					4) + Character.digit(s.charAt(i + 1), 16));
		}
		return data;
	}

}