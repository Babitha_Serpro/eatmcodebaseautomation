package com.cca.ob.util;

import java.util.HashMap;
import java.util.Map;

import com.cca.ob.constants.BCConstants;
import com.ccavenue.security.AesCryptUtil;

public class ResponseUtils {

	public static Map<String,String> getEncryptedResponse(Map<String,String> response,String respSecretKey){
		String plainResp=stringify(response);
		System.out.println("Stringified Response "+plainResp);
		String ecryptedResp=encryptResponse(plainResp, respSecretKey);
		System.out.println("Stringified Response "+plainResp);
		Map<String,String> encRespMap=new HashMap<String, String>();
		encRespMap.put(BCConstants.RESP_PARAM_ENCRYPTED_RESPONSE, ecryptedResp);
		return encRespMap;
	}
	private static String stringify(Map<String,String> response) {
		StringBuilder builder=new StringBuilder();
		for(Map.Entry<String, String> entry:response.entrySet()) {
			builder.append(entry.getKey()+"="+entry.getValue()+"&");
		}
		if(builder.length()>0) {
			builder.deleteCharAt(builder.length()-1);
		}
		return builder.toString();
	}
	
	private static String encryptResponse(String plainResp,String encodeKey) {
		if(encodeKey!=null) {
			AesCryptUtil aesUtil=new AesCryptUtil(encodeKey);
			return aesUtil.encrypt(plainResp);
		}
		return null;
	}
}
