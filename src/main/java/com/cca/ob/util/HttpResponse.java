package com.cca.ob.util;

import java.io.UnsupportedEncodingException;

public class HttpResponse {
	private byte[] response;
	private int statusCode;
	private String responseString;
	
	
	public HttpResponse(byte[] response, int statusCode) {
		super();
		this.response = response;
		this.statusCode = statusCode;
	}
	
	public HttpResponse(String response, int statusCode) {
		super();
		this.responseString = response;
		this.statusCode = statusCode;
	}
	public byte[] getResponseBytes() {
		return response;
	}
	public String getResponseAsString() throws UnsupportedEncodingException {
		if(responseString==null) {
			responseString=new String(response,"UTF-8");
		}
		return responseString;
	}
	public int getStatusCode() {
		return statusCode;
	}
	
	
	
}
