package com.cca.ob.util;



import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateUtil {
	public static String getCurrentDateTime() {
		String pattern = "yyyyMMddhhmmss";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		return simpleDateFormat.format(new Date(System.currentTimeMillis()));
	}
	
	
	public static String getISTDateTime()  {
		Date date= new Date(); 
		String format = "yyyyMMddHHmmss";
		String timeZone = "IST";
		
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		
		if (timeZone == null || "".equalsIgnoreCase(timeZone.trim())) {
			timeZone = Calendar.getInstance().getTimeZone().getID();
		}
		
		sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
		
		String ISTDate= sdf.format(date);
		
		return ISTDate;

		
	}
}
