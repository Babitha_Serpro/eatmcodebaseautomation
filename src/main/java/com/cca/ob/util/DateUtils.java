package com.cca.ob.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {
	public static Date getCurrentDateWithoutTime() throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat(
			      "dd/MM/yyyy");
			    return formatter.parse(formatter.format(new Date()));
	}
	
	public static int getCurrentYear() {
		Calendar cal=Calendar.getInstance();
		return cal.get(Calendar.YEAR);
	}
	public static int getCurrentMonth() {
		Calendar cal=Calendar.getInstance();
		return cal.get(Calendar.MONTH);
	}
}
