package com.cca.ob.util;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

public class PostExecutor {

	public String postJson(String endpoint,String payLaod) {
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
		 HttpEntity<String> request = 
			      new HttpEntity<String>(payLaod, headers);
	    return 
	      restTemplate.postForObject(endpoint, request, String.class);
	}
}
