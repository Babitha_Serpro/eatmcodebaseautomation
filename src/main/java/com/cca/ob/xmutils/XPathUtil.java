package com.cca.ob.xmutils;

import java.io.InputStream;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.cca.ob.exception.OBException;

public class XPathUtil {
	
	private static final Logger LOGGER=LogManager.getLogger(XPathUtil.class);
	
	  private Document document;

	  public Document getDocument() {
	    return document;
	  }

	  public void build(InputStream response, String defaultEncoding) throws OBException {
	    DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder builder;
	    try {
	      disbaleEnternalEntityInjection(builderFactory);
	      builder = builderFactory.newDocumentBuilder();
	      if (defaultEncoding == null) {
	        LOGGER.debug(
	            "Default Encoding not specified..Parsing with encoding specified by the document");
	        document = builder.parse(response);
	      } else {
	        LOGGER.debug("Parsing with the user specified encoding " + defaultEncoding);
	        InputSource iSource = new InputSource(response);
	        iSource.setEncoding(defaultEncoding);
	        document = builder.parse(iSource);
	      }
	      //checkErrorMessages();
	    } catch (Exception e) {
	      LOGGER.error("Parsing with the user specified encoding =" + defaultEncoding
	          + "\t encountered exception ", e);
	      throw new OBException("Exception in parsing the XML " + e.getMessage(), e);
	    }
	  }

	  public void build(String responseStr, String defaultEncoding) throws Exception {
	    DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
	    disbaleEnternalEntityInjection(builderFactory);
	    DocumentBuilder builder;
	    try {
	      builder = builderFactory.newDocumentBuilder();
	      InputSource iSource = new InputSource(new StringReader(responseStr));
	      LOGGER.debug("Parsing with the User Specified Encoding =" + defaultEncoding);
	      if (defaultEncoding != null) {
	        iSource.setEncoding(defaultEncoding);
	      }
	      document = builder.parse(iSource);
	      //checkErrorMessages();
	    } catch (OBException dae) {
	      throw dae;
	    }
	  }

	  private void disbaleEnternalEntityInjection(DocumentBuilderFactory builderFactory)
	      throws ParserConfigurationException {
	    builderFactory.setFeature("http://xml.org/sax/features/external-general-entities", false);
	    builderFactory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
	  }

	/*
	 * private void checkErrorMessages() throws OBException { String expression =
	 * "/entry/error"; XPath xPath = XPathFactory.newInstance().newXPath(); NodeList
	 * nodeList; try { nodeList =
	 * (NodeList)xPath.compile(expression).evaluate(document,
	 * XPathConstants.NODESET); } catch (XPathExpressionException e) { throw new
	 * OBException("Error while Extrating the Xpath " + expression, e); }
	 * 
	 * if (nodeList.getLength() > 0) { Element el = (Element)nodeList.item(0);
	 * String errorCode =
	 * el.getElementsByTagName("errorcode").item(0).getTextContent(); String
	 * errorMessage = el.getElementsByTagName("errormsg").item(0).getTextContent();
	 * String httpStatusCode =
	 * el.getElementsByTagName("httpcode").item(0).getTextContent();
	 * 
	 * throw new OBException(errorCode, errorMessage,
	 * Integer.parseInt(httpStatusCode)); } }
	 */
}
