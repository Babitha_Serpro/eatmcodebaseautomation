package com.cca.ob.bc.validators.ic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

public class SocketConnector {
	private static Logger logger=LogManager.getLogger(SocketConnector.class);
	@Value("${security.ic.socket.ip}")
	private String ip;
	
	@Value("${security.ic.socket.port}")
	private int port;	
	
	public String getResponse(String xmlPacket) throws IOException {
		// TODO Auto-generated method stub
		return connect(xmlPacket);
	}
	
	private String connect(String xmlPacket) throws IOException {
		Socket socket = new Socket(ip, port,InetAddress.getLocalHost(),5220); 
		System.out.println("Socket ---Inet "+InetAddress.getLocalHost().getHostAddress());
		logger.debug("Trying to open the socket "+ip+"  at port "+port+" Localhost "+InetAddress.getLocalHost().getHostAddress()+" port "+5220);
		PrintStream out = new PrintStream( socket.getOutputStream() );
		out.println(xmlPacket); 
		out.flush();
		BufferedReader inReader = new BufferedReader( new InputStreamReader( socket.getInputStream() ) );
		
		 StringBuilder responseBuilder=new StringBuilder();
		 String line = inReader.readLine();
         while( line != null ){
        	 responseBuilder.append(line);
             line = inReader.readLine();
         }
         inReader.close();
         out.close();
         socket.close();
         return responseBuilder.toString();
	}
}
