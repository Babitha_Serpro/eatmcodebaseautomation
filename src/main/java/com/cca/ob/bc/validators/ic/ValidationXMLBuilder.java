package com.cca.ob.bc.validators.ic;

import java.io.OutputStream;
import java.io.StringWriter;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.cca.ob.exception.OBException;
public class ValidationXMLBuilder {
	protected final String MESSAGE_TYPE="MessageType";
	protected final String PROC_CODE="ProcCode";
	protected final String SYST_TRACE="SystemTraceNumber";
	protected final String DATE_TIME="DateTime";
	protected final String RRNO="RRNNumber";
	protected final String TERMINAL_IND="TerminalId";
	protected final String MOBILE_NUM="MobileNo";
	protected final String PIN="Pin";
	protected final String CHANNEL_IND="ChannelId";
	protected final String RESERVED="Reserved";	
	
	
	
	
	private static final Logger LOGGER=LogManager.getLogger(ValidationXMLBuilder.class);
	  private static final String ROOT_ELEMENT = "XML";

	  public String marshal(TransactionBean packetData) throws OBException {
	    try {
	      DocumentBuilderFactory dFact = DocumentBuilderFactory.newInstance();
	      dFact.setNamespaceAware(false);
	      DocumentBuilder build = dFact.newDocumentBuilder();
	      Document doc = build.newDocument();
	      Element root = doc.createElement(ROOT_ELEMENT);
	      doc.appendChild(root);
	      createChildElements(doc, root,packetData);
	      //appendNode(doc, root,"MESSAGE_TYPE","1234");
	      LOGGER.debug("About to create root entity");
	      
	      //return transform(doc).getOutputStream();
	      return transFormToString(doc);
	    } catch (Exception e) {
	      LOGGER.error("Error in creating the XML from the SkyEntity " , e);
	      throw new OBException("Error in creating the XML from the SkyEntity ", e);
	    }
	  }
	  
	  protected void createChildElements(Document doc, Element parentElement,TransactionBean packetData) {
		  appendNode(doc, parentElement, MESSAGE_TYPE, packetData.getMessageType(),getMessageTypeAttributes());
		  appendNode(doc, parentElement, PROC_CODE, packetData.getProcCode(),getProcCodeAttributes());
		  appendNode(doc, parentElement, SYST_TRACE, packetData.getSystemTraceNumber(),getSystemTraceNumber());
		  appendNode(doc, parentElement, DATE_TIME, packetData.getDateTime(),getDateTime());
		  appendNode(doc, parentElement, RRNO, packetData.getrRNNumbe(),getRRNum());
		  appendNode(doc, parentElement, TERMINAL_IND, packetData.getTerminalId(),getTerminalId());
		  appendNode(doc, parentElement, MOBILE_NUM, packetData.getMobileNum(),getMobileNum());
		  appendNode(doc, parentElement, PIN, packetData.getPin(),new HashMap<String, String>());
		  appendNode(doc, parentElement, CHANNEL_IND, packetData.getChannelId(),getChannelId());
		  appendNode(doc, parentElement, RESERVED, packetData.getOptionalParam(),getReserved());
	  }
	  
	  public void appendNode(Document doc, Element parentElement,String key,Object value,Map<String,String> attributes) {
		  //Element entityElement  = doc.createElement(curEntity.getEntityName());
		  Element aField = doc.createElement(key);
		  
		  String strValue=(value==null ? "" :value.toString());
		  aField.appendChild(doc.createTextNode(strValue));
		  for(Map.Entry<String, String> entry :attributes.entrySet() ) {
			  aField.setAttribute(entry.getKey(),entry.getValue());
		  }
          parentElement.appendChild(aField);
	  }
	  
	  public  Map<String,String> getMessageTypeAttributes(){
		  Map<String,String> mp=new HashMap<String, String>();
		  mp.put("DEFAULT", "1200");
		  mp.put("LSP", "0");
		  mp.put("MNL", "4");
		  mp.put("MXL", "4");
		  mp.put("PL", "4");
		  mp.put("PRE", "mandatory");
		  return mp;
	  }
	  public  Map<String,String> getProcCodeAttributes(){
		  Map<String,String> mp=new HashMap<String, String>();
		  mp.put("IND", "3");
		  mp.put("DEFAULT", "920000");
		  mp.put("LSP", "0");
		  mp.put("MNL", "6");
		  mp.put("MXL", "6");
		  mp.put("PL", "6");
		  mp.put("PRE", "mandatory");
		  return mp;
	  }
	  public  Map<String,String> getSystemTraceNumber(){
		  Map<String,String> mp=new HashMap<String, String>();
		  mp.put("IND", "11");
		  mp.put("LSP", "0");
		  mp.put("MNL", "6");
		  mp.put("MXL", "6");
		  mp.put("PL", "6");
		  mp.put("PRE", "mandatory");
		  return mp;
	  }
	  public  Map<String,String> getDateTime(){
		  Map<String,String> mp=new HashMap<String, String>();
		  mp.put("IND", "12");
		  mp.put("LSP", "0");
		  mp.put("MNL", "12");
		  mp.put("MXL", "12");
		  mp.put("PL", "12");
		  mp.put("PRE", "mandatory");
		  return mp;
	  }
	  public  Map<String,String> getRRNum(){
		  Map<String,String> mp=new HashMap<String, String>();
		  mp.put("IND", "37");
		  mp.put("LSP", "0");
		  mp.put("MNL", "12");
		  mp.put("MXL", "12");
		  mp.put("PL", "12");
		  mp.put("PRE", "mandatory");
		  return mp;
	  }
	  public  Map<String,String> getTerminalId(){
		  Map<String,String> mp=new HashMap<String, String>();
		  mp.put("IND", "41");
		  mp.put("LSP", "0");
		  mp.put("MNL", "16");
		  mp.put("MXL", "16");
		  mp.put("PL", "16");
		  mp.put("PRE", "mandatory");
		  return mp;
	  }
	  public  Map<String,String> getMobileNum(){
		  Map<String,String> mp=new HashMap<String, String>();
		  mp.put("IND", "102");
		  mp.put("LSP", "2");
		  mp.put("MNL", "1");
		  mp.put("MXL", "99");
		  mp.put("PL", "10");
		  mp.put("PRE", "mandatory");
		  return mp;
	  }	  
	  public  Map<String,String> getChannelId(){
		  Map<String,String> mp=new HashMap<String, String>();
		  mp.put("IND", "123");
		  mp.put("LSP", "3");
		  mp.put("MNL", "1");
		  mp.put("MXL", "999");
		  mp.put("PL", "3");
		  mp.put("PRE", "mandatory");
		  return mp;
	  }	 
	  public  Map<String,String> getReserved(){
		  Map<String,String> mp=new HashMap<String, String>();
		  mp.put("IND", "125");
		  mp.put("LSP", "3");
		  mp.put("MNL", "1");
		  mp.put("MXL", "999");
		  mp.put("PL", "14");
		  mp.put("PRE", "optional");
		  return mp;
	  }	 	  
	  
	  public  StreamResult transform(Document doc) throws TransformerException {
		    OutputStream oStream = new ByteArrayOutputStream();
		    StreamResult result = new StreamResult(oStream);
		    DOMSource source = new DOMSource(doc);
		    TransformerFactory tFact = TransformerFactory.newInstance();
		    tFact.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
		    Transformer trans = tFact.newTransformer();
		    trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		    trans.transform(source, result);

		    return result;
		  }
	  
	  public  String transFormToString(Document doc) throws TransformerException {
		  StringWriter writer = new StringWriter();
		  DOMSource source = new DOMSource(doc);
		  TransformerFactory tFact = TransformerFactory.newInstance();
	      tFact.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
	      Transformer trans = tFact.newTransformer();
		    trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
	      trans.transform(source, new StreamResult(writer));
	      StringBuffer sb = writer.getBuffer(); 
	      return sb.toString();
	  }

}
