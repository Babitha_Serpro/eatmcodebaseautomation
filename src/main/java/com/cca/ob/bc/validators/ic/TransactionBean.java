package com.cca.ob.bc.validators.ic;

public class TransactionBean {
	protected long messageType;
	protected long procCode;
	protected double amount;
	protected String systemTraceNumber;
	protected String dateTime;
	protected long rRNNumbe;
	protected String actCode;
	protected String terminalId;
	protected String mobileNum;
	protected String pin;
	protected String channelId;
	protected String optionalParam;
	
	public TransactionBean() {}
	
	public TransactionBean(String dateTime,String merchantId,String mobileNum,String pin,String obReqID){
		this.messageType=1200;
		this.procCode=920000;
		this.systemTraceNumber="031770";
		this.dateTime=dateTime;
		this.rRNNumbe=6716;
		this.terminalId=merchantId;
		this.mobileNum=mobileNum;
		this.pin=pin;
		this.channelId="CLT";
		this.optionalParam=obReqID;		
	}
	
	
	public long getMessageType() {
		return messageType;
	}
	public void setMessageType(long messageType) {
		this.messageType = messageType;
	}
	public long getProcCode() {
		return procCode;
	}
	public void setProcCode(long procCode) {
		this.procCode = procCode;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getSystemTraceNumber() {
		return systemTraceNumber;
	}
	public void setSystemTraceNumber(String systemTraceNumber) {
		this.systemTraceNumber = systemTraceNumber;
	}
	public String getDateTime() {
		return dateTime;
	}
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	public long getrRNNumbe() {
		return rRNNumbe;
	}
	public void setrRNNumbe(long rRNNumbe) {
		this.rRNNumbe = rRNNumbe;
	}
	public String getActCode() {
		return actCode;
	}
	public void setActCode(String actCode) {
		this.actCode = actCode;
	}
	public String getTerminalId() {
		return terminalId;
	}
	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}
	public String getMobileNum() {
		return mobileNum;
	}
	public void setMobileNum(String mobileNum) {
		this.mobileNum = mobileNum;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	public String getOptionalParam() {
		return optionalParam;
	}
	public void setOptionalParam(String optionalParam) {
		this.optionalParam = optionalParam;
	}
	
	
	
	
}
