package com.cca.ob.bc.validators.ic;

public class ReversalBean extends TransactionBean{

	private String originalDataElemnts;
	public ReversalBean() {
		super();
	}

	public ReversalBean(String dateTime, String merchantId, String mobileNum, String pin, String obReqID) {
		this.messageType=1420;
		this.procCode=920000;
		this.systemTraceNumber="032179";
		this.dateTime=dateTime;
		this.rRNNumbe=6360;
		this.originalDataElemnts="";
		this.terminalId=merchantId;
		this.mobileNum=mobileNum;
		this.pin=pin;
		this.channelId="CLT";
		this.optionalParam=obReqID;		
	}

	public String getOriginalDataElemnts() {
		return originalDataElemnts;
	}

	public void setOriginalDataElemnts(String originalDataElemnts) {
		this.originalDataElemnts = originalDataElemnts;
	}
	
}
