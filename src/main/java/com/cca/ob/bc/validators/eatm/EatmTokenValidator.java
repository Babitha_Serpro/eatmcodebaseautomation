package com.cca.ob.bc.validators.eatm;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cca.ob.bc.validators.AbstractValidator;
import com.cca.ob.bc.validators.TokenValidationStatusBean;
import com.cca.ob.constants.BCConstants;
import com.cca.ob.dto.TokenValidationBean;
import com.cca.ob.repo.TokenValidationRepo;
import com.cca.ob.util.StringUtils;
@Service
public class EatmTokenValidator extends AbstractValidator{
	private int tokenNumber;
	
	@Value("${token.dayvalidation.max}")
	private int maxTokenValidationAttempts;
	
	@Autowired
	private TokenValidationRepo tokenValidationRepo;
	
	@Override
	public boolean validateRequiredParams(Map<String, String> validationParams, Map<String, String> errorMap) {
		String reqTokenNum=validationParams.get(BCConstants.REQ_PARAM_TOKEN_NUM);
		if(!StringUtils.isNumeric(reqTokenNum)) {
			populateErrors("AG200 " , "Invalid Token Number ",errorMap);
			return false;
		}
		tokenNumber=Integer.parseInt(reqTokenNum);
		return true;
	}

	@Override
	public TokenValidationStatusBean getValidationStatus(Map<String,String> errorMap,Long obReqNum, String merchId, String bcId, String bcRefNum) {
		//TokenValidationRepo tokenValidationRepo=new TokenValidationRepoImpl();
		TokenValidationBean validationBean=tokenValidationRepo.isTokenValid(tokenNumber);
		TokenValidationStatusBean status= getFailedStatusBean(obReqNum, merchId, bcId, bcRefNum);
		if(validationBean==null) {
			return status;
		}
		
		if(validationBean.getValidationAttemptsForTheDay()>maxTokenValidationAttempts) {
			populateErrors("AG120","Token Validation Suspended",errorMap);
			return status;
		}
		status.setAmount(validationBean.getAmount());
		status.setValidationStatus(true);
		return status;
	}

}
