package com.cca.ob.bc.validators.ic;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class ReversalXMLBuilder extends ValidationXMLBuilder{
	protected final String ORIGINAL_DATA_ELEMENTS="OriginalDataElemnts";	
	@Override
	protected void createChildElements(Document doc, Element parentElement, TransactionBean packetData) {
		appendNode(doc, parentElement, MESSAGE_TYPE, packetData.getMessageType(),getMessageTypeAttributes());
		  appendNode(doc, parentElement, PROC_CODE, packetData.getProcCode(),getProcCodeAttributes());
		  appendNode(doc, parentElement, SYST_TRACE, packetData.getSystemTraceNumber(),getSystemTraceNumber());
		  appendNode(doc, parentElement, DATE_TIME, packetData.getDateTime(),getDateTime());
		  appendNode(doc, parentElement, RRNO, packetData.getrRNNumbe(),getRRNum());
		  appendNode(doc, parentElement, TERMINAL_IND, packetData.getTerminalId(),getTerminalId());
		  appendNode(doc, parentElement, ORIGINAL_DATA_ELEMENTS, ((ReversalBean)packetData).getOriginalDataElemnts(),getODEAttributes());
		  appendNode(doc, parentElement, MOBILE_NUM, packetData.getMobileNum(),getMobileNum());
		  appendNode(doc, parentElement, PIN, packetData.getPin(),new HashMap<String, String>());
		  appendNode(doc, parentElement, CHANNEL_IND, packetData.getChannelId(),getChannelId());
		  appendNode(doc, parentElement, RESERVED, packetData.getOptionalParam(),getReserved());
	}
	
	 public  Map<String,String> getODEAttributes(){
		  Map<String,String> mp=new HashMap<String, String>();
		  mp.put("DT", "numeric");
		  mp.put("IND", "56");
		  mp.put("LSP", "2");
		  mp.put("MNL", "1");
		  mp.put("MXL", "99"); 
		  mp.put("PL", "41");
		  mp.put("PRE", "mandatory");
		  return mp;
	  }
	 @Override
	 public  Map<String,String> getMessageTypeAttributes(){
		  Map<String,String> mp=new HashMap<String, String>();
		  mp.put("DEFAULT", "1420");
		  mp.put("DT", "numeric");
		  mp.put("LSP", "0");
		  mp.put("MNL", "4");
		  mp.put("MXL", "4");
		  mp.put("PL", "4");
		  mp.put("PRE", "mandatory");
		  return mp;
	  }
	 public  Map<String,String> getProcCodeAttributes(){
		  Map<String,String> mp=new HashMap<String, String>();
		  mp.put("DT", "numeric");
		  mp.put("IND", "3");
		  mp.put("LSP", "0");
		  mp.put("MNL", "6");
		  mp.put("MXL", "6");
		  mp.put("PL", "6");
		  mp.put("PRE", "mandatory");
		  return mp;
	  }
	 
	  public  Map<String,String> getSystemTraceNumber(){
		  Map<String,String> mp=new HashMap<String, String>();
		  mp.put("DT", "numeric");
		  mp.put("IND", "11");
		  mp.put("LSP", "0");
		  mp.put("MNL", "6");
		  mp.put("MXL", "6");
		  mp.put("PL", "6");
		  mp.put("PRE", "mandatory");
		  return mp;
	  }
	  public  Map<String,String> getDateTime(){
		  Map<String,String> mp=new HashMap<String, String>();
		  mp.put("DT", "numeric");
		  mp.put("IND", "12");
		  mp.put("LSP", "0");
		  mp.put("MNL", "12");
		  mp.put("MXL", "12");
		  mp.put("PL", "12");
		  mp.put("PRE", "mandatory");
		  return mp;
	  }
	  public  Map<String,String> getRRNum(){
		  Map<String,String> mp=new HashMap<String, String>();
		  mp.put("DT", "numeric");
		  mp.put("IND", "37");
		  mp.put("LSP", "0");
		  mp.put("MNL", "12");
		  mp.put("MXL", "12");
		  mp.put("PL", "12");
		  mp.put("PRE", "mandatory");
		  return mp;
	  }
	  public  Map<String,String> getTerminalId(){
		  Map<String,String> mp=new HashMap<String, String>();
		  mp.put("DT", "alphabetnumeric");
		  mp.put("IND", "41");
		  mp.put("LSP", "0");
		  mp.put("MNL", "16");
		  mp.put("MXL", "16");
		  mp.put("PL", "16");
		  mp.put("PRE", "mandatory");
		  return mp;
	  }
	  public  Map<String,String> getMobileNum(){
		  Map<String,String> mp=new HashMap<String, String>();
		  mp.put("IND", "102");
		  mp.put("LSP", "2");
		  mp.put("MNL", "1");
		  mp.put("MXL", "99");
		  mp.put("PL", "10");
		  mp.put("PRE", "mandatory");
		  return mp;
	  }	  
	  public  Map<String,String> getChannelId(){
		  Map<String,String> mp=new HashMap<String, String>();
		  mp.put("DT", "alphabetnumeric");
		  mp.put("IND", "123");
		  mp.put("LSP", "3");
		  mp.put("MNL", "1");
		  mp.put("MXL", "999");
		  mp.put("PL", "3");
		  mp.put("PRE", "mandatory");
		  return mp;
	  }	 
	  public  Map<String,String> getReserved(){
		  Map<String,String> mp=new HashMap<String, String>();
		  mp.put("DT", "alphabetnumeric");
		  mp.put("IND", "125");
		  mp.put("LSP", "3");
		  mp.put("MNL", "1");
		  mp.put("MXL", "999");
		  mp.put("PL", "14");
		  mp.put("PRE", "optional");
		  return mp;
	  }	 	  
	  
}
