package com.cca.ob.bc.validators.ic;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ICResponseBean {

	//'private String jsonResponse;
	private JsonObject respObject;

	public ICResponseBean(String jsonResponse) {
		//this.jsonResponse = jsonResponse;
		this.respObject= JsonParser.parseString(jsonResponse).getAsJsonObject();
	}
	
	public String getErrorCode() {
		if(respObject.get("response")!=null) {
			return respObject.get("response").getAsString();
		}
		return null;
	}
	public String getErrorMessage() {
		if(respObject.get("message")!=null) {
			return respObject.get("message").getAsString();
		}
		return null;
	}
	public String getOptionalParameter() {
		if(respObject.get("reserved")!=null) {
			return respObject.get("reserved").getAsString();
		}
		return null;
	}
	public double getAmount() throws Exception{
		if(respObject.get("amount")!=null) {
			String amount= respObject.get("amount").getAsString();
			amount=amount.replaceAll("^0+", "");
			if((!"null".equalsIgnoreCase(amount))&&  amount.length()>0) { 
				return Double.parseDouble(amount)/100;
			}
		}
		return 0.0;
	}
	
	public String getACTCode() throws Exception{
		if(respObject.get("actCode")!=null) {
			return respObject.get("actCode").getAsString();
		}
		return null;
		
	}
}
