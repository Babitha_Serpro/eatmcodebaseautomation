package com.cca.ob.bc.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cca.ob.bc.validators.eatm.EatmTokenValidator;
import com.cca.ob.bc.validators.ic.TransactionValidator;
import com.cca.ob.exception.OBException;
@Component
public class TokenValidatorFactory {
	@Autowired
	private  EatmTokenValidator eatmTokenValidator;
	@Autowired
	private  TransactionValidator iCICITransactionValidator;
	
	public  Validator getTransactionValidator(String identifier) {
		if(TokenValidatorsEnum.ICICI.getValidator().equalsIgnoreCase(identifier)) {
			return iCICITransactionValidator;
		}else if(TokenValidatorsEnum.EATM.getValidator().equalsIgnoreCase(identifier)) {
			return eatmTokenValidator;
		}
		throw new OBException("Token Validators not existing for the key"+identifier);
	}
}
