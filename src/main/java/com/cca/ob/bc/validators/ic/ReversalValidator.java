package com.cca.ob.bc.validators.ic;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cca.ob.util.DateUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
@Service
public class ReversalValidator extends AbstractICICIValidator{
	@Value("${ic.restgw.reversal.endpoint}")
	private String reversalEndpoint;
	
	protected  String preparePayload(String merchId,String obReqID,String pn6,String pn4,String mobNum ) {
		String dateime=DateUtil.getISTDateTime();
		String pin=new StringBuilder()
				.append(pn6)
				.append("|")
				.append(pn4)
				.toString();
//
//
		JsonObject object = new JsonObject();
		object.addProperty("messageType", "1420");
		object.addProperty("systemTraceNumber", "032179");
		object.addProperty("dateTime", dateime);
		object.addProperty("rrnNumber", obReqID);
		merchId=getTerminalID(merchId);
		object.addProperty("terminalId", merchId);
		object.addProperty("originalDataElemnts", "");
		object.addProperty("mobileNo", mobNum);
		object.addProperty("pin", pin);
		object.addProperty("channelId", "CLT");
		object.addProperty("reserved", obReqID);
		return new Gson().toJson(object);
	}

	@Override
	protected String getEndpoint() {
		return reversalEndpoint;
	}
}
