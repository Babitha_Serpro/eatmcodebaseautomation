package com.cca.ob.bc.validators.ic;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cca.ob.util.DateUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

@Service
public class TransactionValidator extends AbstractICICIValidator{
	@Value("${ic.restgw.transaction.endpoint}")
	private String transEndpoint;
	
	protected  String preparePayload(String merchId,String obReqID,String pn6,String pn4,String mobNum )  {
		String dateime=DateUtil.getISTDateTime();
		String pin=new StringBuilder()
				.append(pn6)
				.append("|")
				.append(pn4)
				.toString();
		//String encryptedMob=encrypt(mobNum);
		//TransactionBean packet=new TransactionBean(dateime, merchId, mobNum, pin, obReqID);
		JsonObject object = new JsonObject();
		object.addProperty("messageType", "1200");
		object.addProperty("systemTraceNumber", "031770");
		object.addProperty("dateTime", dateime);
		object.addProperty("rrnNumber", obReqID);
		merchId=getTerminalID(merchId);
		object.addProperty("terminalId", merchId);
		object.addProperty("mobileNo", mobNum);
		object.addProperty("pin", pin);
		object.addProperty("channelId", "CLT");
		object.addProperty("reserved", obReqID);
		return new Gson().toJson(object);
	}

	@Override
	protected String getEndpoint() {
		// TODO Auto-generated method stub
		return transEndpoint;
	}
}
