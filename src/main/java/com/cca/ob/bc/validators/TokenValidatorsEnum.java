package com.cca.ob.bc.validators;

public enum TokenValidatorsEnum {
	ICICI("icici"),EATM("eatm");
	
	private String validator;
	private TokenValidatorsEnum(String validator) {
		this.validator=validator;
	}
	
	public static TokenValidatorsEnum getValidator(String validator) {
		if("icici".equalsIgnoreCase(validator)) {
			return ICICI;
		}else if("eatm".equalsIgnoreCase(validator)){
			return EATM;
		}
		return null;
	}
	
	public String getValidator() {
		return validator;
	}
}
