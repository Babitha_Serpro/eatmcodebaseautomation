package com.cca.ob.bc.validators;

import java.util.Map;

import com.cca.ob.constants.BCConstants;
import com.cca.ob.dto.TokenValidationBean;

public abstract class AbstractValidator implements Validator{

	protected Map<String,String>  populateErrors(String errorCd,String errMsg,Map<String,String> respMap) {
		respMap.put(BCConstants.RESP_PARAM_ERROR_CODE,errorCd);
		respMap.put(BCConstants.RESP_PARAM_ERROR_MESSAGE,errMsg);
		return respMap;
	}
	protected TokenValidationStatusBean getFailedStatusBean(Long obReqNum, String merchId, String bcId, String bcRefNum) {
		TokenValidationStatusBean status=new TokenValidationStatusBean(obReqNum, merchId, bcId, bcRefNum);
			status.setAmount(0);
			status.setValidationStatus(false);
			return status;
	}
}
