package com.cca.ob.bc.validators;

import java.util.Map;

public interface Validator {
	public boolean validateRequiredParams(Map<String, String> validationParams,Map<String,String> errorMap);
	public TokenValidationStatusBean getValidationStatus(Map<String,String> errorMap,Long obReqNum, String merchId, String bcId, String bcRefNum);
}
