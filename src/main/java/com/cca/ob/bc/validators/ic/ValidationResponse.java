package com.cca.ob.bc.validators.ic;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.cca.ob.exception.OBException;
import com.cca.ob.xmutils.XPathUtil;

public class ValidationResponse extends XPathUtil {
	  private static final Logger LOGGER = LogManager.getLogger(ValidationResponse.class);
	  
	public TransactionBean parseResponse() {
		TransactionBean response=new TransactionBean();
		
		 //String expression = "/XML/MessageType";
		 String value;
		 XPath xPath = XPathFactory.newInstance().newXPath();
	    try {
	    	value=evaluateExpression(xPath, "/XML/MessageType");
	    	response.setMessageType(Long.parseLong(value.trim()));
//System.out.println("==================Value "+value);	    	
	    	value=evaluateExpression(xPath, "/XML/ProcCode");
	    	response.setProcCode(Long.parseLong(value.trim()));
	    	
	    	value=evaluateExpression(xPath, "/XML/Amount");
	    	response.setAmount(Double.parseDouble(value.trim())/100);
	    	
	    	value=evaluateExpression(xPath, "/XML/SystemTraceNumber");
	    	response.setSystemTraceNumber(value.trim());	    	
	    	
	    	value=evaluateExpression(xPath, "/XML/RRNNumber");
	    	response.setrRNNumbe(Long.parseLong(value.trim()));	    	

	    	value=evaluateExpression(xPath, "/XML/TerminalId");
	    	response.setTerminalId(value.trim());	 
	    	
	    	value=evaluateExpression(xPath, "/XML/MobileNo");
	    	//System.out.println("==================Value "+value);		    	
	    	response.setMobileNum(value.trim());	  
	    	
	    	value=evaluateExpression(xPath, "/XML/ChannelId");
	    	response.setChannelId(value.trim());	
	    	
	    	value=evaluateExpression(xPath, "/XML/Reserved");
	    	response.setOptionalParam(value.trim());
	    	
	    	value=evaluateExpression(xPath, "/XML/ActCode");
	    	response.setActCode(value.trim());	  
	    	
	    	value=evaluateExpression(xPath, "/XML/DateTime");
	    	response.setDateTime(value.trim());	  
	    	

	    	value=evaluateExpression(xPath, "/XML/Pin");
	    	response.setPin(value.trim());	  
	    	
	    } catch (Exception e) {
	    	e.printStackTrace();
	      throw new OBException("Error while pulling the Entity List", e);
	      //
	      //@Todo Log the XML to db
	    }
	    
	    return response;
	}
	
	private String evaluateExpression(XPath xPath,String expression) throws XPathExpressionException {
		
		 NodeList  nodeList = (NodeList)xPath.compile(expression).evaluate(getDocument(),
  	          XPathConstants.NODESET);
		 Node node=  nodeList.item(0);
		 if(node==null || node.getFirstChild()==null) {
			 return "";
		 }
		 return node.getFirstChild().getNodeValue();
	}
}
