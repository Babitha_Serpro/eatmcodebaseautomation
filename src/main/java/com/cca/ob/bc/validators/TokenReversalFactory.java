package com.cca.ob.bc.validators;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cca.ob.bc.validators.eatm.EatmTokenValidator;
import com.cca.ob.bc.validators.ic.ReversalValidator;
import com.cca.ob.bc.validators.ic.TransactionValidator;
import com.cca.ob.exception.OBException;
@Component
public class TokenReversalFactory {

	
	@Autowired
	private  ReversalValidator iCICIReversalValidator;
	
	public  Validator getReversalValidator(String identifier) {
		if(TokenValidatorsEnum.ICICI.getValidator().equalsIgnoreCase(identifier)) {
			return iCICIReversalValidator;
		}else if(TokenValidatorsEnum.EATM.getValidator().equalsIgnoreCase(identifier)) {
			return null;
		}
		throw new OBException("Token Validators not existing for the key"+identifier);
	}

}
