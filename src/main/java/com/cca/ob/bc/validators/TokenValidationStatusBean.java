package com.cca.ob.bc.validators;

public class TokenValidationStatusBean {
	private long obReqNum;
	private String merchId;
	private String bcId;
	private String bcRefNum;
	private double amount;
	private boolean validationStatus;
	
	public TokenValidationStatusBean() {}
	
	public TokenValidationStatusBean(long obReqNum, String merchId, String bcId, String bcRefNum) {
		super();
		this.obReqNum = obReqNum;
		this.merchId = merchId;
		this.bcId = bcId;
		this.bcRefNum = bcRefNum;
	}
	
	public long getObReqNum() {
		return obReqNum;
	}
	public void setObReqNum(long obReqNum) {
		this.obReqNum = obReqNum;
	}
	public String getMerchId() {
		return merchId;
	}
	public void setMerchId(String merchId) {
		this.merchId = merchId;
	}
	public String getBcId() {
		return bcId;
	}
	public void setBcId(String bcId) {
		this.bcId = bcId;
	}
	public String getBcRefNum() {
		return bcRefNum;
	}
	public void setBcRefNum(String bcRefNum) {
		this.bcRefNum = bcRefNum;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public boolean isValidationStatus() {
		return validationStatus;
	}
	public void setValidationStatus(boolean validationStatus) {
		this.validationStatus = validationStatus;
	}
	
}
