package com.cca.ob.bc.validators.ic;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import com.cca.ob.bc.validators.AbstractValidator;
import com.cca.ob.bc.validators.BCRequestParamKeyConstants;
import com.cca.ob.bc.validators.TokenValidationStatusBean;
import com.cca.ob.constants.BCConstants;
import com.cca.ob.secure.ic.ICICICryptoUtil;
import com.cca.ob.util.HttpExecutor;
import com.cca.ob.util.HttpResponse;
import com.cca.ob.util.StringUtils;

public abstract class AbstractICICIValidator extends  AbstractValidator{
	
	private String pin4;
	private String pin6;
	private String mobileNum;
	private static Logger logger=LogManager.getLogger(AbstractICICIValidator.class);
	
	@Value("${ic.restgw.apikey}")
	private String apiKey;	
	@Override
	public boolean validateRequiredParams(Map<String, String> validationParams,Map<String,String> errorMap) {
		pin4=validationParams.get(BCRequestParamKeyConstants.ICICI_4DIGIT_PIN);
		pin6=validationParams.get(BCRequestParamKeyConstants.ICICI_6DIGIT_PIN);
		mobileNum=validationParams.get(BCConstants.REQUEST_PARAM_USER_MOBILE_NUM);				
		if(! validateNumericField(pin4,4)) {
			populateErrors("AG200" , "Incorrect 4 digit temporary code",errorMap);
			return false;
		}
		if(! validateNumericField(pin6,6)) {
			populateErrors("AG200" , "Incorrect 6 digit reference code ",errorMap);
			return false;
		}
		if(! validateNumericField(mobileNum,10)) {
			populateErrors("AG200" , "Incorrect Mobile Num ",errorMap);
			return false;
		}
		return true;
	}

	@Override
	public TokenValidationStatusBean getValidationStatus(Map<String,String> errorMap,Long obReqNum, String merchId, String bcId, String bcRefNum)  {
		String response=null;
		String decryptedResp=null;
		try {
			String payLoad=preparePayload( merchId, obReqNum.toString(),pin6,pin4,mobileNum);
			
			//@Todo //Delete logging of response line
			logger.debug("Request Payload UnEncrypted "+payLoad);
			String encryptedReq=ICICICryptoUtil.encryptPayload(payLoad);
			
			logger.debug("Request Payload Encrypted  for bcRefNum ="+bcRefNum+"  obReqNum ="+obReqNum+" \n"+encryptedReq);
			
			HttpExecutor executor=new HttpExecutor();
			HttpResponse httpResp=executor.postPayload(getEndpoint(), encryptedReq,getHeaderMap());
			response=httpResp.getResponseAsString();
			response=response.replaceAll("\\r|\\n", "");
			//response=hardcodedresponse;
			logger.debug("Encrypted Response from the Server for bcRefNum ="+bcRefNum+"  obReqNum ="+obReqNum+"Status Code "+httpResp.getStatusCode()+" \n"+response);
			decryptedResp=ICICICryptoUtil.decryptResponse(response);
			logger.debug("Decrypted Response from the bank"+bcRefNum+"  obReqNum ="+obReqNum+" \n"+decryptedResp);
			//@Todo //Delete logging of response line
			
		} catch (Exception e) {
			logger.error("Error while getting the response from the BC Server for bcRefNum ="+bcRefNum+"  obReqNum ="+obReqNum+"\n", e);
			populateErrors("AG301" , "Error while parsing response ",errorMap);
			return null;
		}
		if(decryptedResp==null) {
			populateErrors("AG306","Problem In connecting to Server for bcRefNum ="+bcRefNum+"  obReqNum ="+obReqNum+"\n",errorMap);
			return null;
		}

		return getStatus( obReqNum,  merchId,  bcId,  bcRefNum,decryptedResp,errorMap);
	}



	protected abstract String preparePayload(String merchId,String obReqID,String pn6,String pn4,String mobNum ) throws Exception; 
	
	protected abstract String getEndpoint();
	
	private Map<String,String> getHeaderMap(){
		Map<String,String> headerMap=new HashMap<String, String>();
		headerMap.put("Content-Type", "text/plain");
		headerMap.put("apikey", apiKey);
		return headerMap;
	}
	
	private TokenValidationStatusBean getStatus(long obReqNum, String merchId, String bcId, String bcRefNum,String response,Map<String,String> errorMap) {
		TokenValidationStatusBean status= new TokenValidationStatusBean( obReqNum,  merchId,  bcId,  bcRefNum);
		status.setAmount(0);
		status.setValidationStatus(false);
		if(response!=null) {
			try {
				ICResponseBean icRespBean=new ICResponseBean(response);
				if(icRespBean.getErrorCode()!=null) {
					populateErrors("IC300" , "Validator Error "+icRespBean.getErrorCode()+" "+icRespBean.getErrorMessage(),errorMap);
					return status;
				}
				String actCode=icRespBean.getACTCode();
				double amount=icRespBean.getAmount();
				populateResponseData(actCode,amount,obReqNum,status,errorMap);
			}catch(Exception e) {
				logger.error("Error occurred while parsing the response ",e);
				populateErrors("AG300" , "Error while parsing response ",errorMap);
			}
			
		}
		return status;
	}
	
	private void populateResponseData(String actCode,double amount, long obReqNum, TokenValidationStatusBean status, Map<String, String> errorMap) {
		logger.info("ObReqNum = "+obReqNum+" ACTCode ="+actCode+" Amount ="+amount);
		status.setObReqNum(obReqNum);
		if(actCode!=null && "000".equals(actCode.trim())) {
			status.setAmount(amount);
			status.setValidationStatus(true);
		}else {
			switch(Integer.parseInt(actCode)) {
			case 70:
				populateErrors("AC70" , "Invalid Length of Data elements",errorMap);
				break;
			case 71:
				populateErrors("AC71" , "Data mismatch i.e 4 digit temporary code/6 digit reference code/mobile number during transaction or Record Not found during reversal",errorMap);
				break;
			case 111:
				populateErrors("AC111" , "Account level issues during transaction or data mismatch i.e 4 digit temporary code/6 digit reference code/mobile number during reversal",errorMap);
				break;
			case 907:
				populateErrors("AC907" , "Account level decline during Reversal",errorMap);
				break;
			case 91:
				populateErrors("AC91" , "Transaction timed out at Icore.",errorMap);
				break;
			case 73:
				populateErrors("AC73" , "Amount >1000",errorMap);
				break;	
			default:
				populateErrors("AG299" , "Unknown ActCode:"+actCode,errorMap);	
			}
		}
	}

	private boolean validateNumericField(String pin,int expLength) {
		if(pin!=null && pin.length()==expLength && StringUtils.isNumeric(pin)) {
			return true;
		}
		return false;
	}
	
	public String getTerminalID(String merchId) {
		if(merchId!=null && merchId.length()>8) {
			merchId=merchId.substring(merchId.length()-8, merchId.length());
		}
		return merchId;
	}
}
