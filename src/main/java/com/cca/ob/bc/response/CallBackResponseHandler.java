package com.cca.ob.bc.response;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cca.ob.constants.BCConstants;
import com.cca.ob.dto.BCDetails;
import com.cca.ob.repo.TokenValidationRepo;
import com.cca.ob.util.HttpExecutor;
import com.cca.ob.util.ResponseUtils;
@Service
public class CallBackResponseHandler {
	@Autowired
	private TokenValidationRepo tokenValidationRepoImpl;
	private static Logger logger=LogManager.getLogger(CallBackResponseHandler.class);
	public void postResponseToCallBackURL(BCDetails aggregator,Map<String,String> plainResponse,final String reqType) {
		//post the response back to the callback url
		//Update the status of post to the db also
		String respKey= aggregator==null ? null :aggregator.getRespKey();
		Map<String,String> enryptedResponsee=ResponseUtils.getEncryptedResponse(plainResponse,respKey);
		boolean success=false;
		if(aggregator!=null) {
			String callBack=null;
			if("T".equalsIgnoreCase(reqType)) {
				callBack=aggregator.getValidatorCallBackURL();
			}else if("R".equalsIgnoreCase(reqType)) {
				callBack=aggregator.getReversalCallBackURL();
			}
			
			try {
				
				logger.info("Posting the response "+enryptedResponsee+" \n to the Call back URL "+callBack);
				new HttpExecutor().doPost(callBack, enryptedResponsee);
				success=true;
			}catch(Exception e) {
				logger.error("Posting to the CallBackURL Encountered a problem ",e);
				//Do nothing
			}
		}
		final Boolean flag=new Boolean(success);
		new Thread(()->logResponse(plainResponse,enryptedResponsee.get(BCConstants.RESP_PARAM_ENCRYPTED_RESPONSE),reqType,flag)).start();
	}
	
	
	private void logResponse(Map<String, String> response,String encryptedResp,String reqType,boolean postSuccesful) {
		Float amount=response.get(BCConstants.RESP_PARAM_AMOUNT)== null? null : Float.valueOf(response.get(BCConstants.RESP_PARAM_AMOUNT));
		Boolean dispenseCash=Boolean.valueOf(response.get(BCConstants.RESP_PARAM_DISPENSE_CASH));
		Boolean reversalSuccess=Boolean.valueOf(response.get(BCConstants.RESP_PARAM_REVERSAL_SUCCESS));
		boolean validationSuccess=false;
		validationSuccess=(dispenseCash!=null && dispenseCash) ||
							(reversalSuccess!=null && reversalSuccess);
		logger.debug("Logging the response to the db status = "+response.get(BCConstants.RESP_PARAM_DISPENSE_CASH));
		try {
			tokenValidationRepoImpl.logResponse(Long.valueOf(response.get(BCConstants.RESP_PARAM_EATM_REF_NUM)),reqType,response.get(BCConstants.RESP_PARAM_ERROR_CODE),
					response.get(BCConstants.RESP_PARAM_ERROR_MESSAGE),validationSuccess,
					amount,postSuccesful);
			logger.debug("Successfully logged the response to db");
		}catch(Exception e) {
			logger.error("Unable to log the Response Details to BC tables ",e);
		}
		try {
			tokenValidationRepoImpl.logResponseDump(Long.valueOf(response.get(BCConstants.RESP_PARAM_EATM_REF_NUM)), response.get(BCConstants.REQ_PARAM_BC_REF_NUM), reqType, encryptedResp);
			logger.debug("Successfully logged the response dump to db");
		}catch(Exception e) {
			logger.error("Unable to log the Response dump to db ",e);
		}
		
	}
	
	
	
}
