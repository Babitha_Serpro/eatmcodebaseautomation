package com.cca.ob.bc.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cca.ob.dto.EncryptedRequestBean;
import com.cca.ob.dto.EncryptedResponseBean;
import com.cca.ob.service.DispensedStatusService;
import com.cca.ob.service.TokenValidationService;
@RestController
@RequestMapping("/token")
public class DispensedStatusController {
	
	@Autowired
	private DispensedStatusService dispensedStatusService;
	
	private static Logger logger=LogManager.getLogger(DispensedStatusController.class);
	
	@PostMapping(path = "/Dispensed")
	public ResponseEntity<EncryptedResponseBean> updateStatus(@RequestBody EncryptedRequestBean encReq, HttpServletRequest request) {
		EncryptedResponseBean resp= dispensedStatusService.updateDispensedStatus(encReq, request);
		return ResponseEntity.ok(resp);
	}

}
