package com.cca.ob.bc.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cca.ob.constants.BCConstants;
import com.cca.ob.dto.BCDetails;
import com.cca.ob.dto.EncryptedRequestBean;
import com.cca.ob.dto.EncryptedResponseBean;
import com.cca.ob.service.DispensedStatusService;
import com.cca.ob.service.MerchantDataService;
import com.cca.ob.service.TokenValidationService;
@RestController
@RequestMapping("/merchantData")
public class MerchantDataController {


	@Autowired
	private MerchantDataService merchantDataService;

	private static Logger logger=LogManager.getLogger(MerchantDataController.class);

	@PostMapping(path = "/add")
	public ResponseEntity<Map<String,String>> insertMerchantDetails(@RequestBody EncryptedRequestBean encReq, HttpServletRequest request) {
		Map<String,String> resp= merchantDataService.insertMerchantDetails(encReq, request);
		return ResponseEntity.ok(resp);
	}

	@PostMapping(path = "/update")
	public ResponseEntity<Map<String,String>> updateMerchantDetails(@RequestBody EncryptedRequestBean encReq, HttpServletRequest request) {
		Map<String,String> resp= merchantDataService.updateMerchantDetails(encReq, request);
		return ResponseEntity.ok(resp);
	}
}
