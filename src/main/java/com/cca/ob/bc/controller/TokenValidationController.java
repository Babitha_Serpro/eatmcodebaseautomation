package com.cca.ob.bc.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cca.ob.constants.BCConstants;
import com.cca.ob.dto.BCDetails;
import com.cca.ob.service.TokenValidationService;
import com.cca.ob.util.RequestIPUtil;
@Controller
@RequestMapping("/token")
public class TokenValidationController {

	@Autowired
	private TokenValidationService tokenValidationService;
	private static Logger logger=LogManager.getLogger(TokenValidationController.class);
	
	@GetMapping(path = "/Validate")
	@ResponseBody
	public ModelAndView  validateToken(final @RequestParam Map<String, String> requestParams,ModelMap model, 
			HttpServletRequest request,HttpServletResponse response) throws IOException{
		String remoteIp=RequestIPUtil.getRemoteIP(request);
		logger.info("Token validation request received from ip"+remoteIp);
		String accessCode=requestParams.get(BCConstants.REQ_PARAM_ACCESS_CODE);
		BCDetails aggregator=tokenValidationService.getAggregator(accessCode);
		long dumpID=tokenValidationService.logRequest(requestParams,remoteIp,accessCode);
		Map<String,String> plainRequest=tokenValidationService.getPlainRequest(requestParams,accessCode,aggregator,remoteIp);
		
		
		new Thread(()->tokenValidationService.validateTokens(plainRequest,accessCode,aggregator,dumpID)).start();
		String redirctURL=plainRequest.get(BCConstants.REQ_PARAM_REDIRECTION_URL);
		if(redirctURL==null) {
			logger.info("Token validation Redirecting ? "+false);
			response.sendError( HttpServletResponse.SC_NO_CONTENT);
			return null;
		}else {
			logger.info("Token validation Redirecting to  "+redirctURL);
			model.addAttribute(BCConstants.REDIRCTION_ATTRIBUTE, "true");
			return new ModelAndView("redirect:"+redirctURL,model);
		}
		
	}

}
