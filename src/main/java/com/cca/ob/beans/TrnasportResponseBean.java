package com.cca.ob.beans;

public class TrnasportResponseBean {
	private long requestId;
	private String ecryptedResponse;
	private String errorCd;
	private String errorMsg;
	
	
	
	public long getRequestId() {
		return requestId;
	}
	public void setRequestId(long requestId) {
		this.requestId = requestId;
	}
	public String getEcryptedResponse() {
		return ecryptedResponse;
	}
	public void setEcryptedResponse(String ecryptedResponse) {
		this.ecryptedResponse = ecryptedResponse;
	}
	public String getErrorCd() {
		return errorCd;
	}
	public void setErrorCd(String errorCd) {
		this.errorCd = errorCd;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	
	
}
