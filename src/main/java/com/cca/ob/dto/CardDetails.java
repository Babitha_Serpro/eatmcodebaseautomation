package com.cca.ob.dto;

import com.cca.ob.enums.GatewayStatusMessage;

public class CardDetails {
	private long cardRefNum;
	private String cardNumber;
	private String maskedCardNum;
	private String cvv;
	private String expiryMonth;
	private String expiryYear;
	private String pennyTransactionStatus;
	private boolean encrypted=false;
	//private boolean initPennyTransaction;
	
	public CardDetails(String cardNumber, String expiryMonth, String expiryYear) {
		super();
		this.cardNumber = cardNumber;
		this.expiryMonth = expiryMonth;
		this.expiryYear = expiryYear;
	}
	
	public CardDetails(long cardRefNum,String cardNumber, String expiryMonth, String expiryYear, String pennyTransactionStatus,boolean isEncrypted) {
		super();
		this.cardRefNum=cardRefNum;
		this.cardNumber = cardNumber;
		this.expiryMonth = expiryMonth;
		this.expiryYear = expiryYear;
		this.pennyTransactionStatus = pennyTransactionStatus;
		this.encrypted=isEncrypted;
	}

	public CardDetails(long cardRefNum, String cardNumber, String maskedCardNum, String cvv, String expiryMonth,
			String expiryYear) {
		super();
		this.cardRefNum = cardRefNum;
		this.cardNumber = cardNumber;
		this.maskedCardNum = maskedCardNum;
		this.cvv = cvv;
		this.expiryMonth = expiryMonth;
		this.expiryYear = expiryYear;
	}

	public CardDetails() {
		// TODO Auto-generated constructor stub
	}

	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getCvv() {
		return cvv;
	}
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	public String getExpiryMonth() {
		return expiryMonth;
	}
	public void setExpiryMonth(String expiryMonth) {
		this.expiryMonth = expiryMonth;
	}
	public String getExpiryYear() {
		return expiryYear;
	}
	public void setExpiryYear(String expiryYear) {
		this.expiryYear = expiryYear;
	}

	public String getMaskedCardNum() {
		return maskedCardNum;
	}

	public void setMaskedCardNum(String maskedCardNum) {
		this.maskedCardNum = maskedCardNum;
	}

	public long getCardRefNum() {
		return cardRefNum;
	}

	public void setCardRefNum(long cardRefNum) {
		this.cardRefNum = cardRefNum;
	}

	/*
	 * public String getPennyTransactionStatus() { return pennyTransactionStatus; }
	 */

	public void setPennyTransactionStatus(String pennyTransactionStatus) {
		this.pennyTransactionStatus = pennyTransactionStatus;
	}

	public boolean isPennyTransactionPending() {
		if(pennyTransactionStatus==null) {
			return true;
		}else if(GatewayStatusMessage.getStatusMessage(pennyTransactionStatus)!=null && 
				"Success".equalsIgnoreCase(GatewayStatusMessage.getStatusMessage(pennyTransactionStatus).getStatus())) {
			return false;
		}
		return true;
	}

	public boolean isEncrypted() {
		return encrypted;
	}

	public void setEncrypted(boolean encrypted) {
		this.encrypted = encrypted;
	}

	/*
	 * public void setInitPennyTransaction(boolean initPennyTransaction) {
	 * this.initPennyTransaction = initPennyTransaction; }
	 */
}
	
	
	
	
	
	
	
