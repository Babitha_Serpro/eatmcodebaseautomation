package com.cca.ob.dto;

public class CardDeletionResponseBean extends CardDetailsResponseDTO{
	private boolean deletionSuccess;

	public boolean isDeletionSuccess() {
		return deletionSuccess;
	}

	public void setDeletionSuccess(boolean deletionSuccess) {
		this.deletionSuccess = deletionSuccess;
	}
	
}
