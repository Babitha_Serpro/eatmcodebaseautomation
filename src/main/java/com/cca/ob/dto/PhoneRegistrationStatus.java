package com.cca.ob.dto;

public class PhoneRegistrationStatus {
	private String authKey;
	private boolean duplicatePhoneNumber;
	private CustomerInfo customerInfo;
	
	
	public PhoneRegistrationStatus(String authKey, boolean duplicatePhoneNumber, CustomerInfo customerInfo) {
		super();
		this.authKey = authKey;
		this.duplicatePhoneNumber = duplicatePhoneNumber;
		this.customerInfo = customerInfo;
	}
	public PhoneRegistrationStatus(String authKey, boolean duplicatePhoneNumber) {
		super();
		this.authKey = authKey;
		this.duplicatePhoneNumber = duplicatePhoneNumber;
		//this.customerInfo=customerInfo;
	}
	public String getAuthKey() {
		return authKey;
	}
	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}
	public boolean isDuplicatePhoneNumber() {
		return duplicatePhoneNumber;
	}
	public void setDuplicatePhoneNumber(boolean duplicatePhoneNumber) {
		this.duplicatePhoneNumber = duplicatePhoneNumber;
	}
	public CustomerInfo getCustomerInfo() {
		return customerInfo;
	}
	public void setCustomerInfo(CustomerInfo customerInfo) {
		this.customerInfo = customerInfo;
	}
	
	
}
