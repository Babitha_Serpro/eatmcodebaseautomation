package com.cca.ob.dto;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class EncryptedRequestBean {
	private String acc_code;
	private String enc_req;
	
	
	public String getAcc_code() {
		return acc_code;
	}
	public void setAcc_code(String acc_code) {
		this.acc_code = acc_code;
	}
	public String getEnc_req() {
		return enc_req;
	}
	public void setEnc_req(String enc_req) {
		this.enc_req = enc_req;
	}
	
	public Map<String,String> getParamMap(){
		 Map<String,String> mp=new HashMap<String, String>();
		 mp.put("acc_code", acc_code);
		 mp.put("enc_req", enc_req);
		 return mp;
	}
}
