package com.cca.ob.dto;

public class CardValidationRequestBean {
	private String cardReferenceNum;
	private String cvv;
	private String mKey;
	private String authKey;
	
	public String getCardReferenceNum() {
		return cardReferenceNum;
	}
	public void setCardReferenceNum(String cardReferenceNum) {
		this.cardReferenceNum = cardReferenceNum;
	}
	public String getCvv() {
		return cvv;
	}
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	public String getmKey() {
		return mKey;
	}
	public void setmKey(String mKey) {
		this.mKey = mKey;
	}
	public String getAuthKey() {
		return authKey;
	}
	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}
	
	
}
