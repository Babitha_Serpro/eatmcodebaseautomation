package com.cca.ob.dto;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class MerchantDetailsMini {
//	agent_id,merchant_name,shop_name,mobile1,mobile2,landline,emailid,address,city,state,pincode,landmark,
//	ST_Distance_Sphere(point( SUBSTRING_INDEX(SUBSTRING_INDEX(geo_code,' ',2),' ',-1),SUBSTRING_INDEX(geo_code,' ',1)),point( 28.6236381 ,77.2785218)) RADIAL_DIST,ai_id,country,weekly_off_days,daily_break 
//	,mcat.Merchant_Category_code, mcat.Description, App_Description
	private String merchId;
	private String merchName;
	private String shopName;
	private String pinCode;
	private String city;
//	private String alternativeNumber;
//	private String landline;
	private String emailID;
	private String state;
	private String landMark;
	private String country;
	private String weeklyOffDays;
//	private String merchantCategorycode;
//	private String description;
	private String appDescription;
//	private float mlat;
//	private float mLong;
	private String mlat;
	private String mLong;
//	private double radialDistance;
	private String radialDistance;
	private String dailyBreakStart;
	private String dailyBreakEnd;
	private String openingTime;
	private String closingTime; 
//	private String category;
	private String mobileNumber;
	private String merchAddress;
	private String dailyBreak;
	private String workingHours;
	
	


	/**
	 * @param merchId
	 * @param merchName
	 * @param shopName
	 * @param pinCode
	 * @param city
	 * @param mobileNumber1
	 * @param mobileNumber2
	 * @param landline
	 * @param emailID
	 * @param address
	 * @param state
	 * @param landMark
	 * @param merchAddress
	 * @param country
	 * @param weeklyOffDays
	 * @param dailyBreak
	 * @param merchantCategorycode
	 * @param description
	 * @param appDescription
	 * @param radialDistance
	 */
	public MerchantDetailsMini(String merchId, String merchName, String shopName, String pinCode, String city,
			String mobileNumber, String alternativeNumber, String landline, String emailID,
			String state, String landMark, String merchAddress, String country, String weeklyOffDays,String workingHours, String dailyBreak,
			String merchantCategorycode, String description, String appDescription, String radialDistance,String mlat, String mLong) {
		this.merchId = merchId;
		this.merchName = merchName;
		this.shopName = shopName;
		this.pinCode = pinCode;
		this.city = city;
		this.mobileNumber = mobileNumber;
//		this.alternativeNumber = alternativeNumber;
//		this.landline = landline;
		this.emailID = emailID;
		this.state = state;
		this.landMark = landMark;
		this.merchAddress = merchAddress;
		this.country = country;
		this.weeklyOffDays = weeklyOffDays;
		this.dailyBreak = dailyBreak;
//		this.merchantCategorycode = merchantCategorycode;
//		this.description = description;
		this.appDescription = appDescription;
		this.radialDistance = radialDistance;
		this.mlat=mlat;
		this.mLong=mLong;
		this.workingHours=workingHours;
	}

	
	
//	public MerchantDetailsMini(String merchId, String merchName, String merchAddress, float mlat, float mLong) {
//		super();
//		this.merchId = merchId;
//		this.merchName = merchName;
//		this.merchAddress = merchAddress;
//		this.mlat = mlat;
//		this.mLong = mLong;
//	}
//	
//	public MerchantDetailsMini(String merchId, String merchName, String merchAddress, float mlat, float mLong,
//			double radialDistance) {
//		super();
//		this.merchId = merchId;
//		this.merchName = merchName;
//		this.merchAddress = merchAddress;
//		this.mlat = mlat;
//		this.mLong = mLong;
//		this.radialDistance = radialDistance;
//	}
	
//	public MerchantDetailsMini(String merchId, String merchName, String merchAddress, float mlat, float mLong,
//			double radialDistance,String mobileNumber,String shopName,String pinCode,String city,String category) {
//		this( merchId,  merchName,  merchAddress,  mlat,  mLong,
//			 radialDistance);
//		this.shopName=shopName;
//		this.pinCode=pinCode;
//		this.city=city;
//		this.mobileNumber=mobileNumber;
//		this.category=category;
//	}
	public String getDailyBreakStart() {
		dailyBreakStart=dailyBreak.equals(" - ")?dailyBreak:dailyBreak.split("-")[0];
		return dailyBreakStart;
	}




	public String getDailyBreakEnd() {
		dailyBreakEnd=dailyBreak.equals(" - ")?dailyBreak:dailyBreak.split("-")[1];
		return dailyBreakEnd;
	}



	public String getOpeningTime() {
		openingTime=workingHours.equals(" - ")?workingHours:workingHours.split("-")[0];
		return openingTime;
	}

	public String getClosingTime() {
		closingTime=workingHours.equals(" - ")?workingHours:workingHours.split("-")[1];
		return closingTime;
	}

	public String getMerchId() {
		return merchId;
	}


	public void setMerchId(String merchId) {
		this.merchId = merchId;
	}

	public String timeFormat(String time) {
		int temp=Integer.parseInt(time.substring(0,2));
		if(temp>12)
			time=(temp-12)+":"+time.substring(2,4)+" PM";
		else
			time=temp+":"+time.substring(2,4)+" AM";
			
		return time;
	}
	public String getMerchName() {
		return merchName;
	}
	public void setMerchName(String merchName) {
		this.merchName = merchName;
	}
	public String getMerchAddress() {
		return merchAddress;
	}
	public void setMerchAddress(String merchAddress) {
		this.merchAddress = merchAddress;
	}
//	public float getMlat() {
//		return mlat;
//	}
//	public void setMlat(float mlat) {
//		this.mlat = mlat;
//	}
//	public float getmLong() {
//		return mLong;
//	}
//	public void setmLong(float mLong) {
//		this.mLong = mLong;
//	}
	public String getMlat() {
		return mlat;
	}
	public void setMlat(String mlat) {
		this.mlat = mlat;
	}
	public String getmLong() {
		return mLong;
	}
	public void setmLong(String mLong) {
		this.mLong = mLong;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

//	public double getRadialDistance() {
//		
//			
//		return radialDistance;
//	}
//	
//	public void setRadialDistance(double radialDistance) {
//	
//		this.radialDistance = radialDistance;
//	}
	public String getRadialDistance() {

		Float litersOfPetrol=Float.parseFloat(radialDistance);
		DecimalFormat df = new DecimalFormat("0.00");
		df.setMaximumFractionDigits(3);
		radialDistance = df.format(litersOfPetrol);
		System.out.println("string liters of petrol putting in preferences is "+radialDistance);
		if(Double.parseDouble(radialDistance)<1)		
			radialDistance=Double.parseDouble(radialDistance)*1000+" M";
		else
			radialDistance=radialDistance+" KM";
		
		return radialDistance;
	}
	
	public void setRadialDistance(String radialDistance) {
		this.radialDistance = radialDistance;
	}

//	public String getCategory() {
//		return category;
//	}
//
//	public void setCategory(String category) {
//		this.category = category;
//	}
//	



//	public String getalternativeNumber() {
//		return alternativeNumber;
//	}
//
//
//	public void setalternativeNumber(String alternativeNumber) {
//		this.alternativeNumber = alternativeNumber;
//	}
//
//
//	public String getLandline() {
//		return landline;
//	}
//

//	public void setLandline(String landline) {
//		this.landline = landline;
//	}


	public String getEmailID() {
		return emailID;
	}


	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public String getLandMark() {
		return landMark;
	}


	public void setLandMark(String landMark) {
		this.landMark = landMark;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getWorkingHours() {
		System.out.println(workingHours);
		if(workingHours.equals(" - "))
			return workingHours;
		String time[]=workingHours.split("-");
		workingHours="Opens "+timeFormat(time[0])+"-Closes "+timeFormat(time[1]);
		return workingHours;
	}



	public void setWorkingHours(String workingHours) {
		this.workingHours = workingHours;
	}


	public String getWeeklyOffDays() {
		return weeklyOffDays;
	}


	public void setWeeklyOffDays(String weeklyOffDays) {
		this.weeklyOffDays = weeklyOffDays;
	}


	public String getDailyBreak() {
		if(dailyBreak.equals(" - "))
			return dailyBreak;
		String time[]=dailyBreak.split("-");
		dailyBreak=timeFormat(time[0])+"-"+timeFormat(time[1]);
		return dailyBreak;
	}


	public void setDailyBreak(String dailyBreak) {
		String time[]=dailyBreak.split("-");
		dailyBreak=timeFormat(time[0])+"-"+timeFormat(time[1]);
		this.dailyBreak = dailyBreak;
	}


//	public String getMerchantCategorycode() {
//		return merchantCategorycode;
//	}
//
//
//	public void setMerchantCategorycode(String merchantCategorycode) {
//		this.merchantCategorycode = merchantCategorycode;
//	}
//
//
//	public String getDescription() {
//		return description;
//	}
//
//
//	public void setDescription(String description) {
//		this.description = description;
//	}


	public String getAppDescription() {
		return appDescription;
	}


	public void setAppDescription(String appDescription) {
		this.appDescription = appDescription;
	}
}
