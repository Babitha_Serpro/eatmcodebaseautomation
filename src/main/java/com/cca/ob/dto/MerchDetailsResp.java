package com.cca.ob.dto;

import java.util.ArrayList;
import java.util.List;

public class MerchDetailsResp {
	private List<MerchantDetailsMini> merchantDetails=new ArrayList<MerchantDetailsMini>();
	private String errorCode;
	private String errorMsg;
	
	public List<MerchantDetailsMini> getMerchantDetails() {
		return merchantDetails;
	}

	public void setMerchantDetails(List<MerchantDetailsMini> merchantDetails) {
		this.merchantDetails = merchantDetails;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	
}
