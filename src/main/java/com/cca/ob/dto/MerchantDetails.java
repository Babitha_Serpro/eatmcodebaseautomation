package com.cca.ob.dto;

public class MerchantDetails {

	private String merchID;
	private float lat;
	private float longitude;
	private String address;
	private boolean isEnabled;
	private String aggregatorId;
	private boolean isAggregatorEnabled;
	private String bcId;
	private boolean bcEnabled;
	
	public MerchantDetails(String merchID, float lat, float longitude, String address) {
		super();
		this.merchID = merchID;
		this.lat = lat;
		this.longitude = longitude;
		this.address = address;
	}
	
	public MerchantDetails(String merchID, float lat, float longitude, String address, boolean isEnabled,
			String aggregatorId, boolean isAggregaorEnabled,String bcId,boolean isBcEnabled) {
		super();
		this.merchID = merchID;
		this.lat = lat;
		this.longitude = longitude;
		this.address = address;
		this.isEnabled = isEnabled;
		this.aggregatorId = aggregatorId;
		this.isAggregatorEnabled = isAggregaorEnabled;
		this.bcId=bcId;
		this.bcEnabled=isBcEnabled;
	}

	public String getMerchID() {
		return merchID;
	}

	public void setMerchID(String merchID) {
		this.merchID = merchID;
	}

	public float getLat() {
		return lat;
	}
	public void setLat(float lat) {
		this.lat = lat;
	}
	public float getLongitude() {
		return longitude;
	}
	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

	public boolean isEnabled() {
		return isEnabled;
	}

	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	public String getAggregatorId() {
		return aggregatorId;
	}

	public void setAggregatorId(String aggregatorId) {
		this.aggregatorId = aggregatorId;
	}

	public boolean isAggregatorEnabled() {
		return isAggregatorEnabled;
	}

	public void setAggregatorEnabled(boolean isAggregaorEnabled) {
		this.isAggregatorEnabled = isAggregaorEnabled;
	}

	public String getBcId() {
		return bcId;
	}

	public void setBcId(String bcId) {
		this.bcId = bcId;
	}

	public boolean isBcEnabled() {
		return bcEnabled;
	}

	public void setBcEnabled(boolean bcEnabled) {
		this.bcEnabled = bcEnabled;
	}
	
	
}
