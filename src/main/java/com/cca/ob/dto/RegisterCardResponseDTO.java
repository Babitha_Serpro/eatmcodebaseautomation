package com.cca.ob.dto;

public class RegisterCardResponseDTO {

	private String mKey;
	private String authKey;
	private String errorCode;
	private String errorMsg;
	private String cardRefNum;
	
	public String getmKey() {
		return mKey;
	}
	public void setmKey(String mKey) {
		this.mKey = mKey;
	}
	public String getAuthKey() {
		return authKey;
	}
	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public String getCardRefNum() {
		return cardRefNum;
	}
	public void setCardRefNum(String maskedCardNumber) {
		this.cardRefNum = maskedCardNumber;
	}
	
	
}
