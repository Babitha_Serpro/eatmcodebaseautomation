package com.cca.ob.dto;

public class CCACallBackRequestDTO {
	private String encResp;

	public  String getEncResp() {
		return encResp;
	}

	public void setEncResp(String encResp) {
		this.encResp = encResp;
	}
	
}
