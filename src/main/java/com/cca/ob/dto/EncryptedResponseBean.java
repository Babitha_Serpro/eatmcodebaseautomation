package com.cca.ob.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class EncryptedResponseBean {
	private String enc_resp;

	public String getEnc_resp() {
		return enc_resp;
	}

	public void setEnc_resp(String enc_resp) {
		this.enc_resp = enc_resp;
	}
	
}
