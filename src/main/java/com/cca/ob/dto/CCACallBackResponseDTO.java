package com.cca.ob.dto;

public class CCACallBackResponseDTO {
	
	private String statusMessage;
	private String obTransactionReference;
	private String bankReference;
	private String amount;
	private String currency;
	private boolean orderSuccess;
	
	/*
	 * private String mKey; private String authKey; private String errorCode;
	 * private String errorMsg;
	 */
	
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public String getObTransactionReference() {
		return obTransactionReference;
	}
	public void setObTransactionReference(String obTransactionReference) {
		this.obTransactionReference = obTransactionReference;
	}
	public String getBankReference() {
		return bankReference;
	}
	public void setBankReference(String bankReference) {
		this.bankReference = bankReference;
	}

	/*
	 * public String getmKey() { return mKey; } public void setmKey(String mKey) {
	 * this.mKey = mKey; } public String getAuthKey() { return authKey; } public
	 * void setAuthKey(String authKey) { this.authKey = authKey; } public String
	 * getErrorCode() { return errorCode; } public void setErrorCode(String
	 * errorCode) { this.errorCode = errorCode; } public String getErrorMsg() {
	 * return errorMsg; } public void setErrorMsg(String errorMsg) { this.errorMsg =
	 * errorMsg; }
	 */
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public boolean isOrderSuccess() {
		return orderSuccess;
	}
	public void setOrderSuccess(boolean orderSuccess) {
		this.orderSuccess = orderSuccess;
	}
	
	
}
