package com.cca.ob.dto;

import java.util.ArrayList;
import java.util.List;

public class CustomerInfo {

	private String mNo;
	private String countryCode;
	private String cName;
	private long custId;
	private List<CardDetails> cardList=new ArrayList<CardDetails>();
	
	public CustomerInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CustomerInfo(String countryCode,String mNo, String cName, long custId) {
		super();
		this.mNo = mNo;
		this.countryCode = countryCode;
		this.cName = cName;
		this.custId = custId;
	}

	public long getCustId() {
		return custId;
	}

	public void setCustId(long custId) {
		this.custId = custId;
	}

	public String getmNo() {
		return mNo;
	}
	
	public void setmNo(String mNo) {
		this.mNo = mNo;
	}
	
	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getcName() {
		return cName;
	}
	
	public void setcName(String cName) {
		this.cName = cName;
	}
	
	
	  public void addCard(CardDetails card) { cardList.add(card); }
	  
	  public List<CardDetails> getCardList() { return cardList; }
	 
}
