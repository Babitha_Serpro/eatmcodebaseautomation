package com.cca.ob.dto;

public class CheckStatusRequestDTO {
	private String mKey;
	private String authKey;
	private long obTransId;
	
	public long getObTransId() {
		return obTransId;
	}
	public void setObTransId(long obTransId) {
		this.obTransId = obTransId;
	}
	public String getmKey() {
		return mKey;
	}
	public void setmKey(String mKey) {
		this.mKey = mKey;
	}
	public String getAuthKey() {
		return authKey;
	}
	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}
}
