package com.cca.ob.dto;

public class CardDTO {
	private String maskedCardNumber;
	private String cardIndex;
	private boolean pennyTransactionPending;
	
	public String getMaskedCardNumber() {
		return maskedCardNumber;
	}
	public void setMaskedCardNumber(String maskedCardNumber) {
		this.maskedCardNumber = maskedCardNumber;
	}
	public String getCardIndex() {
		return cardIndex;
	}
	public void setCardIndex(String cardIndex) {
		this.cardIndex = cardIndex;
	}
	public boolean isPennyTransactionPending() {
		return pennyTransactionPending;
	}
	public void setPennyTransactionPending(boolean pennyTransactionPending) {
		this.pennyTransactionPending = pennyTransactionPending;
	}
	
}
