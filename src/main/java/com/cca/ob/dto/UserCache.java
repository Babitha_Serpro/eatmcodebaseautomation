package com.cca.ob.dto;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class UserCache {

	private Map<String,Object> userObjects=new ConcurrentHashMap<String,Object>();
	
	public void store(String key,Object val) {
		userObjects.put(key, val);
	}
	public Object get(String key) {
		return userObjects.get(key);
	}
}
