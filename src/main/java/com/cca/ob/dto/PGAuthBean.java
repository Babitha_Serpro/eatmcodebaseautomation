package com.cca.ob.dto;

public class PGAuthBean {
	private String merchId;
	private String accessCode;
	private String workingKey;
	
	public PGAuthBean(String merchId, String accessCode, String workingKey) {
		super();
		this.merchId = merchId;
		this.accessCode = accessCode;
		this.workingKey = workingKey;
	}
	public String getMerchId() {
		return merchId;
	}
	public void setMerchId(String merchId) {
		this.merchId = merchId;
	}
	public String getAccessCode() {
		return accessCode;
	}
	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}
	public String getWorkingKey() {
		return workingKey;
	}
	public void setWorkingKey(String workingKey) {
		this.workingKey = workingKey;
	}
	
}
