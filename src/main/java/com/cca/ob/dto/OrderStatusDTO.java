package com.cca.ob.dto;

public class OrderStatusDTO {
	
	private String obTransactionReference;
	private String amount;
	private String currency;
	private String orderSuccess;
	private String tokenNum; 
	private long hidTokenIndex;
	
	private String mKey;
	private String authKey;
	private String errorCode;
	private String errorMsg;
	
	public String getObTransactionReference() {
		return obTransactionReference;
	}
	public void setObTransactionReference(String obTransactionReference) {
		this.obTransactionReference = obTransactionReference;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getOrderSuccess() {
		return orderSuccess;
	}
	public void setOrderSuccess(String orderSuccess) {
		this.orderSuccess = orderSuccess;
	}
	public String getTokenNum() {
		return tokenNum;
	}
	public void setTokenNum(String tokenNum) {
		this.tokenNum = tokenNum;
	}
	public String getmKey() {
		return mKey;
	}
	public void setmKey(String mKey) {
		this.mKey = mKey;
	}
	public String getAuthKey() {
		return authKey;
	}
	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public long getHidTokenIndex() {
		return hidTokenIndex;
	}
	public void setHidTokenIndex(long hidTokenIndex) {
		this.hidTokenIndex = hidTokenIndex;
	}
	
	
}
