package com.cca.ob.dto;

import java.util.ArrayList;
import java.util.List;

public class MerchantLocationResponseDTO {
	private String mKey;
	private String authKey;
	private String errorCode;
	private String errorMsg;
	private List<MerchantDetailsMini> merchantDetails=new ArrayList<MerchantDetailsMini>();
	
	public String getmKey() {
		return mKey;
	}
	public void setmKey(String mKey) {
		this.mKey = mKey;
	}
	public String getAuthKey() {
		return authKey;
	}
	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public List<MerchantDetailsMini> getMerchantDetails() {
		return merchantDetails;
	}
	public void setMerchantDetails(List<MerchantDetailsMini> merchantDetails) {
		this.merchantDetails = merchantDetails;
	}
		
		
}
