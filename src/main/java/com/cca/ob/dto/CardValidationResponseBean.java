package com.cca.ob.dto;

public class CardValidationResponseBean {
		private String stpEndpoint;
		private String mKey;
		private long obTransId;
		private String authKey;
		private String errorCode;
		private String errorMsg;
		
		public String getStpEndpoint() {
			return stpEndpoint;
		}
		public void setStpEndpoint(String stpEndpoint) {
			this.stpEndpoint = stpEndpoint;
		}
		public String getmKey() {
			return mKey;
		}
		public void setmKey(String mKey) {
			this.mKey = mKey;
		}
		public String getAuthKey() {
			return authKey;
		}
		public void setAuthKey(String authKey) {
			this.authKey = authKey;
		}
		public String getErrorCode() {
			return errorCode;
		}
		public void setErrorCode(String errorCode) {
			this.errorCode = errorCode;
		}
		public String getErrorMsg() {
			return errorMsg;
		}
		public void setErrorMsg(String errorMsg) {
			this.errorMsg = errorMsg;
		}
		public long getObTransId() {
			return obTransId;
		}
		public void setObTransId(long obTransId) {
			this.obTransId = obTransId;
		}
		
		
}
