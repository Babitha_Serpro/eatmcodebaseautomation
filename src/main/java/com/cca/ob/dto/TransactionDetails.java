package com.cca.ob.dto;

public class TransactionDetails {
	private long transactionId;
	private String trackingId;
	private String bankRefNo;
	private String orderStatus;
	private String statusCode;
	private String statusMessage;
	private String failureMessage;
	private Double amount;
	private String paymentMode;
	private String currency;
	private Long tokenId;
	private Long tokenNumber;
	private boolean isPennyTransaction;
	
	public TransactionDetails() {
		super();
	}

	public TransactionDetails(long transactionId, String trackingId, String bank_refNo, String orderStatus,
			String statusCode, String statusMessage, String failureMessage, double amount, String paymentMode,
			String currency) {
		super();
		this.transactionId = transactionId;
		this.trackingId = trackingId;
		this.bankRefNo = bank_refNo;
		this.orderStatus = orderStatus;
		this.statusCode = statusCode;
		this.statusMessage = statusMessage;
		this.failureMessage = failureMessage;
		this.amount = amount;
		this.paymentMode = paymentMode;
		this.currency = currency;
	}
	
	public long getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	public String getTrackingId() {
		return trackingId;
	}
	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}
	public String getBankRefNo() {
		return bankRefNo;
	}
	public void setBankRefNo(String bank_refNo) {
		this.bankRefNo = bank_refNo;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public String getFailureMessage() {
		return failureMessage;
	}
	public void setFailureMessage(String failureMessage) {
		this.failureMessage = failureMessage;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Long getTokenId() {
		return tokenId;
	}

	public void setTokenId(Long tokenNum) {
		this.tokenId = tokenNum;
	}

	public boolean isPennyTransaction() {
		return isPennyTransaction;
	}

	public void setPennyTransaction(boolean isPennyTransaction) {
		this.isPennyTransaction = isPennyTransaction;
	}

	public Long getTokenNumber() {
		return tokenNumber;
	}

	public void setTokenNumber(Long tokenRefNum) {
		this.tokenNumber = tokenRefNum;
	}
	
}
