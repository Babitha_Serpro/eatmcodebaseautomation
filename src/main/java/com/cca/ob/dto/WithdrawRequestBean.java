package com.cca.ob.dto;

public class WithdrawRequestBean {
	private float amount;
	private String comments;
	private String cardReferenceNum;
	private String cvv;
	private String mKey;
	private String authKey;
	
	
	
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getCardReferenceNum() {
		return cardReferenceNum;
	}
	public void setCardReferenceNum(String cardReferenceNum) {
		this.cardReferenceNum = cardReferenceNum;
	}
	public String getCvv() {
		return cvv;
	}
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	public String getmKey() {
		return mKey;
	}
	public void setmKey(String mKey) {
		this.mKey = mKey;
	}
	public String getAuthKey() {
		return authKey;
	}
	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}
	
	

}
