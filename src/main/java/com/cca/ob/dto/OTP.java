package com.cca.ob.dto;

public class OTP {
	private String oTPKey;
	private String oTPValue;
	
	
	public OTP(String oTPKey, String oTPValue) {
		super();
		this.oTPKey = oTPKey;
		this.oTPValue = oTPValue;
	}
	public String getoTPKey() {
		return oTPKey;
	}
	public void setoTPKey(String oTPKey) {
		this.oTPKey = oTPKey;
	}
	public String getoTPValue() {
		return oTPValue;
	}
	public void setoTPValue(String oTPValue) {
		this.oTPValue = oTPValue;
	}
	
}
