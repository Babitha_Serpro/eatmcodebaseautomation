package com.cca.ob.dto;

public class VAlidateDeviceResponseDTO {
	private String cName;
	private String mKey;
	private String authKey;
	private String errorCode;
	private String errorMsg;
	private boolean hasRegisteredCards;
	
	public String getcName() {
		return cName;
	}
	public void setcName(String cName) {
		this.cName = cName;
	}
	
	public String getmKey() {
		return mKey;
	}
	public void setmKey(String mKey) {
		this.mKey = mKey;
	}
	public String getAuthKey() {
		return authKey;
	}
	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public boolean isHasRegisteredCards() {
		return hasRegisteredCards;
	}
	public void setHasRegisteredCards(boolean hasRegisteredCards) {
		this.hasRegisteredCards = hasRegisteredCards;
	}
	
}
