package com.cca.ob.dto;

public class BCDetails {
	//private String aggretatorId;
	private String bcId;
	private String validatorCallBackURL;
	private String reversalCallBackURL;
	private String reqKey;
	private String respKey;
	private String secretKey;
	
	
	public BCDetails(String bcId, String validationCallBackURL, String reversalCallBackURL,String reqKey, String respKey) {
		super();
		//this.aggretatorId = aggretatorId;
		this.bcId=bcId;
		this.validatorCallBackURL = validationCallBackURL;
		this.reversalCallBackURL = reversalCallBackURL;
		this.reqKey = reqKey;
		this.respKey = respKey;
	}
	public BCDetails(String bcId,String secretKey) {
		super();
		//this.aggretatorId = aggretatorId;
		this.bcId=bcId;
		this.secretKey=secretKey;
	}
	
	public String getSecretKey() {
		return secretKey;
	}
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getValidatorCallBackURL() {
		return validatorCallBackURL;
	}


	public void setValidatorCallBackURL(String validatorCallBackURL) {
		this.validatorCallBackURL = validatorCallBackURL;
	}


	public String getReversalCallBackURL() {
		return reversalCallBackURL;
	}


	public void setReversalCallBackURL(String reversalCallBackURL) {
		this.reversalCallBackURL = reversalCallBackURL;
	}


	public String getReqKey() {
		return reqKey;
	}
	public void setReqKey(String reqKey) {
		this.reqKey = reqKey;
	}
	public String getRespKey() {
		return respKey;
	}
	public void setRespKey(String respKey) {
		this.respKey = respKey;
	}
	public String getBcId() {
		return bcId;
	}
	public void setBcId(String bcId) {
		this.bcId = bcId;
	}
	
}
