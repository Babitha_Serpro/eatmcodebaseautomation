package com.cca.ob.dto;

public class TokenValidationBean {
	private float amount;
	private int validationAttemptsForTheDay;
	
	public TokenValidationBean(float amount, int validationAttemptsForTheDay) {
		super();
		this.amount = amount;
		this.validationAttemptsForTheDay = validationAttemptsForTheDay;
	}
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
	public int getValidationAttemptsForTheDay() {
		return validationAttemptsForTheDay;
	}
	public void setValidationAttemptsForTheDay(int validationAttemptsForTheDay) {
		this.validationAttemptsForTheDay = validationAttemptsForTheDay;
	}
	
}
