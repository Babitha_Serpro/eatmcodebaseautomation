package com.cca.ob.dto;

public class PGEndPoint {
	private long transactionId;
	private String pgEndPoint;
	
	
	public PGEndPoint(long transactionId, String pgEndPoint) {
		super();
		this.transactionId = transactionId;
		this.pgEndPoint = pgEndPoint;
	}
	public long getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	public String getPgEndPoint() {
		return pgEndPoint;
	}
	public void setPgEndPoint(String pgEndPoint) {
		this.pgEndPoint = pgEndPoint;
	}
	
	
}
