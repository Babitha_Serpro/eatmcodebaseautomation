package com.cca.ob.dto;

public class PhoneDetails {
	private String countryCode;
	private String phoneNum;
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public PhoneDetails(String countryCode, String phoneNum) {
		super();
		this.countryCode = countryCode;
		this.phoneNum = phoneNum;
	}
	
	public String getFullyQualifiedPhoneNumber() {
		return countryCode+phoneNum;
	}
}
