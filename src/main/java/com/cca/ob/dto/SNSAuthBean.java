package com.cca.ob.dto;

public class SNSAuthBean {
	private String authId;
	private String password;
	private String metaID;
	
	public SNSAuthBean() {
		super();
	}
	public SNSAuthBean(String authId, String password) {
		super();
		this.authId = authId;
		this.password = password;
	}
	public SNSAuthBean(String authId, String password,String meta) {
		super();
		this.authId = authId;
		this.password = password;
		this.metaID=meta;
	}	
	public String getAuthId() {
		return authId;
	}
	public void setAuthId(String authId) {
		this.authId = authId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getMetaID() {
		return metaID;
	}
	public void setMetaID(String metaID) {
		this.metaID = metaID;
	}
	
}
