package com.cca.ob.dto;

public class TokenValidationResult {
	//private Long validationRequestNumber;
	private Long internalTokenId;
	private float amount;
	public TokenValidationResult(Long internalTokenId, float amount) {
		super();
		this.internalTokenId = internalTokenId;
		this.amount = amount;
	}
	public Long getInternalTokenId() {
		return internalTokenId;
	}
	public void setInternalTokenId(Long internalTokenId) {
		this.internalTokenId = internalTokenId;
	}
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
	
}
