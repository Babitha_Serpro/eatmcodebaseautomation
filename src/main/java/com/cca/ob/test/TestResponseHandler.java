package com.cca.ob.test;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cca.ob.constants.BCConstants;
import com.cca.ob.controller.CCACallBackController;
import com.cca.ob.dto.BCDetails;
import com.cca.ob.repo.BCRepository;
import com.ccavenue.security.AesCryptUtil;

@Controller
@RequestMapping("/test")
public class TestResponseHandler {
	private static Logger logger=LogManager.getLogger(CCACallBackController.class);
	@Autowired
	private BCRepository bCRepositoryImpl;
	@PostMapping(path = "/dummy")
	@ResponseBody
	public void  redirectHandler(@RequestParam Map<String, String> body, HttpServletRequest request) {
		logger.info("Test Redirect is called with parameters "+body);
		BCDetails aggregator=bCRepositoryImpl.getBCDetails("CC01IA06");
		String plainResp=decryptWithAES(body.get(BCConstants.RESP_PARAM_ENCRYPTED_RESPONSE), aggregator.getRespKey());
		logger.info("Plain Response =", plainResp);
	}
	
	private String decryptWithAES(String aesEncryptedesponse,String secretKey) {
		 AesCryptUtil aesUtil=new AesCryptUtil(secretKey);
		 
		try {
			return aesUtil.decrypt(aesEncryptedesponse);
		} catch (Exception e) {
			logger.error("Error while decrypting the xml packet with AES ",e);	
			return null;
		}
	}
}
