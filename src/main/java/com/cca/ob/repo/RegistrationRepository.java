package com.cca.ob.repo;

import com.cca.ob.dto.CustomerInfo;

public interface RegistrationRepository {
	
	public long registerPhone(String contryCode, String phoneNumber,String userName) ;
	public int insertSMSConfirmation(String qualifiedPhoneNum, String provider,String providerRef) ;
	public CustomerInfo getCustomer(String countryCode,String phoneNum);
	public long registerCard(long custId,String cardNum,String expMnth,String expYr);
}
