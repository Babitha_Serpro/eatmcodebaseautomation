package com.cca.ob.repo;

import com.cca.ob.dto.PGAuthBean;
import com.cca.ob.dto.TransactionDetails;

public interface GatewayRepo {
	
	public PGAuthBean readAuth(String identifier);

	public long createTransaction(long cardRefNum, double amount,String currency,String comments);

	public void logRequestPayload(long transactionRefNum, String encodedReq);

	public void insertTransactionDetails(TransactionDetails details, String source);

	public void logResponsePayload(long transactionRefNum, String encodedResp);

	public TransactionDetails getTrasactionDetails(long obTransID);
}
