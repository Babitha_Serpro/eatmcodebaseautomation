package com.cca.ob.repo;

import java.sql.PreparedStatement;
import java.sql.Statement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.cca.ob.dto.PGAuthBean;
import com.cca.ob.dto.TransactionDetails;
@Repository
public class GatewayRepoImpl implements GatewayRepo{
	private Logger logger=LogManager.getLogger(GatewayRepoImpl.class);
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Override
	public PGAuthBean readAuth(String identifier) {
		String sql = "SELECT * FROM OB.PG_AUTH WHERE PG_IDENTIFIER = ?";

        return jdbcTemplate.queryForObject(sql, new Object[]{identifier}, (rs, rowNum) ->
                new PGAuthBean(
                        rs.getString("MERCHANT_ID"),
                        rs.getString("ACCESS_CODE"),
                        rs.getString("WORKING_KEY")
                ));
	}
	
	@Override
	public long createTransaction(long cardRefNum, double amount,String currency,String comments) {
		String insertSql="INSERT INTO OB.PG_TRANSACTION(CARD_ID,REQUEST_AMOUNT,REQUEST_CURRENCY, COMMENTS) values(?,?,?,?)";
		
		//System.out.println("Creating a transaction of Rs "+amount+" with card_id "+cardRefNum);
		KeyHolder keyHolder = new GeneratedKeyHolder();
		 
	    jdbcTemplate.update(connection -> {
	        PreparedStatement ps = connection
	          .prepareStatement(insertSql,Statement.RETURN_GENERATED_KEYS);
	          ps.setLong(1, cardRefNum);
	          ps.setDouble(2, amount);
	          ps.setString(3, currency);
	          ps.setString(4, comments);
	          return ps;
	        }, keyHolder);
	 
	        return ((java.math.BigInteger ) keyHolder.getKey()).longValue();
	}
	
	@Override
	public void logRequestPayload(long transactionRefNum, String encodedReq) {
		 jdbcTemplate.update(
				"INSERT INTO OB.PG_REQ_PAYLOAD(TRANSACTION_ID,ENC_REQUEST) values(?,?)",
				transactionRefNum,encodedReq);
	}

	@Override
	public void insertTransactionDetails(TransactionDetails details,String source) {
		
		String insertSql="INSERT INTO OB.PG_TRANSACTION_DETAILS "+
							"(TRANSACTION_ID,TRACKING_ID,BANK_REF_NO,ORDER_STATUS,STATUS_CODE,"+
							"STATUS_MESSAGE,FAILURE_MESSAGE,AMOUNT,PAYMENT_MODE,CURRENCY,CALLBACK_SOURCE)"+
							"values(?,?,?,?,?,?,?,?,?,?,?)";
		
		 jdbcTemplate.update(
				 insertSql,
				 details.getTransactionId(),details.getTrackingId(),details.getBankRefNo(),details.getOrderStatus(),details.getStatusCode(),
				 details.getStatusMessage(),details.getFailureMessage(),details.getAmount(),details.getPaymentMode(),details.getCurrency(),source);
	}
	
	@Override
	public void logResponsePayload(long transactionRefNum, String encodedResp) {
		 jdbcTemplate.update(
				"INSERT INTO OB.PG_RESP_PAYLOAD(TRANSACTION_ID,ENC_RESPONSE) values(?,?)",
				transactionRefNum,encodedResp);
	}
	
	
	@Override
	public TransactionDetails getTrasactionDetails(long obTransID) {
		
		
		String sql="SELECT PG.TRANSACTION_ID ,PG.TRACKING_ID ,PG.BANK_REF_NO ,PG.ORDER_STATUS ,PG.STATUS_CODE , " + 
				"PG.STATUS_MESSAGE ,PG.FAILURE_MESSAGE ,PG.AMOUNT ,PG.CURRENCY,T.TOKEN_ID,T.TOKEN_NUMBER   FROM OB.PG_TRANSACTION_DETAILS PG " + 
				"LEFT JOIN OB.TOKENS T ON PG.TRANSACTION_ID=T.WITHDRAW_TRANSACT_ID  WHERE PG.TRANSACTION_ID = ?";
		
		TransactionDetails transaCtionDetail=null;
		try {
			transaCtionDetail =(TransactionDetails) jdbcTemplate.queryForObject(
					sql, 
					new Object[]{obTransID}, 
					new BeanPropertyRowMapper<TransactionDetails>(TransactionDetails.class));
		}catch(EmptyResultDataAccessException e) {
			logger.debug("Transaction details not yet available for ObTransId "+obTransID);
			//Simply consume
		}
		return transaCtionDetail;

      
	}
}
