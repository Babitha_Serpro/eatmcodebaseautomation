package com.cca.ob.repo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.cca.ob.dto.SNSAuthBean;
@Repository
public class SNSCredentialsRepoImpl implements SNSCredentialRepo{


	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Override
	public int createAuth(String id, String pass, String identifier) {
		return jdbcTemplate.update(
                "insert into OB.SNS_AUTH (AUTH_ID, AUTH_PASS,AUTH_META) values(?,?,?)",
                id,pass,identifier);
		
	}

	@Override
	public SNSAuthBean readAuth(String identifier) {
		String sql = "SELECT * FROM OB.SNS_AUTH WHERE AUTH_META = ?";

        return jdbcTemplate.queryForObject(sql, new Object[]{identifier}, (rs, rowNum) ->
                new SNSAuthBean(
                        rs.getString("AUTH_ID"),
                        rs.getString("AUTH_PASS")
                ));
	}

}
