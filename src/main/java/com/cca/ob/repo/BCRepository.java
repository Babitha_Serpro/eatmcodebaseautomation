package com.cca.ob.repo;

import java.util.List;

import com.cca.ob.dto.BCDetails;
import com.cca.ob.dto.MerchantDetails;
import com.cca.ob.dto.MerchantDetailsMini;

public interface BCRepository {
	public List<MerchantDetailsMini> getMerchants(float currLat, float currLong, float radiusInKm,int maxRecord);
	public MerchantDetails getMerchant(String merchantId);
	public BCDetails getBCDetails(String accessCode);
	public int insertMerchantDetails(String agentID, String merchantName, String shopName, String merchantCategoryCode, String mobile1, String mobile2, String landline, String emailID, String address, String city, String state, String pincode, String landMark, String geoCode, String aiID, String country, String weeklyOffDays, String workingHours, String dailyBreak);
	public BCDetails getBCDtls(String accessCode);
	public boolean updateMerchantDetails(String agentID, String merchantName, String shopName, String merchantCategoryCode, String mobile1, String mobile2, String landline, String emailID, String address, String city, String state, String pincode, String landMark, String geoCode, String aiID, String country, String weeklyOffDays, String workingHours, String dailyBreak,String isEnabled);

}
