package com.cca.ob.repo;

import java.util.Map;

import com.cca.ob.dto.TokenValidationBean;

public interface TokenValidationRepo {

	public Long logRawRequest(Map<String, String> requestParams, String remoteIp,String accessCode) ;
	public TokenValidationBean isTokenValid(int tokenNumber);
	public boolean consumeToken(Long tokenId);
	public Long tokenValidationRequested(Long reqDumpID,String aggregatorId, String merchantId,String bcID, Integer tokenNum,String bcRefNumber,String reqType,String validatorID,String encMobileNum,Double merLat,Double merchLong,String userMobilePlain, String pin4,String pin6);
	public void logResponse(long eAtmRefNo,String reqType,String errorCode,String errorMsg,Boolean validationSuccess,Float amount,boolean callBackSuccess) ;
	public void logResponseDump(long eAtmRefNo,String bcRefNum,String reqType,String encResponse) ;
	public Long transactionReversalRequested(Long reqDumpID,String aggregatorId, String merchantId,String bcID, Integer tokenNum,String bcRefNumber,String originalBcRefNum,String originalEatmRefNum,String originalReqTimestamp,String reqType,String validatorID,String encMobileNum,Double merLat,Double merchLong,String reversalCode,String reversaReason,String userMobilePlain, String pin4,String pin6);
	public boolean updateDispensatinoStatus(long eAtmRefNo, String bcRefNum,long reqDumId) ;
	public int fetchTransactionsCount(String mobileNum);
	public int fetchBCREFCount(String bcrefnumber);
	public String fetchOriginalBCREFNO(String originaleatmrefnumber);
	public int fetchBCREFNO(String bcrefnumber);
	public int checkData(int data);
	public int fetchBCREFCountT(String bcrefnumber);
}
