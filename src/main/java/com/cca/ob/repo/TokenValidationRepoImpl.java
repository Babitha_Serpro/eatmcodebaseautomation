package com.cca.ob.repo;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Types;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.cca.ob.dto.TokenValidationBean;
import com.cca.ob.enums.Token;

@Repository
public class TokenValidationRepoImpl implements TokenValidationRepo{
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public Long logRawRequest(Map<String, String> requestParams, String remoteIp,String accessCode) {
		String sql="INSERT INTO OB.TOKEN_VALIDATION_REQ_DUMP "+
					"(`ACCESS_CODE`,`REQ_DUMP`,`REMOTE_IP`) "+
				    "VALUES (?,?,?)";
		KeyHolder keyHolder = new GeneratedKeyHolder();
		 
	    jdbcTemplate.update(connection -> {
	        PreparedStatement ps = connection
	          .prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
	          ps.setString(1, accessCode);
	          ps.setString(2, requestParams.toString());
	          ps.setString(3, remoteIp);
	          return ps;
	        }, keyHolder);
	 
	        return ((java.math.BigInteger ) keyHolder.getKey()).longValue();
	}

	@Override
	public TokenValidationBean isTokenValid(int tokenNumber) {
		/*
		 * String sql="SELECT  TD.AMOUNT FROM OB.TOKENS TK " +
		 * "INNER JOIN OB.PG_TRANSACTION_DETAILS TD " +
		 * "ON TD.TRANSACTION_ID=TK.WITHDRAW_TRANSACT_ID " +
		 * "WHERE TK.TOKEN_NUMBER=? AND " + "(current_date() - TK.ISSUE_DATE)=0 AND " +
		 * "TK.TOKEN_STATUS='"+Token.VALID.getState()+"'";
		 */
		String sql="SELECT  TD.AMOUNT AMOUNT,COUNT(*) REQ_COUNT FROM OB.TOKENS TK " + 
				"		INNER JOIN OB.PG_TRANSACTION_DETAILS TD " + 
				"			ON TD.TRANSACTION_ID=TK.WITHDRAW_TRANSACT_ID " + 
				"		INNER JOIN OB.TOKEN_VALIDATION_REQ R " + 
				"			ON R.TOKEN_NUMBER=TK.TOKEN_NUMBER " + 
				"		WHERE TK.TOKEN_NUMBER=? AND " + 
				"		(current_date() - TK.ISSUE_DATE)=0 AND " + 
				"		(current_date() - DATE(R.CREATED_TS))=0 AND " + 
				"		TK.TOKEN_STATUS='"+Token.VALID.getState()+"'"+
				"		GROUP BY TK.TOKEN_NUMBER";
		
		

		try {
			return jdbcTemplate.queryForObject(sql, new Object[]{tokenNumber}, (rs, rowNum) ->
				new TokenValidationBean(
						rs.getFloat("AMOUNT"),
						rs.getInt("REQ_COUNT")
						
				));
		} catch (EmptyResultDataAccessException e) {
			return null;
		}		

	}
	@Override
	public boolean consumeToken(Long tokenId) {
		String sql="UPDATE OB.TOKENS " + 
				"SET TOKEN_STATUS='"+Token.USED.getState()+"' " + 
				"WHERE TOKEN_NUM = ? AND " + 
				"TOKEN_STATUS='"+Token.VALID.getState()+"'";
		Object[] params = {tokenId};
		int[] types = {Types.BIGINT};
		
        int result= jdbcTemplate.update(sql, params, types);
        return result==1 ? true : false;
	}

	@Override
	public Long tokenValidationRequested(Long reqDumpID,String aggregatorId, String merchantId,String bcID, Integer tokenNum,String bcRefNumber,String reqType,String validatorID,String encMobileNum,Double merLat,Double merchLong,String userMobilePlain, String pin4,String pin6) {
		String sql="INSERT INTO OB.TOKEN_VALIDATION_REQ "+
				"(`BC_REF_NUM`,`AGG_ID`,`MERCHANT_ID`,`TOKEN_NUMBER`,`REQ_DUMP_ID`,`VALIDATOR_ID`,`REQUEST_TYPE`,`BC_ID`,`USER_MOBILE`,`MER_LAT`,`MER_LONG`,`MOBILE_NUMBER_PLAIN`,`PIN_4`,`PIN_6`) "+
			    "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	KeyHolder keyHolder = new GeneratedKeyHolder();
	 
    jdbcTemplate.update(connection -> {
        PreparedStatement ps = connection
          .prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
          ps.setString(1, bcRefNumber);
          ps.setString(2,aggregatorId);
          ps.setString(3, merchantId);
          if(tokenNum!=null) {
        	  ps.setInt(4, tokenNum);
          }else {
        	  ps.setNull(4, java.sql.Types.INTEGER);
          }
          if(reqDumpID!=null) {
        	  ps.setLong(5, reqDumpID);
          }else {
        	  ps.setNull(5, java.sql.Types.BIGINT);
          }
          ps.setString(6, validatorID);
          ps.setString(7, reqType);
          ps.setString(8, bcID);
          if(encMobileNum!=null) {
        	  ps.setString(9, encMobileNum);
          }else {
        	  ps.setNull(9, java.sql.Types.VARCHAR);
          }
          if(merLat!=null) {
        	  ps.setDouble(10, merLat);
          }else {
        	  ps.setNull(10, java.sql.Types.DOUBLE);
          }
          if(merchLong!=null) {
        	  ps.setDouble(11, merchLong);
          }else {
        	  ps.setNull(11, java.sql.Types.DOUBLE);
          }
          
          ps.setString(12, userMobilePlain);
          ps.setString(13, pin4);
          ps.setString(14, pin6);
         
         
          
          return ps;
        }, keyHolder);
 
        return ((java.math.BigInteger ) keyHolder.getKey()).longValue();
	}
	@Override
	public Long transactionReversalRequested(Long reqDumpID,String aggregatorId, String merchantId,String bcID, Integer tokenNum,String bcRefNumber,String originalBcRefNum,String originalEatmRefNum,String originalReqTimestamp,String reqType,String validatorID,String encMobileNum,Double merLat,Double merchLong,String reversalCode,String reversaReason,String userMobilePlain, String pin4,String pin6) {
		String sql="INSERT INTO OB.VALIDATION_REVERSAL_REQ "+
				"(`BC_REF_NUM`,`ORIGINAL_BC_REF_NUM`,`ORIGINAL_EATM_REF_NUM`,`ORIGINAL_REQ_TIMESTAMP`,`AGG_ID`,`MERCHANT_ID`,`TOKEN_NUMBER`,`REQ_DUMP_ID`,`VALIDATOR_ID`,`REQUEST_TYPE`,`BC_ID`,`USER_MOBILE`,`MER_LAT`,`MER_LONG`,`REV_REASON_CD`,`REV_REASON`,`MOBILE_NUMBER_PLAIN`,`PIN_4`,`PIN_6`) "+
			    "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	KeyHolder keyHolder = new GeneratedKeyHolder();
	 
    jdbcTemplate.update(connection -> {
        PreparedStatement ps = connection
          .prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
          ps.setString(1, bcRefNumber);
          ps.setString(2, originalBcRefNum);
          ps.setString(3, originalEatmRefNum);
          ps.setString(4, originalReqTimestamp);
          
          ps.setString(5,aggregatorId);
          ps.setString(6, merchantId);
          if(tokenNum!=null) {
        	  ps.setInt(7, tokenNum);
          }else {
        	  ps.setNull(7, java.sql.Types.INTEGER);
          }
          if(reqDumpID!=null) {
        	  ps.setLong(8, reqDumpID);
          }else {
        	  ps.setNull(8, java.sql.Types.BIGINT);
          }
          ps.setString(9, validatorID);
          ps.setString(10, reqType);
          ps.setString(11, bcID);
          
          if(encMobileNum!=null) {
        	  ps.setString(12, encMobileNum);
          }else {
        	  ps.setNull(12, java.sql.Types.VARCHAR);
          }
          if(merLat!=null) {
        	  ps.setDouble(13, merLat);
          }else {
        	  ps.setNull(13, java.sql.Types.DOUBLE);
          }
          if(merchLong!=null) {
        	  ps.setDouble(14, merchLong);
          }else {
        	  ps.setNull(14, java.sql.Types.DOUBLE);
          } 
          ps.setString(15, reversalCode);
          if(reversaReason!=null) {
        	  ps.setString(16, reversaReason);
          }else {
        	  ps.setNull(16, java.sql.Types.VARCHAR);
          }       
          
          ps.setString(17, userMobilePlain);
          ps.setString(18, pin4);
          ps.setString(19, pin6);
          
          
          
          return ps;
        }, keyHolder);
 
        return ((java.math.BigInteger ) keyHolder.getKey()).longValue();
	}
	
	
	@Override
	public void logResponse(long eAtmRefNo,String reqType,String errorCode,String errorMsg,Boolean validationSuccess,Float amount,boolean callBackSuccess) {
		String sql="INSERT INTO OB.TOKEN_VALIDATION_RESP "+
				"(`REQUEST_ID`,`REQUEST_TYPE`,`ERROR_CODE`,`ERROR_MESSAGE`,`VALIDATION_SUCCESS`,`AMOUNT`,`CALL_BACK_SUCCESS`) "+
			    "VALUES (?,?,?,?,?,?,?)";
	 
    jdbcTemplate.update(connection -> {
        PreparedStatement ps = connection
          .prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
          ps.setLong(1, eAtmRefNo);
          ps.setString(2,reqType);
          ps.setString(3,errorCode);
          ps.setString(4, errorMsg);
          ps.setBoolean(5, validationSuccess);
          if(amount!=null) {
        	  ps.setFloat(6, amount);
          }else {
        	  ps.setNull(6, java.sql.Types.FLOAT);
          }
         
          ps.setBoolean(7, callBackSuccess);
          return ps;
        });
		
	}

	@Override
	public void logResponseDump(long eAtmRefNo, String bcRefNum, String reqType, String encResponse) {
		String sql="INSERT INTO OB.TOKEN_VALIDATION_RESP_DUMP "+
				"(`REQUEST_ID`,`REQUEST_TYPE`,`BC_REF_NUM`,`RESPONSE_DUMP`) "+
			    "VALUES (?,?,?,?)";
	 
    jdbcTemplate.update(connection -> {
        PreparedStatement ps = connection
          .prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
          ps.setLong(1, eAtmRefNo);
          ps.setString(2,reqType);
          ps.setString(3,bcRefNum);
          ps.setString(4, encResponse);
         
          return ps;
        });
		
	}
	
	
	@Override
	public boolean updateDispensatinoStatus(long eAtmRefNo, String bcRefNum,long reqDumId) {
		String sql="UPDATE OB.TOKEN_VALIDATION_REQ " + 
				" SET DISPENSED =1 , DISP_DUMP_ID=" + reqDumId+
				" WHERE REQUEST_ID = ? AND " + 
				"BC_REF_NUM='"+bcRefNum+"'";
		Object[] params = {eAtmRefNo};
		int[] types = {Types.BIGINT};
        int result= jdbcTemplate.update(sql, params, types);
        return result==1 ? true : false;		
	}

	@Override
	public int fetchTransactionsCount(String mobileNum) {
		String sql = "SELECT \n" + 
				"    COUNT(*)\n" + 
				"FROM\n" + 
				"    OB.TOKEN_VALIDATION_REQ TVR\n" + 
				"        INNER JOIN\n" + 
				"    OB.TOKEN_VALIDATION_RESP TVRSP ON TVR.REQUEST_ID = TVRSP.REQUEST_ID\n" + 
				"        AND TVR.USER_MOBILE = ?" + 
				"        AND TVRSP.REQUEST_TYPE = 'T'" + 
				"        AND TVRSP.VALIDATION_SUCCESS = 1\n" + 
				"        AND CURRENT_DATE() - DATE(TVR.created_ts) = 0";
		
		return jdbcTemplate.queryForObject(sql, new Object[] {mobileNum}, Integer.class);
	}
	@Override
	public String fetchOriginalBCREFNO(String originaleatmrefnum) {
		String sql = "Select BC_REF_NUM from OB.TOKEN_VALIDATION_REQ where REQUEST_ID=?";
		
		return jdbcTemplate.queryForObject(sql, new Object[] {originaleatmrefnum}, String.class);
	}
	
	//Fixes
		@Override
		public int fetchBCREFNO(String bcrefnumber) {
			int data=0;
			try {
			String sql = "Select REQUEST_ID from OB.TOKEN_VALIDATION_REQ where  BC_REF_NUM=?";
			data= jdbcTemplate.queryForObject(sql, new Object[] { bcrefnumber }, Integer.class);
			}catch(Exception e) {
				e.getMessage();
			}
			
			return data;
		}

		@Override
		public int checkData(int reqId) {
			int data=0;
			try {
			String sql = "SELECT count(REQUEST_ID) FROM TOKEN_VALIDATION_RESP t where t.REQUEST_ID=? and t.REQUEST_TYPE='R'  and t.Validation_SUCCESS=1 ";
			data=jdbcTemplate.queryForObject(sql, new Object[] { reqId }, Integer.class);
			}catch(Exception e) {
				e.getMessage();
			}
			return data;
		} //
	
	
	@Override
	public int fetchBCREFCount(String bcrefnumber) {
		String sql = "Select count(BC_REF_NUM) as count from OB.VALIDATION_REVERSAL_REQ where BC_REF_NUM=?";
		
		return jdbcTemplate.queryForObject(sql, new Object[] {bcrefnumber}, Integer.class);
	}
	@Override
	public int fetchBCREFCountT(String bcrefnumber) {
		String sql = "Select count(BC_REF_NUM) as count from OB.TOKEN_VALIDATION_REQ where BC_REF_NUM=?";
		
		return jdbcTemplate.queryForObject(sql, new Object[] {bcrefnumber}, Integer.class);
	}

	
}
