package com.cca.ob.repo;

import com.cca.ob.dto.CustomerInfo;

public interface PGTransactionRepo {
	public CustomerInfo getCustomerForTransaction(long transactionId);
}
