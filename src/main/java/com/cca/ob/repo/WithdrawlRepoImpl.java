package com.cca.ob.repo;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Types;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.cca.ob.dto.CardDetails;
import com.cca.ob.enums.Token;
import com.cca.ob.exception.OBException;
import com.cca.ob.service.TokenGeneratorService;
@Repository
public class WithdrawlRepoImpl implements WithDrawlRepository{
	private Logger logger=LogManager.getLogger(TokenGeneratorService.class);
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Override
	public List<CardDetails> getCards(long custId) {
		String sql ="SELECT c.CARD_ID,c.CARD_NUM,c.EXP_MONTH,c.EXP_YEAR,t.ORDER_STATUS FROM  OB.CARD_DETAILS c "+
		"LEFT JOIN OB.PG_TRANSACTION_DETAILS t "+
		"ON c.PENNY_TRANS_ID = t.TRANSACTION_ID "+
		"WHERE c.CUST_ID=? AND c.IS_DELETED=false";
        return jdbcTemplate.query(
                sql,new Object[]{custId},
                (rs, rowNum) ->
                	//final Long pTransctId=rs.getString("CARD_NUM");
                    new CardDetails(
                    	rs.getLong("CARD_ID"),
                        rs.getString("CARD_NUM"),
                        rs.getString("EXP_MONTH"),
                        rs.getString("EXP_YEAR"),
                        rs.getString("ORDER_STATUS"),
                        true
                    )
        );
	}
	@Override
	public int updatePennyTransactionId(long cardID, long transactionId) {
		String updateSql = "UPDATE  OB.CARD_DETAILS SET PENNY_TRANS_ID = ? "
				+ " WHERE CARD_ID=?";
		Object[] params = {transactionId,cardID};
		int[] types = {Types.BIGINT,Types.BIGINT};
		
        return jdbcTemplate.update(updateSql, params, types);
	}
	
	@Override
	public long getToken(long transactionId,List<Long> tokensList) {
		
		
		String querySql="SELECT C.CUST_ID FROM OB.CARD_DETAILS C " + 
				"LEFT JOIN OB.PG_TRANSACTION T ON " + 
				"C.CARD_ID=T.CARD_ID " + 
				"WHERE T.TRANSACTION_ID=?";
		
		Long custId= jdbcTemplate.queryForObject(querySql, new Object[]{transactionId}, 
				(rs, rowNum) ->
        			new Long(rs.getLong("CUST_ID")
        ));
		if(custId==null) {
			throw new OBException("Token could not be created. The customer for the tranascation Id"+transactionId+"could not be retrieved");
		}
		
		
		
		String status=Token.VALID.getState();
		String insertSql="INSERT INTO OB.TOKENS (CUST_ID,TOKEN_NUMBER,WITHDRAW_TRANSACT_ID,TOKEN_STATUS,ISSUE_DATE) values(?,?,?,?,CURDATE())";
		//KeyHolder keyHolder = new GeneratedKeyHolder();
		for(Long tokenRef:tokensList) {
			try {
				jdbcTemplate.update(connection -> {
			        PreparedStatement ps = connection
			          .prepareStatement(insertSql,Statement.RETURN_GENERATED_KEYS);
			          ps.setLong(1, custId.longValue());
			          ps.setLong(2, tokenRef);
			          ps.setLong(3, transactionId);
			          ps.setString(4, status);
			          return ps;
			        });
				return tokenRef.longValue();
			}catch(Exception e) {
				logger.debug("Failed to insert the token ref number "+tokenRef+" in to table for user "+custId.longValue());
			}
		}
		String errorMsg="Tried "+tokensList.size()+" to insert token ref to db but failed. Token not generated for the transaction Id "+transactionId;
	    logger.debug(errorMsg);
	    throw new OBException(errorMsg);
	}
	@Override
	public boolean deleteCard(long cardId) {
		
		String updateSql = "UPDATE  OB.CARD_DETAILS SET IS_DELETED = true "
				+ " WHERE CARD_ID=?";
		Object[] params = {cardId};
		int[] types = {Types.BIGINT};
		
        return jdbcTemplate.update(updateSql, params, types) > 0 ? true:false;
	}
	
	
}

