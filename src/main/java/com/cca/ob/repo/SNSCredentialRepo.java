package com.cca.ob.repo;

import com.cca.ob.dto.SNSAuthBean;

public interface SNSCredentialRepo {
	public int createAuth(String id,String pass,String identifier);
	public SNSAuthBean readAuth(String identifier);
}
