package com.cca.ob.repo;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Types;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.cca.ob.dto.CustomerInfo;

@Repository
public class RegistrationRepositoryImpl implements RegistrationRepository{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Override
	public long registerPhone(String contryCode, String phoneNumber,String userName) {

		String insertSql="INSERT INTO OB.USERS (USER_NAME,CONTRY_CODE, MOBILE_NUM) values(?,?,?)";
		KeyHolder keyHolder = new GeneratedKeyHolder();
		 
	    jdbcTemplate.update(connection -> {
	        PreparedStatement ps = connection
	          .prepareStatement(insertSql,Statement.RETURN_GENERATED_KEYS);
	          ps.setString(1, userName);
	          ps.setString(2, contryCode);
	          ps.setString(3, phoneNumber);
	          return ps;
	        }, keyHolder);
	 
	        return ((java.math.BigInteger ) keyHolder.getKey()).longValue();
	}
	@Override
	public int insertSMSConfirmation(String qualifiedPhoneNum, String provider,String providerRef) {
		
		return jdbcTemplate.update(
                "INSERT INTO OB.SMS_AUDIT (QUALIFIED_PHONE_NO,PROVIDER, PROV_REF_NO) values(?,?,?)",
                qualifiedPhoneNum,provider,providerRef);
		
	}
	
	
	@Override
	public CustomerInfo getCustomer(String countryCode,String phoneNum) {
		String sql = "SELECT * FROM OB.USERS WHERE CONTRY_CODE = ? AND MOBILE_NUM=?";

        return jdbcTemplate.queryForObject(sql, new Object[]{countryCode,phoneNum}, (rs, rowNum) ->
                new CustomerInfo(
                        rs.getString("CONTRY_CODE"),
                        rs.getString("MOBILE_NUM"),
                        rs.getString("USER_NAME"),
                        rs.getLong("CUST_ID")
                ));
	}
/***	
	@Override
	public CustomerInfo getCustomer(String countryCode,String phoneNum) {
		
		String sql="SELECT U.CUST_ID,U.USER_NAME,U.CONTRY_CODE,U.MOBILE_NUM,C.CARD_ID,C.CARD_NUM,C.EXP_MONTH ,C.EXP_YEAR " + 
				"FROM OB.USERS U INNER JOIN OB.CARD_DETAILS C  ON U.CUST_ID  = C.CUST_ID WHERE U.CONTRY_CODE = ? AND U.MOBILE_NUM = ? ";
        return jdbcTemplate.query(sql,new String[]{countryCode,phoneNum},new ResultSetExtractor<CustomerInfo>() {
			@Override
			public CustomerInfo extractData(ResultSet rs) throws SQLException, DataAccessException {
				CustomerInfo custInfo=null;
				String cardNum=null;
				while (rs.next()) {
					if(custInfo==null) {
						custInfo= new CustomerInfo(
		                        rs.getString("CONTRY_CODE"),
		                        rs.getString("MOBILE_NUM"),
		                        rs.getString("USER_NAME"),
		                        rs.getLong("CUST_ID")
		                );
					}
					cardNum=rs.getString("CARD_NUM");
					if(cardNum!=null) {
						custInfo.addCard(new CardDetails(
								rs.getLong("CARD_ID"),
								cardNum,
								null,null,
								rs.getString("EXP_MONTH"),
								rs.getString("EXP_YEAR")));
					}

				}
				return custInfo;
			}
        	
        	});
        
        
	}
	***/
	@Override
	public long registerCard(long custId,String cardNum,String expMnth,String expYr) {
		
		//First Update the table - for custID & cardNum SetIsDeleted=false;
		//If 0 records impacted
			//Insert
		//If 1 record impacted
			//Select to fetch the card id 
			
		String updateSql = "UPDATE  OB.CARD_DETAILS SET IS_DELETED = false,EXP_MONTH=?,EXP_YEAR=? "
				+ " WHERE CUST_ID=? AND CARD_NUM=? ";
		Object[] params = {expMnth,expYr,custId,cardNum};
		int[] types = {Types.VARCHAR,Types.VARCHAR,Types.BIGINT,Types.VARCHAR};
		
        int result= jdbcTemplate.update(updateSql, params, types);
		
        if(result==0) {
        	KeyHolder keyHolder = new GeneratedKeyHolder();
    		String insertSql="INSERT INTO OB.CARD_DETAILS (CUST_ID,CARD_NUM, EXP_MONTH,EXP_YEAR) values(?,?,?,?)";
    		
    		jdbcTemplate.update(connection -> {
    	        PreparedStatement ps = connection
    	          .prepareStatement(insertSql,Statement.RETURN_GENERATED_KEYS);
    	          ps.setLong(1, custId);
    	          ps.setString(2, cardNum);
    	          ps.setString(3, expMnth);
    	          ps.setString(4, expYr);
    	          return ps;
    	        }, keyHolder);
    		 return ((java.math.BigInteger ) keyHolder.getKey()).longValue();
        }else {
        	String sql = "SELECT CARD_ID FROM OB.CARD_DETAILS  WHERE CUST_ID=? AND CARD_NUM=? ";

            Long cardId= jdbcTemplate.queryForObject(sql, new Object[]{custId,cardNum}, (rs, rowNum) ->
                    new Long(
                        rs.getLong("CARD_ID")
                    ));
            return cardId.longValue();
        }
		
		
	}
	
}
