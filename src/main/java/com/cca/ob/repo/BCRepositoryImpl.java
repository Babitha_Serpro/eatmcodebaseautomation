package com.cca.ob.repo;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.cca.ob.bc.controller.MerchantDataController;
import com.cca.ob.dto.BCDetails;
import com.cca.ob.dto.MerchantDetails;
import com.cca.ob.dto.MerchantDetailsMini;
@Repository
public class BCRepositoryImpl implements BCRepository {
	@Autowired
	private JdbcTemplate jdbcTemplate;
	private static Logger logger=LogManager.getLogger(BCRepositoryImpl.class);

	@Override
	public List<MerchantDetailsMini> getMerchants(float currLat, float currLong, float radiusInKm,int maxRecord) {
		/*
		String sql ="SELECT AGENT_ID,AGENT_NAME,GEO_LAT,GEO_LONG,ADDRESS, "+
				"ABS( GEO_LAT- ?) LAT_DIFF, ABS( GEO_LONG- ?) LONG_DIFF FROM  OB.MERCHANT_MASTER M " +
				"LEFT JOIN OB.AGGREGATOR A " + 
				"ON M.AGGREGATOR_ID=A.AGGREGATOR_ID " + 
				"LEFT JOIN OB.BC BC " + 
				"ON A.BC_ID=BC.BC_ID "+
				"WHERE M.IS_ENABLED=true " + 
				"AND A.IS_ENABLED=true " + 
				"AND BC.IS_ENABLED=true "+
				"ORDER BY LAT_DIFF,LONG_DIFF LIMIT ?";
		 */

		String sql="Select agent_id,merchant_name,shop_name,mobile1,mobile2,landline,emailid,address,city,state,pincode,landmark,\n" + 
				"ST_Distance_Sphere(point( SUBSTRING_INDEX(SUBSTRING_INDEX(geo_code,' ',2),' ',-1),SUBSTRING_INDEX(geo_code,' ',1)),point( ? , ?))/1.609 RADIAL_DIST,SUBSTRING_INDEX(geo_code,' ',1) as mLat,SUBSTRING_INDEX(SUBSTRING_INDEX(geo_code,' ',2),' ',-1) as mLong, ai_id,country,weekly_off_days,daily_break \n" + 
				",working_hours,mcat.Merchant_Category_code, mcat.Description, App_Description from OB.MERCHANT_MASTER mstr, OB.Merchant_Category mcat \n" + 
				"where mcat.Merchant_Category_Code = mstr.merchant_category_code and mstr.isEnabled=1 ORDER BY RADIAL_DIST limit ?;";

		//		String sql="SELECT AGENT_ID,AGENT_NAME,GEO_LAT,GEO_LONG,ADDRESS,MOBILE_NUM,CATEGORY,SHOP_NAME,PINCODE,CITY, " + 
		//				" ST_Distance_Sphere(point( GEO_LONG,GEO_LAT),point( ?,?)) RADIAL_DIST" + 
		//				" FROM  OB.MERCHANT_MASTER M " + 
		//				" LEFT JOIN OB.AGGREGATOR A " + 
		//				" ON M.AGGREGATOR_ID=A.AGGREGATOR_ID " + 
		//				" LEFT JOIN OB.BC BC " + 
		//				" ON A.BC_ID=BC.BC_ID " + 
		//				" WHERE M.IS_ENABLED=true  " + 
		//				" AND A.IS_ENABLED=true  " + 
		//				" AND BC.IS_ENABLED=true " + 
		//				" ORDER BY RADIAL_DIST LIMIT ?";

		//		return jdbcTemplate.query(
		//				//The point function of Geo API in mysql takes (Long,Lat)
		//				//and not Lat,Long
		//				sql,new Object[]{currLong,currLat,maxRecord},
		//				(rs, rowNum) ->
		//				new MerchantDetailsMini(
		//						rs.getString("AGENT_ID"),
		//						rs.getString("AGENT_NAME"),
		//						rs.getString("ADDRESS"),
		//						rs.getFloat("GEO_LAT"),
		//						rs.getFloat("GEO_LONG"),
		//						rs.getFloat("RADIAL_DIST"),
		//						rs.getString("MOBILE_NUM"),
		//						rs.getString("SHOP_NAME"),
		//						rs.getString("PINCODE"),
		//						rs.getString("CITY"),
		//						rs.getString("CATEGORY")
		//
		//						)
		//
		//
		//				);
			return jdbcTemplate.query(
				//The point function of Geo API in mysql takes (Long,Lat)
				//and not Lat,Long
				sql,new Object[]{currLong,currLat,maxRecord},
				(rs, rowNum) ->
				new MerchantDetailsMini(
						rs.getString("agent_id"),
						rs.getString("merchant_name"),
						rs.getString("shop_name"),
						rs.getString("pincode"),
						rs.getString("city"),
						rs.getString("mobile1"),
						rs.getString("mobile2"),
						rs.getString("landline"),
						rs.getString("emailid"),
						rs.getString("state"),
						rs.getString("landmark"),
						rs.getString("address"),
						rs.getString("country"),
						rs.getString("weekly_off_days"),
						rs.getString("working_hours"),
						rs.getString("daily_break"),
						rs.getString("Merchant_Category_code"),
						rs.getString("Description"),
						rs.getString("App_Description"),
						rs.getString("RADIAL_DIST"),
						rs.getString("mLat"),
						rs.getString("mLong")

						)


				);
	}
	@Override
	public MerchantDetails getMerchant(String merchantId) {
//		String sql ="SELECT M.AGENT_ID,M.GEO_LAT,M.GEO_LONG,M.ADDRESS,M.IS_ENABLED AGENT_ENABLED,A.AGGREGATOR_ID,A.IS_ENABLED AGG_ENABLED,BC.IS_ENABLED BC_ENABLED,BC.BC_ID BCID FROM   OB.MERCHANT_MASTER M " + 
//				" INNER JOIN OB.AGGREGATOR A "+
//				" ON M.AGGREGATOR_ID=A.AGGREGATOR_ID "+
//				" INNER JOIN OB.BC BC "+
//				" ON BC.BC_ID=A.BC_ID "+
//				" WHERE M.AGENT_ID = ? ";
		String sql="SELECT M.agent_id as AGENT_ID,SUBSTRING_INDEX(geo_code,' ',1) as GEO_LAT,SUBSTRING_INDEX(geo_code,' ',-1) as GEO_LONG,M.address as ADDRESS,M.isEnabled AGENT_ENABLED,A.AGGREGATOR_ID,A.IS_ENABLED AGG_ENABLED,BC.IS_ENABLED BC_ENABLED,BC.BC_ID BCID FROM   \n" + 
				"OB.MERCHANT_MASTER M INNER JOIN OB.AGGREGATOR A ON M.ai_id=A.AGGREGATOR_ID INNER JOIN OB.BC BC \n" + 
				"ON BC.BC_ID=A.BC_ID WHERE M.agent_id = ?";
		try {
			return jdbcTemplate.queryForObject(sql, new Object[]{merchantId}, (rs, rowNum) ->
			new MerchantDetails(
					rs.getString("AGENT_ID"),
					rs.getFloat("GEO_LAT"),
					rs.getFloat("GEO_LONG"),
					rs.getString("ADDRESS"),
					rs.getBoolean("AGENT_ENABLED"),
					rs.getString("AGGREGATOR_ID"),
					rs.getBoolean("AGG_ENABLED"),
					rs.getString("BCID"),
					rs.getBoolean("BC_ENABLED")
					));
		} catch (EmptyResultDataAccessException e) {
			return null;
		}		
	}
	@Override
	public BCDetails getBCDetails(String accessCode) {
		String sql="SELECT BC_ID,VALIDATOR_CALL_BACK,REVERSAL_CALL_BACK,REQUEST_KEY,RESP_KEY FROM OB.BC WHERE ACCESS_CODE= ?";
		try {
			return jdbcTemplate.queryForObject(sql, new Object[]{accessCode}, (rs, rowNum) ->
			new BCDetails(
					//rs.getString("AGGREGATOR_ID"),
					rs.getString("BC_ID"),
					rs.getString("VALIDATOR_CALL_BACK"),
					rs.getString("REVERSAL_CALL_BACK"),
					rs.getString("REQUEST_KEY"),
					rs.getString("RESP_KEY")
					));
		} catch (EmptyResultDataAccessException e) {
			return null;
		}		
	}

	@Override
	public BCDetails getBCDtls(String accessCode) {
		String sql="SELECT BC_ID,Merchant_Secret_Key FROM OB.Merchant_Details_Owner WHERE ACCESS_CODE= ?";
		try {
			return jdbcTemplate.queryForObject(sql, new Object[]{accessCode}, (rs, rowNum) ->
			new BCDetails(
					//rs.getString("AGGREGATOR_ID"),
					rs.getString("BC_ID"),
					rs.getString("Merchant_Secret_Key")
					));
		} catch (EmptyResultDataAccessException e) {
			logger.error("Exception while retreving BC Details with "+accessCode,e);
			return null;
		}		
	}

	@Override
	public int insertMerchantDetails(String agentID, String merchantName, String shopName, String merchantCategoryCode, String mobile1, String mobile2,String landline, String emailID, String address, String city, String state, String pincode, String landMark, String geoCode, String aiID, String country, String weeklyOffDays, String workingHours, String dailyBreak) {
		String sql="INSERT INTO OB.MERCHANT_MASTER(`agent_id`,`merchant_name`,`shop_name`,`merchant_category_code`,`mobile1`,`mobile2`,`landline`,`emailid`,`address`,`city`\r\n" + 
				",`state`,`pincode`,`landmark`,`geo_code`,`ai_id`,`country`,`weekly_off_days`,`working_hours`,`daily_break`) \r\n" + 
				"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		KeyHolder keyHolder = new GeneratedKeyHolder();
		int result=0;
		try {
			result=jdbcTemplate.update(connection -> {
				PreparedStatement ps = connection
						.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, agentID);
				ps.setString(2, merchantName);
				ps.setString(3, shopName);
				ps.setString(4, merchantCategoryCode);
				ps.setLong(5, Long.parseLong(mobile1.replaceAll("\\D+","")));
				ps.setLong(6, mobile2.equals("")?0:Long.parseLong(mobile2.replaceAll("\\D+","")));
				ps.setLong(7, landline.equals("")?0:Long.parseLong(landline.replaceAll("\\D+","")));
				ps.setString(8, emailID);
				ps.setString(9, address);
				ps.setString(10, city);
				ps.setString(11, state);
				ps.setLong(12, Long.parseLong(pincode.replaceAll("\\D+","")));
				ps.setString(13, landMark);
				ps.setString(14, geoCode);
				ps.setString(15, aiID);
				ps.setString(16, country);
				ps.setString(17, weeklyOffDays);
				ps.setString(18, workingHours);
				ps.setString(19, dailyBreak);
				return ps;
			}, keyHolder);
		}catch(DuplicateKeyException  e){
			logger.info("Duplicate Entry found for AGENT ID "+agentID,e.getLocalizedMessage());
			return 2;
		}catch(Exception e) {
			logger.info("Exception found for AGENT ID "+agentID,e.getLocalizedMessage());
			return 0;
		}

		return result;		

	}
	@Override
	public boolean updateMerchantDetails(String agentID, String merchantName, String shopName, String merchantCategoryCode, String mobile1, String mobile2,String landline, String emailID, String address, String city, String state, String pincode, String landMark, String geoCode, String aiID, String country, String weeklyOffDays, String workingHours, String dailyBreak,String isEnabled) {
		String sql="Update OB.MERCHANT_MASTER SET merchant_name=?,shop_name=?,merchant_category_code=?,mobile1=?,mobile2=?,landline=?,emailid=?,address=?\n" + 
				",city=?,state=?,pincode=?,landmark=?,geo_code=?,ai_id=?,country=?,weekly_off_days=?, working_hours=?,daily_break=?,isEnabled=? where agent_id=?";
		KeyHolder keyHolder = new GeneratedKeyHolder();
		int result=0;
		try {
			result=jdbcTemplate.update(connection -> {
				PreparedStatement ps = connection
						.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, merchantName);
				ps.setString(2, shopName);
				ps.setString(3, merchantCategoryCode);
				ps.setLong(4, Long.parseLong(mobile1.replaceAll("\\D+","")));
				ps.setLong(5, mobile2.equals("")?0:Long.parseLong(mobile2.replaceAll("\\D+","")));
				ps.setLong(6, landline.equals("")?0:Long.parseLong(landline.replaceAll("\\D+","")));
				ps.setString(7, emailID);
				ps.setString(8, address);
				ps.setString(9, city);
				ps.setString(10, state);
				ps.setLong(11, Long.parseLong(pincode.replaceAll("\\D+","")));
				ps.setString(12, landMark);
				ps.setString(13, geoCode);
				ps.setString(14, aiID);
				ps.setString(15, country);
				ps.setString(16, weeklyOffDays);
				ps.setString(17, workingHours);
				ps.setString(18, dailyBreak);
				ps.setString(19, isEnabled);
				ps.setString(20, agentID);
				return ps;
			}, keyHolder);
		}catch(Exception e) {
			logger.info("Exception found for AGENT ID "+agentID,e.getLocalizedMessage());
			return false;
		}

		return true;		

	}
}
