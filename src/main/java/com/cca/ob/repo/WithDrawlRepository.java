package com.cca.ob.repo;

import java.util.List;

import com.cca.ob.dto.CardDetails;

public interface WithDrawlRepository {
	public List<CardDetails> getCards(long custId);

	public int updatePennyTransactionId(long cardID, long transactionId2);

	public long getToken(long transactionId, List<Long> tokensList);
	
	public boolean deleteCard(long cardId);
}
