package com.cca.ob.repo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.cca.ob.dto.CustomerInfo;
@Repository
public class PGTransactionRepoImpl implements PGTransactionRepo {
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public CustomerInfo getCustomerForTransaction(long transactionId) {
		
		String sql="SELECT U.CUST_ID,U.USER_NAME,U.CONTRY_CODE, U.MOBILE_NUM  FROM OB.USERS U " + 
				"LEFT JOIN OB.CARD_DETAILS C ON U.CUST_ID=C.CUST_ID " + 
				"LEFT JOIN OB.PG_TRANSACTION T ON T.CARD_ID=C.CARD_ID " + 
				"  WHERE T.TRANSACTION_ID = ?";
		 return jdbcTemplate.queryForObject(sql, new Object[]{transactionId}, (rs, rowNum) ->
         new CustomerInfo(
                 rs.getString("CONTRY_CODE"),
                 rs.getString("MOBILE_NUM"),
                 rs.getString("USER_NAME"),
                 rs.getLong("CUST_ID")
         ));
	}

}
