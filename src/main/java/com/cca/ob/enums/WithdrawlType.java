package com.cca.ob.enums;

public enum WithdrawlType {
	PENNYTRANSACTION("PENNY_TRANSACTION"),WITHDRAWAL("WITHDRAWAL");
	private String type;
	private WithdrawlType(String wType){
		type=wType;
	}
	
	public String getType() {
		return type;
	}
}
