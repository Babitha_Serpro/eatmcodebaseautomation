package com.cca.ob.enums;

public enum Token {
	VALID("VALID"),INVALID("INVALID"),EXPIRED("EXPIRED"),USED("USED");
	private String state;
	private Token(String currState) {
		state=currState;
	}
	public String getState() {
		return state;
	}
}
