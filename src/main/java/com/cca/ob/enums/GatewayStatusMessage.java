package com.cca.ob.enums;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public enum GatewayStatusMessage {
	FAILURE("Failure"),SUCCESS("Success"),INVALID("Invalid"),ABORTED("Aborted"),TIMEDOUT("Timeout");
	
	private String status;
	private static Logger logger=LogManager.getLogger(GatewayStatusMessage.class);
	private GatewayStatusMessage(String val) {
		this.status=val;
	}
	 
	public static GatewayStatusMessage getStatusMessage(String status) {
		logger.debug("Feching Status for "+status);
		if("Failure".equalsIgnoreCase(status)) {
			return FAILURE;
		}else if("Success".equalsIgnoreCase(status)) {
			return SUCCESS;
		}else if("Invalid".equalsIgnoreCase(status)) {
			return INVALID;
		}else if("Aborted".equalsIgnoreCase(status)) {
			return ABORTED;
		}else if("Timeout".equalsIgnoreCase(status)) {
			return TIMEDOUT;
		}else {
			return null;
		}
	}
	
	public String getStatus() {
		return status;
	}
}
