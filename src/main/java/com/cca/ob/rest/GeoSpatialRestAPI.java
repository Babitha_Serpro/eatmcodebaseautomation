package com.cca.ob.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cca.ob.dto.MerchantDetailsMini;
import com.cca.ob.dto.MerchantLocationResponseDTO;
import com.cca.ob.service.CacheService;
import com.cca.ob.service.GeoSpatialService;
import com.cca.ob.service.RegistrationService;
import com.cca.ob.service.UserSessionService;

@RestController
@RequestMapping("/api/v1")


public class GeoSpatialRestAPI {
	@Autowired
	private GeoSpatialService geoSpatialService;
	@Autowired
	private RegistrationService registrationService;
	@Autowired
	private CacheService cacheService;
	@Autowired
	private UserSessionService userSessionService;
	
	private Logger logger=LogManager.getLogger(GeoSpatialRestAPI.class);
	
	@PostMapping(path = "/MerchantLocations")
	public ResponseEntity<MerchantLocationResponseDTO> getNearByMerchants(@RequestBody MerchantLocaationReqDTO reqDto, HttpServletRequest request) {
		logger.debug("Request for MerchantLocations "+reqDto.getmKey());
		MerchantLocationResponseDTO response = new MerchantLocationResponseDTO();
		response.setmKey(reqDto.getmKey());
		response.setAuthKey(reqDto.getAuthKey()); 
		//Object custInfo=request.getSession().getAttribute(OBConstants.CUSTOMER_INFO);
		Object custInfo=cacheService.getCustomerInfo(reqDto.getmKey());
		if (!registrationService.validateMobileNumberHashKey(reqDto.getmKey())) {
			logger.debug("Request for CardDetails "+reqDto.getmKey()+"\t Mobile number is not valid");
			response.setErrorCode("1003"); // come up with error codes for each error type
			response.setErrorMsg("Mobile number is not valid");
		} else if(custInfo==null ||  !userSessionService.isValidSession(reqDto.getmKey(), reqDto.getAuthKey())) {
			logger.debug("Request for CardDetails "+reqDto.getmKey()+"\t Incosistent Session");
			response.setErrorCode("1010"); // come up with error codes for each error type
			response.setErrorMsg("Incosistent Session");
		}else {
			response.setAuthKey(userSessionService.refreshAuthCode(reqDto.getmKey())); 
			List<MerchantDetailsMini> merchList=geoSpatialService.getMerchants(reqDto.getmKey(),reqDto.getCurrLat(),reqDto.getCurrLong());
			if(merchList==null || merchList.size()<1) {
				response.setErrorCode("4330"); // come up with error codes for each error type
				response.setErrorMsg("Near By Merchant not available in your location.Try with a different location");
				logger.debug("Request for MerchantLocations "+reqDto.getmKey()+"\t No near by merchants");
			}else {
				response.setMerchantDetails(merchList);
				logger.debug("Request for CardDetails "+reqDto.getmKey()+"\t Multiple Merchants found");
			}
			logger.debug("Request for MerchantLocations "+reqDto.getmKey()+"\t returning");
		}
		return ResponseEntity.ok(response);
	}
	
	@GetMapping(path = "/Merchants")
	public ResponseEntity<MerchantLocationResponseDTO> getMerchants(@RequestParam("uLt") float uLt,
			@RequestParam("uLng") float uLng ,@RequestParam(required = false) Integer count,HttpServletRequest request) {
		logger.debug("Generic Request for MerchantLocations "+uLt+","+uLng);
		MerchantLocationResponseDTO response = new MerchantLocationResponseDTO();
		
		List<MerchantDetailsMini> merchList=geoSpatialService.getMerchants(uLt,uLng,count);
		if(merchList==null || merchList.size()<1) {
			response.setErrorCode("4330"); // come up with error codes for each error type
			response.setErrorMsg("Near By Merchant not available in your location.Try with a different location");
			logger.debug("Request for MerchantLocations "+"\t No near by merchants");
		}else {
			response.setMerchantDetails(merchList);
			logger.debug("Request for CardDetails "+"\t Multiple Merchants found");
		}
		return ResponseEntity.ok(response);
	}
}
