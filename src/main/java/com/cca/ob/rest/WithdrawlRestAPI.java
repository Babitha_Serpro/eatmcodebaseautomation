package com.cca.ob.rest;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cca.ob.dto.CardDetails;
import com.cca.ob.dto.CardValidationResponseBean;
import com.cca.ob.dto.PGEndPoint;
import com.cca.ob.dto.WithdrawRequestBean;
import com.cca.ob.service.CacheService;
import com.cca.ob.service.RegistrationService;
import com.cca.ob.service.UserSessionService;
import com.cca.ob.service.WithdrawlService;
import com.cca.ob.util.CardUtils;

@RestController
@RequestMapping("/api/v1")
public class WithdrawlRestAPI {
	@Autowired
	private RegistrationService registrationService;
	@Autowired
	private CacheService cacheService;
	@Autowired
	private UserSessionService userSessionService;
	@Autowired
	private WithdrawlService withdrawlService;
	
	@Value("${withdrawl.max.limit}")
	private int maxWithdrawLimit;
	@PostMapping(path = "/Withdraw")
	public ResponseEntity<CardValidationResponseBean> getCardDetails(@RequestBody WithdrawRequestBean reqDto, HttpServletRequest request) {
		CardValidationResponseBean response=new CardValidationResponseBean();
		response.setmKey(reqDto.getmKey());
		response.setAuthKey(reqDto.getAuthKey()); 
		CardDetails card= cacheService.getCard(reqDto.getmKey(), reqDto.getCardReferenceNum());
		if (!registrationService.validateMobileNumberHashKey(reqDto.getmKey())) {
			response.setErrorCode("1003"); // come up with error codes for each error type
			response.setErrorMsg("Mobile number is not valid");
			return ResponseEntity.ok(response);
		} else if(card==null ||!userSessionService.isValidSession(reqDto.getmKey(), reqDto.getAuthKey())) {
			response.setErrorCode("1010"); // come up with error codes for each error type
			response.setErrorMsg("Incosistent Session");
			return ResponseEntity.ok(response);
		}else if(! CardUtils.isCvvVAlid(reqDto.getCvv()))  {
			response.setErrorCode("1600"); // come up with error codes for each error type
			response.setErrorMsg("Invalid CVV");
			return ResponseEntity.ok(response);
		}else {
			float amount=reqDto.getAmount();
			if(amount==0) {
				response.setErrorCode("2312"); // come up with error codes for each error type
				response.setErrorMsg("Invalid Amount");
				return ResponseEntity.ok(response);
			}else if(amount >maxWithdrawLimit) {
				response.setErrorCode("2313"); // come up with error codes for each error type
				response.setErrorMsg("Max withdraw limit is "+maxWithdrawLimit);
				return ResponseEntity.ok(response);
			}
			
		}
		response.setAuthKey(userSessionService.refreshAuthCode(reqDto.getmKey())); 
		card.setCvv(reqDto.getCvv());
		PGEndPoint pgEndpoint=withdrawlService.getPGEndPoint(card, reqDto.getAmount(),"INR",reqDto.getComments(),false);
		response.setStpEndpoint(pgEndpoint.getPgEndPoint());
		response.setObTransId(pgEndpoint.getTransactionId());
		
		return ResponseEntity.ok(response);
	}
}
