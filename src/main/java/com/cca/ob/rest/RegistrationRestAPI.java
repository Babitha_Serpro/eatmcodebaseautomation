package com.cca.ob.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cca.ob.dto.CardDTO;
import com.cca.ob.dto.CustomerInfo;
import com.cca.ob.dto.PhoneRegistrationStatus;
import com.cca.ob.dto.RegisterDeviceResponseDTO;
import com.cca.ob.dto.ResetDeviceChallengeRequestDTO;
import com.cca.ob.dto.VAlidateDeviceResponseDTO;
import com.cca.ob.dto.ValidateDeviceRequestDTO;
import com.cca.ob.service.CacheService;
import com.cca.ob.service.RegistrationService;
import com.cca.ob.service.UserSessionService;
import com.cca.ob.service.WithdrawlService;
import com.cca.ob.util.Util;

@RestController
@RequestMapping("/api/v1")
public class RegistrationRestAPI {
	
	@Autowired
	private RegistrationService registrationService;
	@Autowired
	private WithdrawlService withdrawlService;
	@Autowired
	private CacheService cacheService;
	@Autowired
	private UserSessionService userSessionService;
	private Logger logger=LogManager.getLogger(CardsRestAPI.class);
	@PostMapping(path = "/RegisterDevice")
	public ResponseEntity<RegisterDeviceResponseDTO> registerDevice(@RequestBody CustomerInfo registerDeviceReq, HttpServletRequest request) {
		logger.debug("Request for RegisterDevice "+registerDeviceReq.getmNo());
		RegisterDeviceResponseDTO response = new RegisterDeviceResponseDTO();
		String mKey = Util.hash(registerDeviceReq.getmNo());
		response.setmKey(mKey);
		if (!registrationService.validateMobileNumber(registerDeviceReq.getmNo())) {
			logger.debug("Request for RegisterCard "+registerDeviceReq.getmNo()+"\t Mobile number is not valid");
			response.setErrorCode("1001"); // come up with error codes for each error type
			response.setErrorMsg("Mobile number is not valid");
			return ResponseEntity.ok(response);
		} 
		if(!registrationService.validateUserName(registerDeviceReq.getcName())) {
			logger.debug("Request for RegisterCard "+registerDeviceReq.getmNo()+"\t Customer Name is Empty");
			response.setErrorCode("1101"); // come up with error codes for each error type
			response.setErrorMsg("Customer Name is Empty");
			return ResponseEntity.ok(response);
		}
		PhoneRegistrationStatus status=registrationService.registerMobileNumber(mKey, registerDeviceReq.getmNo(),registerDeviceReq.getCountryCode(),registerDeviceReq.getcName());
		response.setAuthKey(status.getAuthKey());
		registrationService.incrementCounter(RegistrationService.OTP_RESEND_COUNTER, request.getSession());
		
		if (status.isDuplicatePhoneNumber()) {
			logger.debug("Request for RegisterCard "+registerDeviceReq.getmNo()+"\t Mobile number is already registered");
			response.setErrorCode("1002"); // come up with error codes for each error type
			response.setErrorMsg("Mobile number is already registered");
		} 
		//request.getSession().setAttribute(OBConstants.CUSTOMER_INFO, status.getCustomerInfo());
		cacheService.storeCustomerInfo(mKey,status.getCustomerInfo());
		return ResponseEntity.ok(response);
	}
	
	@PostMapping(path = "/ValidateDevice")
	public ResponseEntity<VAlidateDeviceResponseDTO> validateDevice(@RequestBody ValidateDeviceRequestDTO validateDeviceReq, HttpServletRequest request) {
		logger.debug("Request for ValidateDevice "+validateDeviceReq.getmKey());
		VAlidateDeviceResponseDTO response = new VAlidateDeviceResponseDTO();
		response.setmKey(validateDeviceReq.getmKey());
		response.setAuthKey(validateDeviceReq.getAuthKey()); 
		//Object custInfo=request.getSession().getAttribute(OBConstants.CUSTOMER_INFO);
		Object custInfo=cacheService.getCustomerInfo(validateDeviceReq.getmKey());
		if (!registrationService.validateMobileNumberHashKey(validateDeviceReq.getmKey())) {
			logger.debug("Request for ValidateDevice "+validateDeviceReq.getmKey()+"\t Mobile number is not valid");
			response.setErrorCode("1003"); // come up with error codes for each error type
			response.setErrorMsg("Mobile number is not valid");
		} else if (!registrationService.validateOTP(validateDeviceReq.getmKey(), validateDeviceReq.getAuthKey(), validateDeviceReq.getOtpValue())) {
			logger.debug("Request for ValidateDevice "+validateDeviceReq.getmKey()+"\t Invalid OTP");
			response.setErrorCode("1004"); // come up with error codes for each error type
			response.setErrorMsg("Invalid OTP");
			registrationService.incrementCounter(RegistrationService.OTP_FAILED_COUNTER, request.getSession());
		//} else if(custInfo==null) {
			/*
			 * response.setErrorCode("1010"); response.setErrorMsg("Incosistent Session");
			 */
		}else {
			
			if(custInfo==null) {
				//custInfo=registrationService.getCustomerInfo(validateDeviceReq.getmKey());
			}
			//request.getSession().setAttribute(OBConstants.CUSTOMER_INFO,custInfo);
			//cacheService.storeCustomerInfo(validateDeviceReq.getmKey(),status.getCustomerInfo());
			logger.debug("Request for ValidateDevice "+validateDeviceReq.getmKey()+"\t Invalid OTP");
			response.setcName(((CustomerInfo)custInfo).getcName());
			response.setAuthKey( userSessionService.refreshAuthCode(validateDeviceReq.getmKey()));
			List<CardDTO> cards=withdrawlService.getCardDetails(validateDeviceReq.getmKey(),((CustomerInfo)custInfo).getCustId());
			if(cards.size()>0) {
				/*
				 * CardDetails acard=cards.get(0);
				 * acard.setMaskedCardNum(CardUtils.maskCard(acard.getCardNumber(), true));
				 * String ref=cacheService.storeCards(validateDeviceReq.getmKey(),acard);
				 * response.setCardRefNum(ref);
				 * response.setMaskedCardNum(acard.getMaskedCardNum());
				 */
				response.setHasRegisteredCards(true);
			}
			logger.debug("Request for ValidateDevice "+validateDeviceReq.getmKey()+"\t total cards ="+cards.size());
		}
		return ResponseEntity.ok(response);
	}
	
	@PostMapping(path = "/ResetDeviceChallenge")
	public ResponseEntity<RegisterDeviceResponseDTO> resetDeviceChallenge(@RequestBody ResetDeviceChallengeRequestDTO deviceChallengeReq, HttpServletRequest request) {
		logger.debug("Request for ResetDeviceChallenge "+deviceChallengeReq.getmKey()+"ResetDeviceChallenge");
		RegisterDeviceResponseDTO response = new RegisterDeviceResponseDTO();
		response.setmKey(deviceChallengeReq.getmKey());
		//Object custInfo=request.getSession().getAttribute(registrationService.CUSTOMER_INFO);
		if (!registrationService.validateMobileNumberHashKey(deviceChallengeReq.getmKey())) {
			logger.debug("Request for ResetDeviceChallenge "+deviceChallengeReq.getmKey()+"\tMobile number is not valid");
			response.setErrorCode("1003"); // come up with error codes for each error type
			response.setErrorMsg("Mobile number is not valid");
		}else {
			logger.debug("Request for ResetDeviceChallenge "+deviceChallengeReq.getmKey()+"\t Resending OTP");
			PhoneRegistrationStatus status=registrationService.resetOTP(deviceChallengeReq.getmKey());
			response.setAuthKey(status.getAuthKey());
			registrationService.incrementCounter(RegistrationService.OTP_RESEND_COUNTER, request.getSession());
			response.setAuthKey(status.getAuthKey());
		}
		//TODO: If 1 or 4 fails - treat it as new request for mobile registration and follow <baseUrl>/api/v1/RegisterDevice
		return ResponseEntity.ok(response);
	}


}
