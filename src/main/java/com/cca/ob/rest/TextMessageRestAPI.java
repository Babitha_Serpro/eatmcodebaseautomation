package com.cca.ob.rest;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cca.ob.dto.RegisterDeviceResponseDTO;
import com.cca.ob.dto.SNSAuthBean;
import com.cca.ob.service.SNSRepoService;

@RestController
@RequestMapping("/api/v1")
public class TextMessageRestAPI {
	@Autowired
	private SNSRepoService sNSRepoService;
	//Security Breach
	//@PostMapping(path = "/SNSAuth")
	public ResponseEntity<RegisterDeviceResponseDTO> createAuthEntry(@RequestBody SNSAuthBean authBean, HttpServletRequest request)  {
		RegisterDeviceResponseDTO response = new RegisterDeviceResponseDTO();
		//System.out.println("Auth Id "+authBean.getAuthId()+" "+authBean.getPassword()+"  "+authBean.getMetaID());
		sNSRepoService.insertAuthData(authBean.getAuthId(), authBean.getPassword(), authBean.getMetaID());
		return ResponseEntity.ok(response);
	}
}
