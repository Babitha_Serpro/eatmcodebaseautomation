package com.cca.ob.rest;

public class MerchLocationsReqDTO {
	private float uLt;
	private float uLng;
	private int count;
	public float getuLt() {
		return uLt;
	}
	public void setuLt(float uLt) {
		this.uLt = uLt;
	}
	public float getuLng() {
		return uLng;
	}
	public void setuLng(float uLng) {
		this.uLng = uLng;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	
	
}
