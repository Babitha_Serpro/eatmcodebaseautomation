package com.cca.ob.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cca.ob.dto.CheckStatusRequestDTO;
import com.cca.ob.dto.OrderStatusDTO;
import com.cca.ob.dto.TransactionDetails;
import com.cca.ob.enums.GatewayStatusMessage;
import com.cca.ob.service.OrderStatusService;
import com.cca.ob.service.RegistrationService;
import com.cca.ob.service.UserSessionService;
import com.cca.ob.util.TokenUtils;
@Controller
@RequestMapping("/api/v1")
public class OrderStatusRestAPI {
	@Autowired
	private RegistrationService registrationService;
	
	@Autowired
	private UserSessionService userSessionService;
	
	@Autowired
	private OrderStatusService OrderStatusService;
	
	@PostMapping(path = "/CheckStatus")
	public ResponseEntity<OrderStatusDTO> getPennyTransactionStatus(@RequestBody CheckStatusRequestDTO reqDto) {
		OrderStatusDTO response=new OrderStatusDTO();
		response.setmKey(reqDto.getmKey());
		response.setAuthKey(reqDto.getAuthKey()); 
		if (!registrationService.validateMobileNumberHashKey(reqDto.getmKey())) {
			response.setErrorCode("1003"); // come up with error codes for each error type
			response.setErrorMsg("Mobile number is not valid");
		} else if(!userSessionService.isValidSession(reqDto.getmKey(), reqDto.getAuthKey())) {
			response.setErrorCode("1010"); // come up with error codes for each error type
			response.setErrorMsg("Incosistent Session");
		}else {
			TransactionDetails transDetails=OrderStatusService.getOrderStatus(reqDto.getObTransId());
			if(transDetails==null) {
				response.setObTransactionReference(String.valueOf(reqDto.getObTransId()));
				response.setOrderSuccess("awaited");
			}else {
				response.setObTransactionReference(String.valueOf(transDetails.getTransactionId()));
				response.setAmount(String.valueOf(transDetails.getAmount()));
				response.setCurrency(transDetails.getCurrency());
				String status=transDetails.getOrderStatus();
				if(status!=null && "success".equalsIgnoreCase(GatewayStatusMessage.getStatusMessage(status).getStatus())) {
					response.setOrderSuccess("true");
				}else {
					response.setOrderSuccess("false");
				}				
			}
			response.setTokenNum("0");
			response.setAuthKey(userSessionService.refreshAuthCode(reqDto.getmKey())); 
		}
		
		return ResponseEntity.ok(response);
	}
	
	@PostMapping(path = "/GetToken")
	public ResponseEntity<OrderStatusDTO> getGeneratedToken(@RequestBody CheckStatusRequestDTO reqDto) {
		OrderStatusDTO response=new OrderStatusDTO();
		response.setmKey(reqDto.getmKey());
		response.setAuthKey(reqDto.getAuthKey()); 
		if (!registrationService.validateMobileNumberHashKey(reqDto.getmKey())) {
			response.setErrorCode("1003"); // come up with error codes for each error type
			response.setErrorMsg("Mobile number is not valid");
		} else if(!userSessionService.isValidSession(reqDto.getmKey(), reqDto.getAuthKey())) {
			response.setErrorCode("1010"); // come up with error codes for each error type
			response.setErrorMsg("Incosistent Session");
		}else {
			TransactionDetails transDetails=OrderStatusService.getOrderStatus(reqDto.getObTransId());
			if(transDetails==null) {
				response.setOrderSuccess("awaited");
				response.setObTransactionReference(String.valueOf(reqDto.getObTransId()));
			}else {
				long obTransId=transDetails.getTransactionId();
				response.setObTransactionReference(String.valueOf(obTransId));
				response.setAmount(String.valueOf(transDetails.getAmount()));
				response.setCurrency(transDetails.getCurrency());
				String status=transDetails.getOrderStatus();
				if(status!=null && "success".equalsIgnoreCase(GatewayStatusMessage.getStatusMessage(status).getStatus())) {
					response.setOrderSuccess("true");
					if(transDetails.getTokenNumber()!=null) {
						response.setTokenNum(TokenUtils.getZeroPaddedToken(transDetails.getTokenNumber().longValue()));
						long hiddenTokenNum=OrderStatusService.generateHiddenTokenIndex(reqDto.getmKey(), transDetails.getTokenNumber());
						response.setHidTokenIndex(hiddenTokenNum);
					}
					
				}else {
					response.setOrderSuccess("false");
					response.setTokenNum("0");
				}
			}
			
			response.setAuthKey(userSessionService.refreshAuthCode(reqDto.getmKey())); 
		}
		
		return ResponseEntity.ok(response);
	}
}
