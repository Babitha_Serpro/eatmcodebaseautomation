package com.cca.ob.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cca.ob.constants.OBConstants;
import com.cca.ob.dto.CardDTO;
import com.cca.ob.dto.CardDeletionReqBean;
import com.cca.ob.dto.CardDeletionResponseBean;
import com.cca.ob.dto.CardDetails;
import com.cca.ob.dto.CardDetailsRequestDTO;
import com.cca.ob.dto.CardDetailsResponseDTO;
import com.cca.ob.dto.CardValidationRequestBean;
import com.cca.ob.dto.CardValidationResponseBean;
import com.cca.ob.dto.CustomerInfo;
import com.cca.ob.dto.PGEndPoint;
import com.cca.ob.dto.RegisterCardRequestDTO;
import com.cca.ob.dto.RegisterCardResponseDTO;
import com.cca.ob.service.CacheService;
import com.cca.ob.service.RegistrationService;
import com.cca.ob.service.UserSessionService;
import com.cca.ob.service.WithdrawlService;
import com.cca.ob.util.CardUtils;

@RestController
@RequestMapping("/api/v1")
public class CardsRestAPI {
	@Autowired
	private WithdrawlService withdrawlService;
	@Autowired
	private RegistrationService registrationService;
	@Autowired
	private CacheService cacheService;
	@Autowired
	private UserSessionService userSessionService;
	
	private Logger logger=LogManager.getLogger(CardsRestAPI.class);
	
	@PostMapping(path = "/RegisterCard")
	public ResponseEntity<RegisterCardResponseDTO> registerCard(@RequestBody RegisterCardRequestDTO registerCardReq, HttpServletRequest request) {
		logger.debug("Request for RegisterCard "+registerCardReq.getmKey());
		RegisterCardResponseDTO response = new RegisterCardResponseDTO();
		response.setmKey(registerCardReq.getmKey());
		response.setAuthKey(registerCardReq.getAuthKey()); //TODO: Should it be refreshed?
	
		Object custInfo=cacheService.getCustomerInfo(registerCardReq.getmKey());
		if (!registrationService.validateMobileNumberHashKey(registerCardReq.getmKey())) {
			logger.debug("Request for RegisterCard "+registerCardReq.getmKey()+"\t Mobile number is not valid");
			response.setErrorCode("1003"); // come up with error codes for each error typeob
			response.setErrorMsg("Mobile number is not valid");
		} else if(custInfo==null ||  !userSessionService.isValidSession(registerCardReq.getmKey(), registerCardReq.getAuthKey())) {
			//System.out.println("custInfo ="+custInfo);
			logger.debug("Request for RegisterCard "+registerCardReq.getmKey()+"\t Incosistent Session");
			response.setErrorCode("1010"); // come up with error codes for each error type
			response.setErrorMsg("Incosistent Session");
		}else if (!CardUtils.isValidCard(registerCardReq.getCardNumber())) { // TODO: Logic is to verify the card number
			logger.debug("Request for RegisterCard "+registerCardReq.getmKey()+"\t Invalid card number");
			response.setErrorCode("1005"); // come up with error codes for each error type
			response.setErrorMsg("Invalid card number");
		}else if (!CardUtils.isValidExpiry(registerCardReq.getExpMonth(), registerCardReq.getExpYear())) { 
			logger.debug("Request for RegisterCard "+registerCardReq.getmKey()+"\tInvalid Card Details");
			response.setErrorCode("2001"); // come up with error codes for each error type
			response.setErrorMsg("Invalid Card Details");
		}else {
			String expMonth=registerCardReq.getExpMonth();
			String expYear=registerCardReq.getExpYear();
			if(expYear.length()==2) {
				expYear=OBConstants.YYYY_PREFIX+expYear;
			}
			CardDetails card=new CardDetails(registerCardReq.getCardNumber(), expMonth, expYear);
			long cardRefNo=registrationService.registerCard(((CustomerInfo)custInfo).getCustId(), card);
			logger.debug("Request for RegisterCard "+registerCardReq.getmKey()+"\t Card succesfully registered");
			if(cardRefNo<0) {
				response.setErrorCode("2000"); // come up with error codes for each error type
				response.setErrorMsg("Card Already Registered for the user");
				logger.debug("Request for RegisterCard "+registerCardReq.getmKey()+"\tCard Already Registered for the user");
			}else {
				card.setCardRefNum(cardRefNo);
				String ref=cacheService.storeCard(registerCardReq.getmKey(),card);
				response.setCardRefNum(ref);
			}
			response.setAuthKey( userSessionService.refreshAuthCode(registerCardReq.getmKey())); 
		}
		return ResponseEntity.ok(response);
	}
	
	@PostMapping(path = "/CardDetails")
	public ResponseEntity<CardDetailsResponseDTO> getCardDetails(@RequestBody CardDetailsRequestDTO reqDto, HttpServletRequest request) {
		logger.debug("Request for CardDetails "+reqDto.getmKey());
		CardDetailsResponseDTO response = new CardDetailsResponseDTO();
		response.setmKey(reqDto.getmKey());
		response.setAuthKey(reqDto.getAuthKey()); 
		//Object custInfo=request.getSession().getAttribute(OBConstants.CUSTOMER_INFO);
		Object custInfo=cacheService.getCustomerInfo(reqDto.getmKey());
		if (!registrationService.validateMobileNumberHashKey(reqDto.getmKey())) {
			logger.debug("Request for CardDetails "+reqDto.getmKey()+"\t Mobile number is not valid");
			response.setErrorCode("1003"); // come up with error codes for each error type
			response.setErrorMsg("Mobile number is not valid");
		} else if(custInfo==null ||  !userSessionService.isValidSession(reqDto.getmKey(), reqDto.getAuthKey())) {
			logger.debug("Request for CardDetails "+reqDto.getmKey()+"\t Incosistent Session");
			response.setErrorCode("1010"); // come up with error codes for each error type
			response.setErrorMsg("Incosistent Session");
		}else {
			
			response.setAuthKey(userSessionService.refreshAuthCode(reqDto.getmKey())); 
			List<CardDTO> cards=withdrawlService.getCardDetails(reqDto.getmKey(),((CustomerInfo)custInfo).getCustId());
			if(cards==null || cards.size()<1) {
				response.setErrorCode("2100"); // come up with error codes for each error type
				response.setErrorMsg("User has not registered any card");
				logger.debug("Request for CardDetails "+reqDto.getmKey()+"\t User has not registered any card");
			}else {
				response.setCardList(cards);
				logger.debug("Request for CardDetails "+reqDto.getmKey()+"\t User has one oe more registered  cards");
			}

		}
		return ResponseEntity.ok(response);
	}
	
	@PostMapping(path = "/ValidateCard")
	public ResponseEntity<CardValidationResponseBean> getCardDetails(@RequestBody CardValidationRequestBean reqDto, HttpServletRequest request) {
		logger.debug("Request for ValidateCard "+reqDto.getmKey());
		CardValidationResponseBean response=new CardValidationResponseBean();
		response.setmKey(reqDto.getmKey());
		response.setAuthKey(reqDto.getAuthKey()); 
		CardDetails card= cacheService.getCard(reqDto.getmKey(), reqDto.getCardReferenceNum());
		if (!registrationService.validateMobileNumberHashKey(reqDto.getmKey())) {
			response.setErrorCode("1003"); // come up with error codes for each error type
			response.setErrorMsg("Mobile number is not valid");
			logger.debug("Request for ValidateCard "+reqDto.getmKey()+"\t Mobile number is not valid");
		} else if(card==null ||!userSessionService.isValidSession(reqDto.getmKey(), reqDto.getAuthKey())) {
			response.setErrorCode("1010"); // come up with error codes for each error type
			response.setErrorMsg("Incosistent Session");
			logger.debug("Request for ValidateCard "+reqDto.getmKey()+"\t Incosistent Session");
		}else if(! CardUtils.isCvvVAlid(reqDto.getCvv()))  {
			response.setErrorCode("1600"); // come up with error codes for each error type
			response.setErrorMsg("Invalid CVV");
			logger.debug("Request for ValidateCard "+reqDto.getmKey()+"\t Invalid CVV");
		}else {
			response.setAuthKey(userSessionService.refreshAuthCode(reqDto.getmKey())); 
			card.setCvv(reqDto.getCvv());
			PGEndPoint pgEndpoint=withdrawlService.getPGEndPoint(card, 1.00f,"INR","Penny Transaction",true);
			response.setStpEndpoint(pgEndpoint.getPgEndPoint());
			response.setObTransId(pgEndpoint.getTransactionId());
			//String cardId=card.isEncrypted() ? new EncryptionUtils().decrypt(acard.getCardNumber()) :card.getCardNumber();
			int status=withdrawlService.updatePennyTransactionStatus(card.getCardRefNum(),pgEndpoint.getTransactionId());
			logger.debug("Request for ValidateCard "+reqDto.getmKey()+"\t Penny Transaction updated Status ="+status);
		}
		return ResponseEntity.ok(response);
	}
	
	@PostMapping(path = "/DeleteCard")
	public ResponseEntity<CardDeletionResponseBean> getCardDetails(@RequestBody CardDeletionReqBean reqDto, HttpServletRequest request) {
		logger.debug("Request for DeleteCard "+reqDto.getmKey());
		CardDeletionResponseBean response=new CardDeletionResponseBean();
		response.setmKey(reqDto.getmKey());
		response.setAuthKey(reqDto.getAuthKey()); 
		CardDetails card= cacheService.getCard(reqDto.getmKey(), reqDto.getCardReferenceNum());
		Object custInfo=cacheService.getCustomerInfo(reqDto.getmKey());
		if (!registrationService.validateMobileNumberHashKey(reqDto.getmKey())) {
			response.setErrorCode("1003"); // come up with error codes for each error type
			response.setErrorMsg("Mobile number is not valid");
			logger.debug("Request for DeleteCard "+reqDto.getmKey()+"\t Mobile number is not valid");
		} else if(custInfo==null || card==null ||!userSessionService.isValidSession(reqDto.getmKey(), reqDto.getAuthKey())) {
			response.setErrorCode("1010"); // come up with error codes for each error type
			response.setErrorMsg("Incosistent Session");
			logger.debug("Request for DeleteCard "+reqDto.getmKey()+"\t Incosistent Session");
		}else {
			response.setAuthKey(userSessionService.refreshAuthCode(reqDto.getmKey())); 
			boolean success=withdrawlService.deleteCard(card.getCardRefNum());
			if(success) {
				response.setDeletionSuccess(true);
				List<CardDTO> cards=withdrawlService.getCardDetails(reqDto.getmKey(),((CustomerInfo)custInfo).getCustId());
				if(cards==null || cards.size()<1) {
					response.setErrorCode("2100"); // come up with error codes for each error type
					response.setErrorMsg("User does not have  any registered card");
					logger.debug("Request for DeleteCard "+reqDto.getmKey()+"\t User has not registered any card");
				}else {
					response.setCardList(cards);
					logger.debug("Request for DeleteCard "+reqDto.getmKey()+"\t User has One or more registered Cards");
				}
			}else {
				response.setDeletionSuccess(false);
				response.setErrorCode("1032");
				response.setErrorMsg("Card Could not be deleted");
				//response.setCardIndex(reqDto.getCardReferenceNum());
			}

		}
		return ResponseEntity.ok(response);
	}
}
