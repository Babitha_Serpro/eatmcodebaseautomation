package com.cca.ob.rest;

public class MerchantLocaationReqDTO {
	private String mKey;
	private String authKey;
	private float currLat;
	private float currLong;
	
	public String getmKey() {
		return mKey;
	}
	public void setmKey(String mKey) {
		this.mKey = mKey;
	}
	public String getAuthKey() {
		return authKey;
	}
	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}
	public float getCurrLat() {
		return currLat;
	}
	public void setCurrLat(float currLat) {
		this.currLat = currLat;
	}
	public float getCurrLong() {
		return currLong;
	}
	public void setCurrLong(float currLong) {
		this.currLong = currLong;
	}
	
	
}
