package com.cca.ob.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cca.ob.util.OTPGenerator;
import com.cca.ob.util.Util;
@Service
public class UserSessionService {
	@Autowired
	private CacheService cacheService;
	
	public  boolean isValidSession(String mKey,String authKey) {
		Object authKeyInVault=cacheService.getAuthKey(mKey);
		if(authKeyInVault!=null && authKeyInVault.equals(authKey)) {
			return true;
		}
		return false;
	}
	

	
	public  String refreshAuthCode(String mKey) {
		String authKey= Util.hash(OTPGenerator.generate(8));
		cacheService.storeAuthKey(mKey,authKey);
		return authKey;
	}
	
}
