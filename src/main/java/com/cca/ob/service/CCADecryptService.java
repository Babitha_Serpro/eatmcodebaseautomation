package com.cca.ob.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cca.ob.dto.PGAuthBean;
import com.cca.ob.exception.OBException;
import com.cca.ob.repo.GatewayRepo;
import com.cca.ob.util.EncryptionUtils;
import com.ccavenue.security.AesCryptUtil;
@Service
public class CCADecryptService {
	@Autowired
	private GatewayRepo gatewayRepoImpl;
	
	@Value("${pg.account.identifier}")
	private String pgAccountId;
	
	public String decode(String encodedMessage) {
		PGAuthBean bean=gatewayRepoImpl.readAuth(pgAccountId);
		
		String wKey;
		try {
			wKey = new EncryptionUtils().decrypt(bean.getWorkingKey());
			 AesCryptUtil aesUtil=new AesCryptUtil(wKey);
			 return aesUtil.decrypt(encodedMessage);
		} catch (Exception e) {
			throw new OBException("Could not decode the encrypted call back response",e);
		} 
		
	}
}
