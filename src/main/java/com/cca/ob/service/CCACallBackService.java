package com.cca.ob.service;

import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cca.ob.constants.OBConstants;
import com.cca.ob.dto.TransactionDetails;
import com.cca.ob.enums.WithdrawlType;
import com.cca.ob.exception.OBException;
import com.cca.ob.repo.GatewayRepo;
import com.cca.ob.util.CCAvenueResponseParser;

@Service
public class CCACallBackService {

	@Autowired
	private GatewayRepo gatewayRepoImpl;
	@Autowired
	private CCADecryptService cCADecryptService;
	
	private static final Logger logger=LogManager.getLogger(CCACallBackService.class);
	
	
	public TransactionDetails  calledBack(Map<String, String> response, String callBackSource) {
		String encoded=response.get(OBConstants.ENCODED_RESPONSE); 
		final String decoded=cCADecryptService.decode(encoded);
		CCAvenueResponseParser parser=new CCAvenueResponseParser(decoded);
		String transactId=parser.getValue(CCAvenueResponseParser.ORDER_ID);

		if(transactId==null || transactId.length()==0) {
			logger.error("The call back "+callBackSource+"\t doesnt contain OB Transaction Id "+decoded);
			throw new OBException("The Callback received doesnt contain the Transaction Id" );
		}
		
		gatewayRepoImpl.logResponsePayload(Long.parseLong(transactId), encoded);
		String statusMessage=parser.getValue(CCAvenueResponseParser.STATUS_MESSAGE);
		if(statusMessage==null || statusMessage.length()==0) {
			logger.error("The call back "+callBackSource+"\t doesnt contain StatusMessage "+decoded);
		}
		String amount=parser.getValue(CCAvenueResponseParser.AMOUNT);
		Double amnt=(amount==null ? null:Double.valueOf(amount));
		//System.out.println("String amount "+amount+" double "+amnt);
		
		TransactionDetails tDetails=new TransactionDetails(Long.parseLong(transactId), parser.getValue(CCAvenueResponseParser.TRACKING_ID),
						parser.getValue(CCAvenueResponseParser.BANK_REF_NO), parser.getValue(CCAvenueResponseParser.ORDER_STATUS), 
						parser.getValue(CCAvenueResponseParser.STATUS_CODE),parser.getValue(CCAvenueResponseParser.STATUS_MESSAGE),
						parser.getValue(CCAvenueResponseParser.FAILURE_MSG), amnt,
						parser.getValue(CCAvenueResponseParser.PAYMENT_MODE), parser.getValue(CCAvenueResponseParser.CURRENCY));
		
		gatewayRepoImpl.insertTransactionDetails(tDetails, callBackSource);
		//Update the token generation here...
		String withdrawlType=parser.getValue(CCAvenueResponseParser.WITHDRAWL_TYPE);
		if(isWithdrawal(withdrawlType,amnt.intValue())){
			//System.out.println("Setting Penny Transaction to false :");
			tDetails.setPennyTransaction(false);
		}else {
			tDetails.setPennyTransaction(true);
			//System.out.println("Setting Penny Transaction to true :");
		}
		return tDetails;
	}
	
	private boolean isWithdrawal(String withdrawlType,int amount) {
		//System.out.println("withdrawlType :"+withdrawlType+"\t amount "+amount);
		if(withdrawlType==null || WithdrawlType.WITHDRAWAL.getType().equalsIgnoreCase(withdrawlType) || amount>1 ) {
			return true;
		}
		return false;
	}
	
}
