package com.cca.ob.service.pg;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cca.ob.dto.CardDetails;
import com.cca.ob.dto.PGEndPoint;
import com.cca.ob.enums.WithdrawlType;
import com.cca.ob.exception.OBException;
import com.cca.ob.repo.GatewayRepo;
import com.cca.ob.util.EncryptionUtils;
@Service
public class CCATransactionService {
	
	@Value("${pg.redirect.url}")
	private String redirectURL;
	
	@Value("${pg.cancel.url}")
	private String cancelURL;
	
	
	@Value("${cca.transaction.prodEndpoint}")
	private String ccaProductionHost;
	
	@Value("${cca.transaction.testEndpoint}")
	private String ccaTestHost;
	
	@Autowired
	private GatewayRepo gatewayRepoImpl;
	
	private Logger logger=LogManager.getLogger(CCATransactionService.class);
	public PGEndPoint getEncodedURL(CardDetails card,double amount,long obOrdID,boolean isPennyTransaction,String ccaWorkingKey,String merchantId,String accessCode) {
		Map<String, String> params;
		logger.info("Preparing the Request Payload for Withdrawl for OB Order Id "+obOrdID+" & Card Number ="+card.getCardNumber());
		try {
			params = populateReqParams(card,amount,merchantId,obOrdID,isPennyTransaction);
		} catch (Exception e) {
			logger.error("Could not decode the Card details ",e);
			throw new OBException("Could not decode the Card details",e);
		}
		String encryptedReq=ecrypt(params,ccaWorkingKey);
		gatewayRepoImpl.logRequestPayload(obOrdID, encryptedReq);
		String endPoint= new CCAEndpointBuilder().getTransactionEndpoint(ccaProductionHost,encryptedReq,accessCode);
		return new PGEndPoint(obOrdID,endPoint);
	}
	
	private Map<String,String> populateReqParams(CardDetails card,double amount,String merchId,long obOrdID,boolean isPennyTransaction) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException{
		DecimalFormat df = new DecimalFormat("0.00");
		Map<String,String> reqPArams=new HashMap<String,String>();
		
		reqPArams.put("merchant_id", merchId);
		reqPArams.put("order_id", String.valueOf(obOrdID));
		reqPArams.put("currency", "INR");
		
		reqPArams.put("amount", df.format(amount));
		reqPArams.put("payment_option", "OPTDBCRD"); //Debit Card
		reqPArams.put("card_type", "DBCRD"); //Debit Card
		reqPArams.put("data_accept", "Y");//Expected values – Y or N
		
		logger.debug("Card is Encrypted ??"+card.isEncrypted());
		if(card.isEncrypted()) {
			reqPArams.put("card_number", new EncryptionUtils().decrypt(card.getCardNumber()));
			reqPArams.put("expiry_month", new EncryptionUtils().decrypt(card.getExpiryMonth()));
			reqPArams.put("expiry_year", new EncryptionUtils().decrypt(card.getExpiryYear()));
		}else {
			reqPArams.put("card_number", card.getCardNumber());
			reqPArams.put("expiry_month", card.getExpiryMonth());
			reqPArams.put("expiry_year", card.getExpiryYear());
		}
		
		
		reqPArams.put("cvv_number", card.getCvv());
		
		//Name of the card used by the customer. This list will be provided by CCAvenue.
		//For debit cards leave this empty
		reqPArams.put("card_name", ""); 
		//reqPArams.put("issuing_bank", "10.00"); //Card issuing bank name 
		if(isPennyTransaction) {
			reqPArams.put("merchant_param1",WithdrawlType.PENNYTRANSACTION.getType()); 
		}else {
			reqPArams.put("merchant_param1",WithdrawlType.WITHDRAWAL.getType()); 
		}
		
		reqPArams.put("redirect_url",redirectURL);
		reqPArams.put("cancel_url", cancelURL);
		
		
		return reqPArams;
		
	}
	private String ecrypt(Map<String,String> reqPArams,String workingKey) {
		String ccaRequest="";
		for(Map.Entry<String,String> anEntry: reqPArams.entrySet()) {
		      ccaRequest = ccaRequest + anEntry.getKey() + "=" + anEntry.getValue() + "&";
		 }
		return CCAEncodeUtil.encode(ccaRequest,workingKey);
	}
}
