package com.cca.ob.service.pg;

import com.ccavenue.security.AesCryptUtil;

public class CCAEncodeUtil {

	public static String encode(String ccaRequest,String ccaWorkingKey) {
		 AesCryptUtil aesUtil=new AesCryptUtil(ccaWorkingKey);
		 return aesUtil.encrypt(ccaRequest);
	}

}
