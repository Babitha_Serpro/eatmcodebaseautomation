package com.cca.ob.service.pg;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cca.ob.dto.PGAuthBean;
import com.cca.ob.exception.OBException;
import com.cca.ob.repo.GatewayRepo;
import com.cca.ob.service.CCADecryptService;
import com.cca.ob.util.EncryptionUtils;
import com.cca.ob.util.HttpExecutor;
import com.google.gson.JsonObject;


@Service
public class OrderStatusConfirmationService {
	@Value("${pg.account.identifier}")
	private String pgAccountId;
	@Autowired
	private GatewayRepo gatewayRepoImpl;
	
	@Value("${cca.api.prodEndpoint}")
	private String ccaApiProdHost;
	private Logger logger=LogManager.getLogger(OrderStatusConfirmationService.class);
	
	@Autowired
	private CCADecryptService cCADecryptService;
	
	
	public void confirmStatus(long transactionId) {
		try {
			PGAuthBean bean=gatewayRepoImpl.readAuth(pgAccountId);
			String jsonReq=getJsonReq(transactionId);
			logger.info("+The Json = "+jsonReq);
			//Map<String,String> reqParamMAp=getReqPAram();
			//System.out.println("Json Request ="+jsonReq);
			String encodedRequest=CCAEncodeUtil.encode(jsonReq,
					new EncryptionUtils().decrypt(bean.getWorkingKey()));
			
			//String encodeRequest=encrypt(reqParamMAp,new EncryptionUtils().decrypt(bean.getWorkingKey()));
			String endPoint=new CCAEndpointBuilder().getOrderStatusConfirmationEndPoint(ccaApiProdHost,encodedRequest, 
					new EncryptionUtils().decrypt(bean.getAccessCode()),"JSON");
			Map<String,String> postParams=getPostParamsMap(bean, encodedRequest);
			new HttpExecutor().doPost(endPoint, postParams);
			//new HttpExecutor().sendGet(endPoint);
			//System.out.println("Order Status Confirmatin Endpoint ="+endPoint);
			
			//String decodedReqParams=cCADecryptService.decode(encodedRequest);
			//String response=new PostExecutor().postJson(endPoint, encodedRequest);
			
			//System.out.println("Order Status ReqParams   ="+decodedReqParams);
			//System.out.println("Order Status Confirmatin Response  ="+response);
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new OBException("Error while Encoding the Gateway URL", e);
		} 
	}

	

	private String getJsonReq(long transactionId) {
		JsonObject obj=new JsonObject();
		obj.addProperty("order_no", String.valueOf(transactionId));
		
		return obj.toString();
	}
	
	private Map<String,String> getPostParamsMap(PGAuthBean bean,String encReq) throws Exception{
		Map<String,String> mp=new HashMap<String, String>();
		mp.put("command", "orderStatusTracker");
		mp.put("access_code", new EncryptionUtils().decrypt(bean.getAccessCode()));
		mp.put("enc_request", encReq);
		mp.put("request_type", "JSON");
		mp.put("version", "1.1");
		return mp;
	}
	/*
	 * private Map<String, String> getReqPAram() { Map<String, String> mp=new
	 * HashMap<String, String>(); mp.put("merchant_id", "245717");
	 * mp.put("order_no", "250"); mp.put("reference_no", "109765423293");
	 * 
	 * return mp; }
	 */
	
}
