package com.cca.ob.service.pg;

import org.apache.http.client.utils.URIBuilder;
import org.springframework.stereotype.Service;
@Service
public class CCAEndpointBuilder {
	
	public String getTransactionEndpoint(String host,String encodedReq,String accessCode) {
		return getTransactionEndpoint(host,"initiateTransaction", encodedReq, accessCode);
	}
	public String getOrderStatusConfirmationEndPoint(String host,String encodedReq,String accessCode,String reqType) {
		return getStusCheckEndpoint(host,"orderStatusTracker", encodedReq, accessCode,reqType);
	}
	
	private String getTransactionEndpoint(String host,String command,String encodedReq,String accessCode) {
		URIBuilder builder = new URIBuilder()
			    .setScheme("https")
			    .setHost(host)
			    .setPath("transaction.do")
			    .addParameter("command", command)
			    .addParameter("encRequest", encodedReq)
			    .addParameter("access_code", accessCode);
				
			return builder.toString();
	}
	private String getStusCheckEndpoint(String host,String command,String encodedReq,String accessCode,String requestType) {
		URIBuilder builder = new URIBuilder()
			    .setScheme("https")
			    .setHost(host);
			    //.addParameter("command", command);
			   // .addParameter("enc_request", encodedReq)
			   // .addParameter("access_code", accessCode)
				// .addParameter("request_type", requestType)
				// .addParameter("version", "1.1");
				 
			return builder.toString();
	}
}
