package com.cca.ob.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.cca.ob.bc.response.CallBackResponseHandler;
import com.cca.ob.bc.validators.TokenValidationStatusBean;
import com.cca.ob.bc.validators.TokenValidatorsEnum;
import com.cca.ob.bc.validators.Validator;
import com.cca.ob.constants.BCConstants;
import com.cca.ob.constants.TokenProcessConstants;
import com.cca.ob.dto.BCDetails;
import com.cca.ob.dto.MerchantDetails;
import com.cca.ob.repo.BCRepository;
import com.cca.ob.util.EncryptionUtils;
import com.cca.ob.util.TokenProcessUtils;


public abstract class AbstractProcessTokenService {

	@Autowired
	private BCRepository bcRepository;


	@Autowired
	private CallBackResponseHandler responseHandler;

	@Value("${token.dayvalidation.max}")
	private int maxTokenValidationAttempts;

	private static Logger logger=LogManager.getLogger(AbstractProcessTokenService.class);

	public Map<String,String> getResponse(Map<String, String> plainRequest,String requestType,String accessCode,Long reqDumpID,final BCDetails aggregator){
		Long obReqId=createObRequestId(reqDumpID,plainRequest);
		Map<String, String> responseMap=getDefaultResponseMap(plainRequest,obReqId);

		MerchantDetails merchDetails=validateParams(plainRequest,responseMap, accessCode, reqDumpID, aggregator);
		if(merchDetails==null) {
			return responseMap;
		}
		Validator validator=getValidator(plainRequest.get(BCConstants.REQUEST_PARAM_BANK_IDENTIFIER));


		boolean flag=validator.validateRequiredParams(plainRequest,  responseMap);
		if(!flag) {
			return responseMap;
		}
		if(TokenValidatorsEnum.EATM.getValidator().equalsIgnoreCase(plainRequest.get(BCConstants.REQUEST_PARAM_BANK_IDENTIFIER))) {
			TokenValidationStatusBean status =validator.getValidationStatus(responseMap,obReqId,merchDetails.getMerchID(), merchDetails.getAggregatorId(),
					plainRequest.get(BCConstants.REQ_PARAM_BC_REF_NUM));
			//updateValidationStatus(status, responseMap);
			TokenProcessUtils.populateValidationStatus(responseMap, status,isReversal());

		}else {
			responseMap.put(TokenProcessConstants.RESPONSE_AWAITED, TokenProcessConstants.RESPONSE_AWAITED);
			new Thread(()->{

				TokenValidationStatusBean status =validator.getValidationStatus(responseMap,obReqId,merchDetails.getMerchID(), merchDetails.getAggregatorId(),
						plainRequest.get(BCConstants.REQ_PARAM_BC_REF_NUM));
				// updateValidationStatus(status, responseMap);
				TokenProcessUtils.populateValidationStatus(responseMap, status,isReversal());
				responseMap.remove(TokenProcessConstants.RESPONSE_AWAITED);
				responseHandler.postResponseToCallBackURL(aggregator, responseMap,requestType);

			}).start();

		}


		return responseMap;
	}

	/*
	 * private void updateValidationStatus(TokenValidationStatusBean
	 * status,Map<String, String> responseMap) { if(status!=null &&
	 * status.isValidationStatus() && status.getAmount()>0) {
	 * responseMap.put(BCConstants.RESP_PARAM_DISPENSE_CASH, "true");
	 * responseMap.put(BCConstants.RESP_PARAM_AMOUNT,
	 * String.valueOf(status.getAmount())); }
	 * 
	 * }
	 */

	protected abstract Map<String,String> getDefaultResponseMap(Map<String, String> plainRequest,Long obReqId) ;

	protected abstract Validator getValidator(String bankIdentifier);

	protected abstract boolean isReversal();

	protected abstract String[] getMandatoryParam();

	protected abstract long createObRequestId(Long reqDumpID,Map<String,String> plainRequest);

	private MerchantDetails  validateParams(Map<String, String> plainRequest,Map<String, String> responseMap,String accessCode,Long reqDumpID,BCDetails aggregator) {
		//Check for invalid encrypted Data
		if(plainRequest==null||plainRequest.isEmpty()) {
			populateErrors("AG113","Invalid Encrypted Data",responseMap);
			return null;
		}
		//store this request as token_Validation_Request

		boolean missingReqdParams=checkMandatoryParams(plainRequest,responseMap);
		if(missingReqdParams) {
			populateErrors("AG101","Missing Required Parameters",responseMap);
			return null;
		}
		//Check for maximum 3 transactions per day per mobilenumber - only for request type - T
		if(plainRequest.get(BCConstants.REQUEST_PARAM_REQUEST_TYPE).equalsIgnoreCase("t")) {
			if(checkMaxmimumTransactionPerDayReached(plainRequest)) {
				populateErrors("AG111","Mobile Number has reached maximum transactions per day.",responseMap);
				return null;
			}
		}
		if(!plainRequest.get(BCConstants.REQ_PARAM_ACCESS_CODE).equals(accessCode)){
			populateErrors("AG102","Inconsistent AccessCode ",responseMap);
			return null;
		}
		MerchantDetails merchDetails=bcRepository.getMerchant(plainRequest.get(BCConstants.REQ_PARAM_MERCHANT_ID));
		if(merchDetails==null) {
			populateErrors("AG103","Incosistent Details ",responseMap);
			return null;
		}
		if(!merchDetails.isAggregatorEnabled()) {
			populateErrors("AG104","Aggregator Not enabled ",responseMap);
			return null;
		}
		if(aggregator==null) {
			populateErrors("AG105","Incorrect Access Code",responseMap);
			return null;
		}
		if(!aggregator.getBcId().equalsIgnoreCase(plainRequest.get(BCConstants.REQ_PARAM_BC_ID))) {
			populateErrors("AG112","Incorrect BCID.",responseMap);
			return null;
		}
		if(!aggregator.getBcId().equalsIgnoreCase(merchDetails.getBcId())) {
			populateErrors("AG106","Incorrect BCID.",responseMap);
			return null;
		}
		if(containsSpecialCharacter(plainRequest.get(BCConstants.REQ_PARAM_BC_REF_NUM))) {
			populateErrors("AG116","Incorrect BC_REF_NUM.",responseMap);
			return null;
		}
		
		
		//Validate Unique BC_REF_NUM 

		if(!checkUniqueBCREFNO(plainRequest)) {
			populateErrors("AG117","Invalid BC_REF_NUM.",responseMap);
			return null;
		}
		//Validate Original_BC_REF_NUM with the transaction BC_REF_NUM
		if(plainRequest.get(BCConstants.REQUEST_PARAM_REQUEST_TYPE).equalsIgnoreCase("R")) {
			if(plainRequest.get(BCConstants.REQUEST_PARAM_ORIGINAL_EATM_REF_NUM)!=null && !checkOriginalBCREFNO(plainRequest)) {
				populateErrors("AG114","Invalid Original_BC_REF_NUM.",responseMap);
				return null;
			}
			
			
			
			//Loop Fixes
			if(plainRequest.get(BCConstants.REQUEST_PARAM_ORIGINAL_EATM_REF_NUM)==null && checkInvalidOriginalBCREFNO(plainRequest) ) {
				populateErrors("AG114","Invalid Original_BC_REF_NUM.",responseMap);
				return null;
			}	
			//
			if(containsSpecialCharacter(plainRequest.get(BCConstants.REQUEST_PARAM_ORIGINAL_BC_REF_NUM))) {
				populateErrors("AG115","Incorrect Original_BC_REF_NUM.",responseMap);
				return null;
			}
			if(!containsTimestamp(plainRequest.get(BCConstants.REQUEST_PARAM_ORIGINAL_REQUEST_TIMESTAMP))) {
				populateErrors("AG118","Incorrect Original_req_timestamp.",responseMap);
				return null;
			}
		}
		
		if(!plainRequest.get(BCConstants.REQ_PARAM_AGGREGATOR_ID).equalsIgnoreCase(merchDetails.getAggregatorId())){
			populateErrors("AG107","Incorrect AGGREGATOR ID.",responseMap);
			return null;
		}
		if(!merchDetails.isEnabled()) {
			populateErrors("AG108"," Merchant not enabled",responseMap);
			return null;
		}

		if(!merchDetails.isBcEnabled()) {
			populateErrors("AG109","BC not enabled",responseMap);
			return null;
		}
		String reqType=plainRequest.get(BCConstants.REQUEST_PARAM_REQUEST_TYPE);
		if(!("t".equalsIgnoreCase(reqType) || "r".equalsIgnoreCase(reqType))){
			populateErrors("AG110","Incorrect Request Type.",responseMap);
			return null;
		}
		if("r".equalsIgnoreCase(reqType)) {
			String reversalReasonCode=plainRequest.get(BCConstants.REQUEST_PARAM_REV_REASON_CD);
			String reversalReason=plainRequest.get(BCConstants.REQUEST_PARAM_REV_REASON);


			
			
			if(reversalReasonCode==null || reversalReasonCode.trim().length()==0 || (!"ABCXYZO".contains(reversalReasonCode))|| reversalReasonCode.length()>1) {
				populateErrors("AG171","Reason Code for Reversal Not Valid",responseMap);
				return null;				 
			}
			if("O".equalsIgnoreCase(reversalReasonCode) && reversalReason==null) {
				populateErrors("AG172","Reversal Reason Mandatory for Other Reasons",responseMap);
				return null;	
			}else if("O".equalsIgnoreCase(reversalReasonCode) && (reversalReason.trim().length()<5 || reversalReason.trim().length()>100)) {
				populateErrors("AG172","Reversal Reason should have minimum character 5 and maximum character 100",responseMap);
				return null;
			}
			//Loop Fixes
			else if(reversalReason!=null && containsSpecialCharacterWithSpace(reversalReason)) {
				populateErrors("AG172","Invalid Reversal Reason",responseMap);
				return null;
			}//
		}

		String validatorIdentifier=plainRequest.get(BCConstants.REQUEST_PARAM_BANK_IDENTIFIER);
		if(validatorIdentifier==null || TokenValidatorsEnum.getValidator(validatorIdentifier)==null) {
			populateErrors("AG201","Invalid Bank Idnetifier",responseMap);
			return null;
		}

		return merchDetails;
	}
	
	public static boolean containsTimestamp(String inputString)
	{ 
	    SimpleDateFormat format = new java.text.SimpleDateFormat("yyyyMMddHHmmss");
	    try{
	       format.parse(inputString);
	       return true;
	    }
	    catch(ParseException e)
	    {
	        return false;
	    }
	}
	
	private boolean containsSpecialCharacter(String text) {
		Pattern pattern = Pattern.compile("[^a-zA-Z0-9]");
		Matcher matcher = pattern.matcher(text);		
		return matcher.find();
	}

	private boolean containsSpecialCharacterWithSpace(String text) {
		Pattern pattern = Pattern.compile("[^a-zA-Z0-9\\s]");
		Matcher matcher = pattern.matcher(text);		
		return matcher.find();
	}
	private boolean checkMandatoryParams(Map<String, String> plainRequest, Map<String, String> respParam) {
		String[] mandatoryReqParams= getMandatoryParam();
		return checkMissingRequiredParams(mandatoryReqParams, plainRequest, respParam);
	}

	private boolean checkMissingRequiredParams(String reqParamName[],Map<String, String> plainRequest, Map<String, String> respParam) {
		boolean paramMissing=false;
		String value=null;
		for(String aMandatoryParam:reqParamName) {
			value=plainRequest.get(aMandatoryParam);
			respParam.put(aMandatoryParam, value);
			if(value==null) {
				respParam.put(BCConstants.RESP_PARAM_ERROR_CODE,"AG101");
				respParam.put(BCConstants.RESP_PARAM_ERROR_MESSAGE,"Missing Mandatory Request Parameter "+reqParamName);
				paramMissing=true;
			}
		}
		return paramMissing;

	}

	private void  populateErrors(String errorCd,String errMsg,Map<String,String> respMap) {
		respMap.put(BCConstants.RESP_PARAM_ERROR_CODE,errorCd);
		respMap.put(BCConstants.RESP_PARAM_ERROR_MESSAGE,errMsg);
	}



	public String getRequestType() {
		if(isReversal()) {
			return "R";
		}
		return "T";
	}

	public String getUserMobileEncrypted(Map<String,String> plainRequest) {

		String usrMobileParam=plainRequest.get(BCConstants.REQUEST_PARAM_USER_MOBILE_NUM);
		String encryptedMobileNumber=null;

		if(usrMobileParam!=null && usrMobileParam.length()==10) {
			try {
				encryptedMobileNumber=new EncryptionUtils().encrypt(usrMobileParam);
			}catch(Exception e) {
				logger.error("Could not encrypt the mobile number ",e);
			}
		}
		return encryptedMobileNumber;
	}

	public Double[] getMerchantLatLong(Map<String,String> plainRequest) {

		Double[] latLong=new Double[2];
		String merChLatLongParam=plainRequest.get(BCConstants.REQUEST_PARAM_MER_LATLONG);
		Double merLat=null;
		Double merLong=null;
		if(merChLatLongParam!=null) {
			String latLongArr[]=merChLatLongParam.split(",");
			if(latLongArr!=null && latLongArr.length==2) {
				try {
					merLat=Double.parseDouble(latLongArr[0]);
					merLong=Double.parseDouble(latLongArr[1]);	
					latLong[0]=merLat;
					latLong[1]=merLong;
				}catch(Exception e) {
					logger.error("Lat longs are not Double values "+latLongArr[0]+","+latLongArr[1],e);
				}
			}else {
				logger.debug("Incorrect Lat Long parameter "+merChLatLongParam);
			}
		}
		return latLong;
	}

	protected abstract boolean checkMaxmimumTransactionPerDayReached(Map<String,String> plainRequest);
	protected abstract boolean checkOriginalBCREFNO(Map<String,String> plainRequest);
	protected abstract boolean checkInvalidOriginalBCREFNO(Map<String,String> plainRequest);
	protected abstract boolean checkUniqueBCREFNO(Map<String,String> plainRequest);



}
