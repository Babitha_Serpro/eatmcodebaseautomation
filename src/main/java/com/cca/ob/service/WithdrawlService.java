package com.cca.ob.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cca.ob.dto.CardDTO;
import com.cca.ob.dto.CardDetails;
import com.cca.ob.dto.PGAuthBean;
import com.cca.ob.dto.PGEndPoint;
import com.cca.ob.exception.OBException;
import com.cca.ob.repo.GatewayRepo;
import com.cca.ob.repo.WithDrawlRepository;
import com.cca.ob.service.pg.CCATransactionService;
import com.cca.ob.util.CardUtils;
import com.cca.ob.util.EncryptionUtils;

@Service
public class WithdrawlService {

	@Autowired
	private WithDrawlRepository withDrawlRepositoryImpl;
	@Autowired
	private GatewayRepo gatewayRepoImpl;
	
	@Autowired
	private CCATransactionService cCAPGService;
	@Autowired
	private CacheService cacheService;
	
	@Value("${pg.account.identifier}")
	private String pgAccountId;
	
	public List<CardDTO> getCardDetails(String mobileKey,long custID) {
		List<CardDetails> cards=withDrawlRepositoryImpl.getCards(custID);
		List<CardDTO> responseList=new ArrayList<CardDTO>();
		CardDTO cardDTO;
		for(CardDetails acard:cards) {
			cardDTO=new CardDTO();
			String ref=cacheService.storeCard(mobileKey,acard);
			cardDTO.setCardIndex(ref);
			try {
				cardDTO.setMaskedCardNumber(CardUtils.maskCard(new EncryptionUtils().decrypt(acard.getCardNumber())));
			}catch(Exception e) {
				throw new OBException("The card number could not be decrypted", e);
			}
			cardDTO.setPennyTransactionPending(acard.isPennyTransactionPending());
			responseList.add(cardDTO);
		}
		return responseList;
	}
	
	
	public PGEndPoint getPGEndPoint(CardDetails card,double amount,String currency,String comments,boolean isPennyTransaction) {
		PGAuthBean bean=gatewayRepoImpl.readAuth(pgAccountId);
		long transactionId=gatewayRepoImpl.createTransaction(card.getCardRefNum(),amount,currency,comments);
		try {
			return cCAPGService.getEncodedURL(card, amount, transactionId ,isPennyTransaction,
					new EncryptionUtils().decrypt(bean.getWorkingKey()),
					new EncryptionUtils().decrypt(bean.getMerchId()),
					new EncryptionUtils().decrypt(bean.getAccessCode()));
		} catch (Exception e) {
			throw new OBException("Error while Encoding the Gateway URL", e);
		} 
	}
	
	public int updatePennyTransactionStatus(long cardID,long transactionId) {
		return withDrawlRepositoryImpl.updatePennyTransactionId(cardID,transactionId);
	}
	
	public boolean deleteCard(long cardId) {
		return withDrawlRepositoryImpl.deleteCard(cardId);
	} 
}
