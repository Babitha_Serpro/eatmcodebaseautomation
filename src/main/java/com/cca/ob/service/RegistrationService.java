package com.cca.ob.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cca.ob.dto.CardDetails;
import com.cca.ob.dto.CustomerInfo;
import com.cca.ob.dto.PhoneDetails;
import com.cca.ob.dto.PhoneRegistrationStatus;
import com.cca.ob.exception.OBException;
import com.cca.ob.repo.RegistrationRepository;
import com.cca.ob.util.EncryptionUtils;
import com.cca.ob.util.OTPGenerator;
import com.cca.ob.util.Util;

@Service
public class RegistrationService {
	@Autowired
	private RegistrationRepository registrationRepositoryImpl;
	
	@Autowired
	private TextSMSService sNSService;	
	
	
	private Map<String, String> otpCache = new ConcurrentHashMap<String, String>();
	private Map<String, PhoneDetails> mobileCache = new ConcurrentHashMap<String, PhoneDetails>();
	
	
	public static final String OTP_RESEND_COUNTER = "OTPResendCounter";
	public static final String OTP_FAILED_COUNTER = "OTPFailedCounter";
	

	@Value("${otp.message.suffix}")
	private String otpMsgSuffix;
	
	@Value("${otp.message.senderid}")
	private String otpMsgSenderId;
	
	@Value("${otp.byepass.enabled}")
	private boolean bypassOtpEnabled;
	
	@Value("${otp.byepass.value}")
	private String otpByepassValue;
	
	public PhoneRegistrationStatus registerMobileNumber(String mKey,String mNo,String countryCode,String userName) {
		boolean duplicatePoneNum=false;
		long custId=-1;
		if(countryCode==null||countryCode.length()<1) {
			countryCode="+91";
		}else if(!(countryCode.startsWith("+"))) {
			countryCode="+"+countryCode;
		}
		CustomerInfo cust=null;
		try {
			custId=registrationRepositoryImpl.registerPhone(countryCode, mNo,userName);
			cust=new CustomerInfo(countryCode, mNo, userName, custId);
		}catch(Exception ex) {
			if(ex.getMessage().contains("java.sql.SQLIntegrityConstraintViolationException")){
				duplicatePoneNum=true;
				//ignore and continue with registration
			}else {
				throw ex;
			}
		}
		if(duplicatePoneNum) {
			cust=registrationRepositoryImpl.getCustomer(countryCode, mNo);
		}

		//mNo=countryCode+mNo;
		cacheMobileNumber(mKey,countryCode, mNo);
		return initiateNewOTP( mKey, cust.getCountryCode()+cust.getmNo(),duplicatePoneNum,cust);
	}
		
	public PhoneRegistrationStatus resetOTP(String mKey) {
		PhoneDetails phoneDetails=getCachedPhoneDetails(mKey);
		return initiateNewOTP( mKey, phoneDetails.getFullyQualifiedPhoneNumber(),false,null);
	}
	
	
	public long registerCard(long custId,CardDetails card) {
		try {
			return registrationRepositoryImpl.registerCard(custId,new EncryptionUtils().encrypt(card.getCardNumber()),
													new EncryptionUtils().encrypt(card.getExpiryMonth()),
													new EncryptionUtils().encrypt(card.getExpiryYear()));
		} catch (Exception ex) {
			if(ex.getMessage().contains("java.sql.SQLIntegrityConstraintViolationException")){
				//ignore and continue with registration
				return -1;
			}
			throw new OBException("Error while Inserting card details",ex);
		} 
	}
	private PhoneRegistrationStatus initiateNewOTP(String mKey,String fullyQualifiedMNo,boolean isDuplicate,CustomerInfo cust) {


		String otp = generateOTP();
		String otpKey = generateOTPKey(otp);
		cacheOTP(mKey, otpKey, otp);

		new Thread(()->{
			String confirmationId=sNSService.sendSMSMessage(fullyQualifiedMNo, otp+" "+otpMsgSuffix,otpMsgSenderId);
			registrationRepositoryImpl.insertSMSConfirmation(fullyQualifiedMNo, "AWS_SNS", confirmationId);
			
		}) .start();
				
		return new PhoneRegistrationStatus(otpKey, isDuplicate,cust);
	}
	private String generateOTP() {
		String otp=OTPGenerator.generate(6);
		return otp; // TODO: generate and cache the OTP against the mobile.
	}
	
	private String generateOTPKey(String otp) {
		return Util.hash(otp); // TODO: generate and cache the OTPKey against the mobile.
	}
	
	
	
	public boolean validateMobileNumber(String mobNumber) {
		//TODO: add more validations on mobile number
		if (mobNumber == null || mobNumber.length() != 10) {
			return false;
		}
		return true;
	}
	public boolean validateUserName(String cName) {
		//TODO: add more validations on mobile number
		if (cName == null || cName.length() == 0) {
			return false;
		}
		return true;
	}
	public boolean validateOTP(String mobHashKey, String otpKey, String otp) {
		if (otp != null && otp.equals(otpCache.get(mobHashKey + "|" + otpKey))) {
			//TODO: Delete key from map
			otpCache.remove(mobHashKey + "|" + otpKey);
			return true;
		}
		return false;
	}
	
	public boolean validateMobileNumberHashKey(String mobHashKey) {
		if (mobileCache.containsKey(mobHashKey)) {
			return true;
		}
		return false;
	}
	
	public void cacheOTP(String mobHashKey, String otpKey, String otp) {
		otpCache.put(mobHashKey + "|" + otpKey, otp);
	}
	
	private void cacheMobileNumber(String mobHashKey,String countryCode, String phoneNumber) {
		mobileCache.put(mobHashKey, new PhoneDetails(countryCode, phoneNumber));
	}
	private PhoneDetails getCachedPhoneDetails(String mobHashKey) {
		return mobileCache.get(mobHashKey);
	}
	
	public void incrementCounter(String counterName, HttpSession session) {
		Integer counter = (Integer) session.getAttribute(counterName);
		if (counter == null) {
			counter = new Integer(0);
		}
		session.setAttribute(counterName, (counter.intValue() + 1));
	}


	public CustomerInfo getCustomerInfo(String mKey) {
		PhoneDetails phone=mobileCache.get(mKey);
		return registrationRepositoryImpl.getCustomer(phone.getCountryCode(), phone.getPhoneNum());
	}
	
	/*
	 * public CustomerInfo getCustomerCardDetails(String getmKey) { PhoneDetails
	 * phone=mobileCache.get(getmKey); return
	 * registrationRepositoryImpl.getCustomerCardDetails(phone.getCountryCode(),
	 * phone.getPhoneNum()); }
	 */
}
