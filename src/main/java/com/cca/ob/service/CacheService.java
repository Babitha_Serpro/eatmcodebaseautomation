package com.cca.ob.service;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.cca.ob.constants.OBConstants;
import com.cca.ob.dto.CardDetails;
import com.cca.ob.dto.CustomerInfo;
import com.cca.ob.dto.UserCache;
import com.cca.ob.util.OTPGenerator;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class CacheService {

	private final Map<String,UserCache> userCacheMap=new ConcurrentHashMap<String,UserCache>();
	
	public void storeCustomerInfo(String userKey,CustomerInfo cust) {
		store(userKey, OBConstants.CUSTOMER_INFO, cust);
	}
	public Object getCustomerInfo(String userKey) {
		return get(userKey,OBConstants.CUSTOMER_INFO);
	}
	
	public String storeCard(String userKey,CardDetails card) {
		String ref=OTPGenerator.generate(8);
		Object cardsMap=get(userKey,OBConstants.USER_CARDS);
		if(cardsMap==null) {
			cardsMap=new HashMap<String,CardDetails>();
			store(userKey,OBConstants.USER_CARDS, cardsMap);
		}
		(( HashMap<String,CardDetails>)cardsMap).put(ref, card);
		return ref;
	}
	public CardDetails getCard(String userKey,String cardRef) {
		Object cardsMap=get(userKey,OBConstants.USER_CARDS);
		if(cardsMap!=null) {
			return ((Map<String,CardDetails>)cardsMap).get(cardRef);
		}
		return null;
	}
	
	public void storeUserTokens(String mkey, Long tokenRefNum, Long hiddenTokenKey) {
		Object cardsMap=get(mkey,OBConstants.USER_TOKENS);
		if(cardsMap==null) {
			cardsMap=new HashMap<Long,Long>();
			store(mkey,OBConstants.USER_TOKENS, cardsMap);
		}
		(( HashMap<Long,Long>)cardsMap).put(hiddenTokenKey, tokenRefNum);
		
	}
	public Long getUserToken(String userKey,long hiddenTokenKey) {
		Object cardsMap=get(userKey,OBConstants.USER_CARDS);
		if(cardsMap!=null) {
			return ((Map<Long,Long>)cardsMap).get(hiddenTokenKey);
		}
		return null;
	}
	public void storeAuthKey(String userKey,String authKey) {
		store(userKey, OBConstants.AUTH_KEY, authKey);
	}
	public Object getAuthKey(String userKey) {
		return get(userKey,OBConstants.AUTH_KEY);
	}
	
	
	
	public void storeMerchantRef(String userKey,Map<Integer,String> refMap) {
		store(userKey, OBConstants.MERCHANT_REFERENCE, refMap);
	}
	public String getMerchantRef(String userKey,int merchRefNum) {
		Map<Integer,String> refMap=(Map<Integer,String>)get(userKey, OBConstants.MERCHANT_REFERENCE);
		return refMap==null ? null : refMap.get(merchRefNum);
	}
	
	private void store(String userKey,String key,Object val) {
		UserCache cache=userCacheMap.get(userKey);
		if(cache==null) {
			cache=new UserCache();
			userCacheMap.put(userKey, cache);
		}
		cache.store( key, val);
	}
	private Object get(String userKey,String key) {
		UserCache cache=userCacheMap.get(userKey);
		if (cache==null) {
			return  null ;
		}
		return cache.get(key);
	}
	
	
	
	
}
