package com.cca.ob.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cca.ob.exception.OBException;
import com.cca.ob.repo.SNSCredentialsRepoImpl;
import com.cca.ob.util.EncryptionUtils;
@Service
public class SNSRepoService {
	@Autowired
	private SNSCredentialsRepoImpl SNSCredentialsRepoImpl;
	public void insertAuthData(String id,String pw,String meta)  {
		try {
			SNSCredentialsRepoImpl.createAuth(new EncryptionUtils().encrypt(id),
					new EncryptionUtils().encrypt(pw), meta);
		} catch (Exception e) {
			throw new OBException("Error while creating the Auth Entries",e);
		} 
		
	}
}
		
		
