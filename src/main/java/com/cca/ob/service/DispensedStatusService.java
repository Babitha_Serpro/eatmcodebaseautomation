package com.cca.ob.service;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cca.ob.bc.validators.TokenValidatorsEnum;
import com.cca.ob.constants.BCConstants;
import com.cca.ob.dto.BCDetails;
import com.cca.ob.dto.EncryptedRequestBean;
import com.cca.ob.dto.EncryptedResponseBean;
import com.cca.ob.dto.MerchantDetails;
import com.cca.ob.repo.BCRepository;
import com.cca.ob.repo.TokenValidationRepo;
import com.cca.ob.util.RequestIPUtil;
import com.cca.ob.util.ResponseUtils;

@Service
public class DispensedStatusService {
	private static Logger logger=LogManager.getLogger(DispensedStatusService.class);
	@Autowired
	private BCRepository bcRepository;
	@Autowired
	private TokenValidationRepo tokenValidationRepo;


	@Autowired
	private TokenValidationService tokenValidationService;
	public EncryptedResponseBean updateDispensedStatus(EncryptedRequestBean req, HttpServletRequest request) {
		String remoteIp=RequestIPUtil.getRemoteIP(request);
		logger.info("DispensedStatus Updatation request received from ip"+remoteIp);
		String accessCode=req.getAcc_code();
		BCDetails bcDetails=tokenValidationService.getAggregator(accessCode);
		long dumpID=tokenValidationService.logRequest(req.getParamMap(),remoteIp,accessCode);
		Map<String,String> plainRequest=tokenValidationService.getPlainRequest(req.getParamMap(), accessCode, bcDetails, remoteIp);
		Map<String,String> responseMap=getResponseMap();
		Object merchDetails=vaidateInputParams(plainRequest,responseMap, accessCode, dumpID, bcDetails);
		Long eatmRefNum=getEatmRefNum(responseMap, plainRequest.get(BCConstants.REQUEST_PARAM_ORIGINAL_EATM_REF_NUM));
		if(merchDetails==null || eatmRefNum==null) {
			return getResponseBean(responseMap, bcDetails);
		}
		boolean status=tokenValidationRepo.updateDispensatinoStatus(eatmRefNum.longValue(), plainRequest.get(BCConstants.REQUEST_PARAM_ORIGINAL_BC_REF_NUM), dumpID);
		if(!status) {
			populateErrors("DISP010", "Could not update the DB", responseMap);
			return getResponseBean(responseMap, bcDetails);
		}
		responseMap.put(BCConstants.RESP_PARAM_CONFIRMATION_SUCCESS, "true");
		return getResponseBean(responseMap, bcDetails);
	}

	private Long getEatmRefNum(Map<String,String> responseMap,String eatmRefNum) {
		try {
			return new Long(eatmRefNum);
		}catch(Exception e ) {
			populateErrors("DISP001", "BC Parameters Invalid", responseMap);
			return null;
		}

	}

	private EncryptedResponseBean getResponseBean(Map<String,String> responseMap, BCDetails bcDetails) {
		String respKey= (bcDetails==null ? null :bcDetails.getRespKey());
		responseMap=ResponseUtils.getEncryptedResponse(responseMap, respKey); 
		EncryptedResponseBean bean=new EncryptedResponseBean();
		bean.setEnc_resp(responseMap.get(BCConstants.RESP_PARAM_ENCRYPTED_RESPONSE));
		return bean;
	}

	private Map<String, String> getResponseMap(){
		Map<String,String> responseMap=new HashMap<String, String>();
		responseMap.put(BCConstants.RESP_PARAM_CONFIRMATION_SUCCESS, "false");
		return responseMap;
	}

	private MerchantDetails vaidateInputParams(Map<String, String> plainRequest,Map<String, String> responseMap,String accessCode,Long reqDumpID,BCDetails bcDetails) {
		//Check for invalid encrypted Data

		if(plainRequest==null||plainRequest.isEmpty()) {
			populateErrors("AG113","Invalid Encrypted Data",responseMap);
			return null;
		}
		//store this request as token_Validation_Request
		String []mandatoryParams=getMandatoryParamList();
		boolean missingReqdParams=checkMandatoryParams(plainRequest,responseMap,mandatoryParams);

		if(missingReqdParams) {
			populateErrors("AG101","Missing Required Parameters",responseMap);
			return null;
		}
		if(!plainRequest.get(BCConstants.REQ_PARAM_ACCESS_CODE).equals(accessCode)){
			populateErrors("AG102","Inconsistent AccessCode ",responseMap);
			return null;
		}
		MerchantDetails merchDetails=bcRepository.getMerchant(plainRequest.get(BCConstants.REQ_PARAM_MERCHANT_ID));
		if(merchDetails==null) {
			populateErrors("AG103","Incosistent Details ",responseMap);
			return null;
		}
		if(!merchDetails.isAggregatorEnabled()) {
			populateErrors("AG104","Aggregator Not enabled ",responseMap);
			return null;
		}
		if(bcDetails==null) {
			populateErrors("AG105","Incorrect Access Code",responseMap);
			return null;
		}
		if(!bcDetails.getBcId().equalsIgnoreCase(merchDetails.getBcId())) {
			populateErrors("AG106","Incorrect Access Code.",responseMap);
			return null;
		}
		if(!plainRequest.get(BCConstants.REQ_PARAM_AGGREGATOR_ID).equalsIgnoreCase(merchDetails.getAggregatorId())){
			populateErrors("AG107","Incorrect Access Code.",responseMap);
			return null;
		}
		if(!merchDetails.isEnabled()) {
			populateErrors("AG108"," Merchant not enabled",responseMap);
			return null;
		}

		if(!merchDetails.isBcEnabled()) {
			populateErrors("AG109","BC not enabled",responseMap);
			return null;
		}
		//Loop Fixes	
	       if (containsSpecialCharacter(plainRequest.get(BCConstants.REQ_PARAM_BC_REF_NUM))) {
				
				populateErrors("AG116", "Incorrect BC_REF_NUM.", responseMap);
				return null;
			}

			if (containsSpecialCharacter(plainRequest.get(BCConstants.REQ_PARAM_BC_ID))) {
				
				populateErrors("AG112", "Incorrect BCID.", responseMap);
				return null;
			}  		
		//	
		
		
		
		String reqType=plainRequest.get(BCConstants.REQUEST_PARAM_REQUEST_TYPE);
		if(!("c".equalsIgnoreCase(reqType))){
			populateErrors("AG110","Incorrect Request Type.",responseMap);
			return null;
		}


		String validatorIdentifier=plainRequest.get(BCConstants.REQUEST_PARAM_BANK_IDENTIFIER);
		if(validatorIdentifier==null || TokenValidatorsEnum.getValidator(validatorIdentifier)==null) {
			populateErrors("AG201","Invalid Bank Idnetifier",responseMap);
			return null;
		}

		return merchDetails;
	}

	private void  populateErrors(String errorCd,String errMsg,Map<String,String> respMap) {
		respMap.put(BCConstants.RESP_PARAM_ERROR_CODE,errorCd);
		respMap.put(BCConstants.RESP_PARAM_ERROR_MESSAGE,errMsg);
	}

	private boolean checkMandatoryParams(Map<String, String> plainRequest,Map<String, String> respParam,String[] reqParamName) {
		boolean paramMissing=false;
		String value=null;
		for(String aMandatoryParam:reqParamName) {
			value=plainRequest.get(aMandatoryParam);
			if(BCConstants.REQUEST_PARAM_ORIGINAL_BC_REF_NUM.equalsIgnoreCase(aMandatoryParam) || 
					BCConstants.REQ_PARAM_BC_REF_NUM.equalsIgnoreCase(aMandatoryParam) || 
					BCConstants.REQUEST_PARAM_ORIGINAL_EATM_REF_NUM.equalsIgnoreCase(aMandatoryParam)||
					BCConstants.REQ_PARAM_BC_PARMA1.equalsIgnoreCase(aMandatoryParam) ) {
				respParam.put(aMandatoryParam, value);
			}

			if(value==null) {
				respParam.put(BCConstants.RESP_PARAM_ERROR_CODE,"AG101");
				respParam.put(BCConstants.RESP_PARAM_ERROR_MESSAGE,"Missing Mandatory Request Parameter "+reqParamName);
				paramMissing=true;
			}
		}
		return paramMissing;

	}

	private String[] getMandatoryParamList() {
		String[] mandatoryReqParams= {BCConstants.REQ_PARAM_MERCHANT_ID,BCConstants.REQ_PARAM_AGGREGATOR_ID,BCConstants.REQ_PARAM_BC_ID,
				BCConstants.REQUEST_PARAM_REQUEST_TYPE,
				BCConstants.REQ_PARAM_BC_REF_NUM,
				BCConstants.REQ_PARAM_ACCESS_CODE,
				BCConstants.REQUEST_PARAM_BANK_IDENTIFIER,
				BCConstants.REQUEST_PARAM_ORIGINAL_BC_REF_NUM,
				BCConstants.REQUEST_PARAM_ORIGINAL_EATM_REF_NUM
		};
		return mandatoryReqParams;
	}
	private boolean containsSpecialCharacter(String text) {
		Pattern pattern = Pattern.compile("[^a-zA-Z0-9]");
		Matcher matcher = pattern.matcher(text);
		return matcher.find();
	}
}
