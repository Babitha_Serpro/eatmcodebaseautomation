package com.cca.ob.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cca.ob.bc.controller.TokenValidationController;
import com.cca.ob.bc.response.CallBackResponseHandler;
import com.cca.ob.constants.BCConstants;
import com.cca.ob.constants.OBConstants;
import com.cca.ob.constants.TokenProcessConstants;
import com.cca.ob.dto.BCDetails;
import com.cca.ob.repo.BCRepository;
import com.cca.ob.repo.TokenValidationRepo;
import com.cca.ob.util.HttpExecutor;
import com.ccavenue.security.AesCryptUtil;

@Service
public class TokenValidationService {
	@Autowired
	private TokenValidationRepo tokenValidationRepoImpl;
	@Autowired
	private ProcessValidationService processTokenService;
	@Autowired
	private ProcessReversalService processReversalService;
	@Autowired
	private BCRepository bcRepository;
	@Autowired
	private CallBackResponseHandler responseHandler;

	private static Logger logger=LogManager.getLogger(TokenValidationController.class);

	public Map<String,String> getPlainRequest(Map<String, String> requestParams,String accessCode,BCDetails aggregator, String remoteIp) {
		Map<String,String> plainRequest=new HashMap<String, String>();
		if(accessCode==null) {
			plainRequest=requestParams;
			logger.info("Invalid Request from "+remoteIp+" Missing Access code Call back URL can not be determined. BC Ref :"+requestParams.get(BCConstants.REQ_PARAM_BC_REF_NUM));
		}else if(aggregator==null) {
			plainRequest=requestParams;
			logger.info("Invalid Accesscode from  "+remoteIp+" BC Ref :"+requestParams.get(BCConstants.REQ_PARAM_BC_REF_NUM));
		}else if(requestParams.get(BCConstants.REQ_PARAM_ENCRYPTED)!=null){
			logger.info("Decoding the request from  "+remoteIp+" Aggregator  :"+aggregator);
			plainRequest=decryptRequest(requestParams.get(BCConstants.REQ_PARAM_ENCRYPTED),aggregator.getReqKey());
		}else {
			logger.info("Reading the plain Non Encrypted Requests from  "+remoteIp+" BC Ref :"+requestParams.get(BCConstants.REQ_PARAM_BC_REF_NUM));
			//plainRequest=requestParams;
		}
		return plainRequest;
	}

	public BCDetails getAggregator(String accessCode) {
		return bcRepository.getBCDetails(accessCode);
	}

	public void validateTokens(Map<String, String> plainRequest,String accessCode,BCDetails bc, Long dumpID) {
		Map<String,String> response=processTokenService.getResponse(plainRequest,OBConstants.TRANSACTION_REQUEST,accessCode,dumpID,bc);
		postResponse(bc,response,OBConstants.TRANSACTION_REQUEST);

	}
	public void validateReversal(Map<String, String> plainRequest,String accessCode,BCDetails bc, Long dumpID) {
		Map<String,String> response=processReversalService.getResponse(plainRequest,OBConstants.REVERSAL_REQUEST,accessCode,dumpID,bc);
		postResponse(bc,response,OBConstants.REVERSAL_REQUEST);

	}
	private void postResponse(BCDetails bc,Map<String,String> response,String reqType) {
		if(!response.containsKey(TokenProcessConstants.RESPONSE_AWAITED)) {
			responseHandler.postResponseToCallBackURL(bc, response,reqType);
		}		
	}




	private Map<String, String> decryptRequest(String encryptedRequestParams,String decodeKey) {
		Map<String, String> reqParams=new HashMap<String, String>();
		String decryptedReq= encryptedRequestParams;
		try {
			if(decodeKey!=null) {
				AesCryptUtil aesUtil=new AesCryptUtil(decodeKey);
				decryptedReq= aesUtil.decrypt(encryptedRequestParams);
			}
			String params[]=decryptedReq.split("&");
			if(params==null) {
				return reqParams;
			}
			String keyValue[];
			for(String aReqParam:params) {
				keyValue=aReqParam.split("=");
				if(keyValue!=null && keyValue.length==2) {
					reqParams.put(keyValue[0], keyValue[1]);
				}
			}
		}catch(Exception e) {
			logger.info("Invalid Encrypted data :"+encryptedRequestParams);	
			return reqParams;
		}
		return reqParams;
	}


	public Long logRequest(Map<String, String> requestParams, String remoteIp,String accessCode) {
		return tokenValidationRepoImpl.logRawRequest(requestParams,remoteIp,accessCode);
	}
}
