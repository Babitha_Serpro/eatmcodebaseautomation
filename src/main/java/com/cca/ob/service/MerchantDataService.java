package com.cca.ob.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cca.ob.bc.validators.TokenValidatorsEnum;
import com.cca.ob.constants.BCConstants;
import com.cca.ob.dto.BCDetails;
import com.cca.ob.dto.EncryptedRequestBean;
import com.cca.ob.dto.EncryptedResponseBean;
import com.cca.ob.dto.MerchantDetails;
import com.cca.ob.repo.BCRepository;
import com.cca.ob.repo.TokenValidationRepo;
import com.cca.ob.util.DecryptionUtil;
import com.cca.ob.util.RequestIPUtil;
import com.cca.ob.util.ResponseUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class MerchantDataService {
	private static Logger logger=LogManager.getLogger(MerchantDataService.class);
	@Autowired
	private BCRepository bcRepository;

	public Map<String,String> insertMerchantDetails(EncryptedRequestBean encReq, HttpServletRequest request) {
		String remoteIp=RequestIPUtil.getRemoteIP(request);
		logger.info("Merchant Data Updatation request received from ip"+remoteIp);
		BCDetails bcDetails=bcRepository.getBCDtls(encReq.getAcc_code());
		Map<String,String> plainRequest=getPlainRequest(encReq,bcDetails,remoteIp);
		Map<String,String> responseMap=getResponseMap();
		boolean validateFlag=vaidateInputParams(plainRequest,responseMap,bcDetails,false);
		if(validateFlag) {
			int status=bcRepository.insertMerchantDetails(plainRequest.get(BCConstants.REQUEST_PARAM_AGENT_ID), plainRequest.get(BCConstants.REQUEST_PARAM_MERCHANT_NAME), plainRequest.get(BCConstants.REQUEST_PARAM_SHOP_NAME), plainRequest.get(BCConstants.REQUEST_PARAM_MERCHANT_CATEGORY_CODE), plainRequest.get(BCConstants.REQUEST_PARAM_MOBILE_1), plainRequest.get(BCConstants.REQUEST_PARAM_MOBILE_2), plainRequest.get(BCConstants.REQUEST_PARAM_LANDLINE), plainRequest.get(BCConstants.REQUEST_PARAM_EMAIL_ID), plainRequest.get(BCConstants.REQUEST_PARAM_ADDRESS), plainRequest.get(BCConstants.REQUEST_PARAM_CITY), plainRequest.get(BCConstants.REQUEST_PARAM_STATE), plainRequest.get(BCConstants.REQUEST_PARAM_PINCODE), plainRequest.get(BCConstants.REQUEST_PARAM_LANDMARK), plainRequest.get(BCConstants.REQUEST_PARAM_GEO_CODE), plainRequest.get(BCConstants.REQUEST_PARAM_AI_ID), plainRequest.get(BCConstants.REQUEST_PARAM_COUNTRY), plainRequest.get(BCConstants.REQUEST_PARAM_WEEKLY_OFF_DAYS), plainRequest.get(BCConstants.REQUEST_PARAM_WORKING_HOURS), plainRequest.get(BCConstants.REQUEST_PARAM_DAILY_BREAK));
			if(status==0) {
				populateErrors("MD010", "Could not update the DB", responseMap);
				return responseMap;
			}else if(status == 2) {
				populateErrors("MD011", plainRequest.get(BCConstants.REQUEST_PARAM_AGENT_ID)+" already exists.", responseMap);
				return responseMap;
			}
		}else {
			return responseMap;
		}
		responseMap.put(BCConstants.RESP_PARAM_CONFIRMATION_SUCCESS, "true");
		return responseMap;
	}

	private Map<String, String> getPlainRequest(EncryptedRequestBean encReq, BCDetails bcDetails, String remoteIp) {
		Map<String,String> plainRequest=new HashMap<String, String>();
		if(encReq.getAcc_code()==null) {
			logger.info("Invalid Request from "+remoteIp+" Missing Access code.");
		}else if(bcDetails==null) {
			logger.info("Invalid Accesscode from  "+remoteIp);
		}else if(encReq.getEnc_req()!=null){
			logger.info("Decoding the request from  "+remoteIp+" Aggregator  :"+bcDetails);
			String originalString = encReq.getEnc_req();
			logger.info("Secret Key:"+bcDetails.getSecretKey());
			originalString=DecryptionUtil.decrypt(originalString,bcDetails.getSecretKey());
			logger.info("List of Merchant Data: "+originalString);

			ObjectMapper mapper = new ObjectMapper();
			try {
				plainRequest = mapper.readValue(originalString, Map.class);
	            return plainRequest;
	        } catch (Exception e) {
				logger.error("Exception in getPlainRequest",e);
	        }	
		}else {
			logger.info("Reading the plain Non Encrypted Requests from  "+remoteIp);
		}
		
		return null;
	}


	private Map<String, String> getResponseMap(){
		Map<String,String> responseMap=new HashMap<String, String>();
		responseMap.put(BCConstants.RESP_PARAM_CONFIRMATION_SUCCESS, "false");
		return responseMap;
	}

	private boolean vaidateInputParams(Map<String, String> plainRequest,Map<String, String> responseMap, BCDetails bcDetails, boolean update) {
		//Check for invalid encrypted Data

		if(plainRequest==null||plainRequest.isEmpty()) {
			populateErrors("MD113","Invalid Encrypted Data",responseMap);
			return false;
		}
		String []mandatoryParams=getMandatoryParamList();
		boolean missingReqdParams=checkMandatoryParams(plainRequest,responseMap,mandatoryParams);
		if(missingReqdParams) {
			populateErrors("MD101","Missing Required Parameters",responseMap);
			return false;
		}
		if(update && plainRequest.get(BCConstants.REQUEST_PARAM_IS_ENABLED)==null) {
			populateErrors("MD103","Missing IS Enabled Parameters",responseMap);
			return false;
		}
		if(!bcDetails.getBcId().equals(plainRequest.get(BCConstants.REQ_PARAM_BC_ID))) {
			populateErrors("MD102","Invalid BC_ID",responseMap);
			return false;
		}
		return true;
	}

	private void  populateErrors(String errorCd,String errMsg,Map<String,String> respMap) {
		respMap.put(BCConstants.RESP_PARAM_ERROR_CODE,errorCd);
		respMap.put(BCConstants.RESP_PARAM_ERROR_MESSAGE,errMsg);

	}

	private boolean checkMandatoryParams(Map<String, String> plainRequest,Map<String, String> respParam,String[] reqParamName) {
		boolean paramMissing=false;
		String value=null;
		for(String aMandatoryParam:reqParamName) {
			value=plainRequest.get(aMandatoryParam);
			if(value==null) {
				respParam.put(BCConstants.RESP_PARAM_ERROR_CODE,"AG101");
				respParam.put(BCConstants.RESP_PARAM_ERROR_MESSAGE,"Missing Mandatory Request Parameter "+reqParamName);
				paramMissing=true;
			}
		}
		return paramMissing;

	}

	private String[] getMandatoryParamList() {
		String[] mandatoryReqParams= {BCConstants.REQUEST_PARAM_AGENT_ID,BCConstants.REQUEST_PARAM_MERCHANT_NAME,BCConstants.REQUEST_PARAM_SHOP_NAME,
				BCConstants.REQUEST_PARAM_MERCHANT_CATEGORY_CODE,
				BCConstants.REQUEST_PARAM_MOBILE_1,
				BCConstants.REQUEST_PARAM_EMAIL_ID,
				BCConstants.REQUEST_PARAM_ADDRESS,
				BCConstants.REQUEST_PARAM_CITY,
				BCConstants.REQUEST_PARAM_STATE,
				BCConstants.REQUEST_PARAM_PINCODE,
				BCConstants.REQUEST_PARAM_LANDMARK,
				BCConstants.REQUEST_PARAM_GEO_CODE,
				BCConstants.REQUEST_PARAM_AI_ID,
				BCConstants.REQUEST_PARAM_COUNTRY,
				BCConstants.REQUEST_PARAM_WORKING_HOURS,
				BCConstants.REQUEST_PARAM_DAILY_BREAK,
				BCConstants.REQ_PARAM_BC_ID
		};
		return mandatoryReqParams;
	}

	public Map<String, String> updateMerchantDetails(EncryptedRequestBean encReq, HttpServletRequest request) {
		String remoteIp=RequestIPUtil.getRemoteIP(request);
		logger.info("Merchant Data Updatation request received from ip"+remoteIp);
		BCDetails bcDetails=bcRepository.getBCDtls(encReq.getAcc_code());
		Map<String,String> plainRequest=getPlainRequest(encReq,bcDetails,remoteIp);
		Map<String,String> responseMap=getResponseMap();
		boolean validateFlag=vaidateInputParams(plainRequest,responseMap,bcDetails,true);
		if(validateFlag) {
			boolean status=bcRepository.updateMerchantDetails(plainRequest.get(BCConstants.REQUEST_PARAM_AGENT_ID), plainRequest.get(BCConstants.REQUEST_PARAM_MERCHANT_NAME), plainRequest.get(BCConstants.REQUEST_PARAM_SHOP_NAME), plainRequest.get(BCConstants.REQUEST_PARAM_MERCHANT_CATEGORY_CODE), plainRequest.get(BCConstants.REQUEST_PARAM_MOBILE_1), plainRequest.get(BCConstants.REQUEST_PARAM_MOBILE_2), plainRequest.get(BCConstants.REQUEST_PARAM_LANDLINE), plainRequest.get(BCConstants.REQUEST_PARAM_EMAIL_ID), plainRequest.get(BCConstants.REQUEST_PARAM_ADDRESS), plainRequest.get(BCConstants.REQUEST_PARAM_CITY), plainRequest.get(BCConstants.REQUEST_PARAM_STATE), plainRequest.get(BCConstants.REQUEST_PARAM_PINCODE), plainRequest.get(BCConstants.REQUEST_PARAM_LANDMARK), plainRequest.get(BCConstants.REQUEST_PARAM_GEO_CODE), plainRequest.get(BCConstants.REQUEST_PARAM_AI_ID), plainRequest.get(BCConstants.REQUEST_PARAM_COUNTRY), plainRequest.get(BCConstants.REQUEST_PARAM_WEEKLY_OFF_DAYS), plainRequest.get(BCConstants.REQUEST_PARAM_WORKING_HOURS), plainRequest.get(BCConstants.REQUEST_PARAM_DAILY_BREAK), plainRequest.get(BCConstants.REQUEST_PARAM_IS_ENABLED));
			if(!status) {
				populateErrors("MD010", "Could not update the DB", responseMap);
				return responseMap;
			}
		}else {
			return responseMap;
		}
		responseMap.put(BCConstants.RESP_PARAM_CONFIRMATION_SUCCESS, "true");
		return responseMap;		
	}

}
