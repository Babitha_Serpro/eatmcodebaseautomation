package com.cca.ob.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cca.ob.dto.MerchantDetails;
import com.cca.ob.dto.MerchantDetailsMini;
import com.cca.ob.repo.BCRepository;

@Service
public class GeoSpatialService {
	@Value("${merch.geo.radius}")
	private float radiusKm;
	@Value("${merch.nearby.maxlimit}")
	private int maxMerchantRecords;
	@Autowired
	private BCRepository geoSpatialRepositoryImpl;
	@Autowired
	private CacheService cacheService;
	public List<MerchantDetailsMini> getMerchants(String mkey,float currLat, float currLong) {
		List<MerchantDetailsMini> merchList= geoSpatialRepositoryImpl.getMerchants(currLat, currLong, radiusKm,maxMerchantRecords);
		Map<Integer,String> merchRefMap=new HashMap<Integer,String>();
		for(int i=0;i<merchList.size();i++) {
			merchRefMap.put(i, merchList.get(i).getMerchId());
			merchList.get(i).setMerchId(String.valueOf(i));
		}
		cacheService.storeMerchantRef(mkey, merchRefMap);
		return merchList;
	}
	
	public List<MerchantDetailsMini> getMerchants(float currLat, float currLong,Integer maxMerchants) {
		int maxRecords=(maxMerchants==null? maxMerchantRecords :maxMerchants.intValue());
		List<MerchantDetailsMini> merchList= geoSpatialRepositoryImpl.getMerchants(currLat, currLong, radiusKm,maxRecords);
		for(int i=0;i<merchList.size();i++) {
			merchList.get(i).setMerchId(String.valueOf(i));
		}
		return merchList;
	}
}
