package com.cca.ob.service;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.cca.ob.dto.SNSAuthBean;
import com.cca.ob.exception.OBException;
import com.cca.ob.repo.SNSCredentialsRepoImpl;
import com.cca.ob.util.EncryptionUtils;

@Service
public class SNSService implements TextSMSService {

	@Autowired
	private SNSCredentialsRepoImpl SNSCredentialsRepoImpl;
	
	@Value("${sns.auth.identifier}")
	private String authIdentifier;
	
	private AmazonSNSClient getSNSClient() throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException {
		BasicAWSCredentials basicAwsCredentials = getBasicCredentials(authIdentifier);
		AmazonSNS snsClient = AmazonSNSClient.builder().withRegion(Regions.US_EAST_1)
				.withCredentials(new AWSStaticCredentialsProvider(basicAwsCredentials)).build();
		return (AmazonSNSClient) snsClient;
	}

	private PublishResult sendSMSMessage(AmazonSNSClient snsClient, String message, String phoneNumber,
			Map<String, MessageAttributeValue> smsAttributes) {
		return snsClient.publish(new PublishRequest().withMessage(message).withPhoneNumber(phoneNumber)
				.withMessageAttributes(smsAttributes));

	}
	
	private BasicAWSCredentials getBasicCredentials(String identifier) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException {
		SNSAuthBean auth=SNSCredentialsRepoImpl.readAuth(identifier);
		String eid=new EncryptionUtils().decrypt(auth.getAuthId());
		String epw=new EncryptionUtils().decrypt(auth.getPassword());
		
		eid="AKIA5ABPV4FRBXRO3JPM";
		epw="z47Ce2UqHmBhO4L2aQVOW6agNmPSAN16gwb/QG8y";
		return new BasicAWSCredentials(eid,epw);
	}

	public String sendSMSMessage(String qualifiedPhoneNum, String message,String senderId) {
		Map<String, MessageAttributeValue> smsAttributes = new HashMap<String, MessageAttributeValue>();
		AmazonSNSClient snsClient;
		try {
			snsClient = getSNSClient();
		} catch (Exception e) {
			throw new OBException("Could not get the AWS credentials for SNS",e);
		} 

		// <set SMS attributes>

		// set the SenderId to the from info
		
		  smsAttributes.put("AWS.SNS.SMS.SenderID", new MessageAttributeValue()
		  .withStringValue(senderId).withDataType("String"));
		  
		  // these SMS messages are important to transactional
		  
		  smsAttributes.put("AWS.SNS.SMS.SMSType", new MessageAttributeValue()
		  .withStringValue("Transactional").withDataType("String"));
		 
		return sendSMSMessage(snsClient, message, qualifiedPhoneNum, smsAttributes).getMessageId();
	}

	
}
