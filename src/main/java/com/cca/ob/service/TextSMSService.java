package com.cca.ob.service;

public interface TextSMSService {
	public String sendSMSMessage(String qualifiedPhoneNum,String message,String custName) ;
}
