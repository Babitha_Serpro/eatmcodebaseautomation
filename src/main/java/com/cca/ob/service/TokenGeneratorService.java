package com.cca.ob.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cca.ob.repo.WithDrawlRepository;
import com.cca.ob.util.NumberGenerator;

@Service
public class TokenGeneratorService {
	private Logger logger=LogManager.getLogger(TokenGeneratorService.class);
	@Autowired
	private WithDrawlRepository withDrawlRepositoryImpl;
	public long generateToken(long obTransId) {
		//long tokenRefNum=NumberGenerator.generate(6);
		List<Long> tokens=new ArrayList<Long>();
		for(int i=0;i<3;i++) {
			tokens.add(NumberGenerator.generate(6));
		}
		try {
			return withDrawlRepositoryImpl.getToken(obTransId,tokens);
			//return tokenRefNum;
		}catch(Exception e) {
			logger.error("Error while generating token",e);
		}
		 return -1;
	}

}
