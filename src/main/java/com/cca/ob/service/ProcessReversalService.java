package com.cca.ob.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cca.ob.bc.validators.BCRequestParamKeyConstants;
import com.cca.ob.bc.validators.TokenReversalFactory;
import com.cca.ob.bc.validators.Validator;
import com.cca.ob.constants.BCConstants;
import com.cca.ob.repo.TokenValidationRepo;
import com.cca.ob.util.StringUtils;
@Service
public class ProcessReversalService extends AbstractProcessTokenService{
	@Autowired
	private TokenValidationRepo tokenValidationRepo;
	
	
	@Autowired
	private TokenReversalFactory tokenReversalFactory;

	@Value("${daily.transaction.limit.max}")
	private int maxTransactionLimitPerDay;

	@Override
	protected  Map<String,String> getDefaultResponseMap(Map<String,String> plainRequest,Long obReqId) {
		Map<String,String> responseMap=new HashMap<String, String>();
		responseMap.put(BCConstants.RESP_PARAM_EATM_REF_NUM,obReqId.toString() );
		responseMap.put(BCConstants.REQ_PARAM_BC_REF_NUM,plainRequest.get(BCConstants.REQ_PARAM_BC_REF_NUM) );
		responseMap.put(BCConstants.REQ_PARAM_BC_PARMA1,plainRequest.get(BCConstants.REQ_PARAM_BC_PARMA1) );
		responseMap.put(BCConstants.REQ_PARAM_MERCHANT_ID,plainRequest.get(BCConstants.REQ_PARAM_MERCHANT_ID) );
		responseMap.put(BCConstants.RESP_PARAM_REVERSAL_SUCCESS, "false");	
		responseMap.put(BCConstants.RESP_PARAM_AMOUNT, "-0.0");	
		return responseMap;
	}

	@Override
	protected Validator getValidator(String bankIdentifier) {
		return tokenReversalFactory.getReversalValidator(bankIdentifier);
	}

	@Override
	protected  boolean isReversal() {
		return true;
	}
	@Override
	protected String[] getMandatoryParam() {
		String[] mandatoryReqParams= {BCConstants.REQ_PARAM_MERCHANT_ID,BCConstants.REQ_PARAM_AGGREGATOR_ID,BCConstants.REQ_PARAM_BC_ID,
									  BCConstants.REQUEST_PARAM_REQUEST_TYPE,
									  BCConstants.REQ_PARAM_BC_REF_NUM,
									  BCConstants.REQ_PARAM_ACCESS_CODE,
									  BCConstants.REQUEST_PARAM_BANK_IDENTIFIER,
									  BCConstants.REQUEST_PARAM_ORIGINAL_BC_REF_NUM,
									  BCConstants.REQUEST_PARAM_ORIGINAL_REQUEST_TIMESTAMP,
									  BCConstants.REQUEST_PARAM_REV_REASON_CD
									};
		return mandatoryReqParams;
	}
	
	@Override
	protected long createObRequestId(Long reqDumpID,Map<String,String> plainRequest) {
		String reqTokenNum=plainRequest.get(BCConstants.REQ_PARAM_TOKEN_NUM);
		Integer numericTokenNum=null;
		if(StringUtils.isNumeric(reqTokenNum)) {
			numericTokenNum=Integer.parseInt(reqTokenNum);
		}
		//Common for eatm and other bank validation. All the params are nullable.
		String encryptedMobileNumber=getUserMobileEncrypted(plainRequest);
		Double[] latLong=getMerchantLatLong(plainRequest);
		return tokenValidationRepo.transactionReversalRequested(reqDumpID,
				plainRequest.get(BCConstants.REQ_PARAM_AGGREGATOR_ID),
				plainRequest.get(BCConstants.REQ_PARAM_MERCHANT_ID), plainRequest.get(BCConstants.REQ_PARAM_BC_ID),
				numericTokenNum, plainRequest.get(BCConstants.REQ_PARAM_BC_REF_NUM),
				plainRequest.get(BCConstants.REQUEST_PARAM_ORIGINAL_BC_REF_NUM),
				plainRequest.get(BCConstants.REQUEST_PARAM_ORIGINAL_EATM_REF_NUM),
				plainRequest.get(BCConstants.REQUEST_PARAM_ORIGINAL_REQUEST_TIMESTAMP), getRequestType(),
				plainRequest.get(BCConstants.REQUEST_PARAM_BANK_IDENTIFIER), encryptedMobileNumber, latLong[0],
				latLong[1], plainRequest.get(BCConstants.REQUEST_PARAM_REV_REASON_CD),
				plainRequest.get(BCConstants.REQUEST_PARAM_REV_REASON),
				plainRequest.get(BCConstants.REQUEST_PARAM_USER_MOBILE_NUM),
				plainRequest.get(BCRequestParamKeyConstants.ICICI_4DIGIT_PIN),
				plainRequest.get(BCRequestParamKeyConstants.ICICI_6DIGIT_PIN));
	
	}

	@Override
	protected boolean checkMaxmimumTransactionPerDayReached(Map<String, String> plainRequest) {
		String encryptedMobileNum = getUserMobileEncrypted(plainRequest);
		int transactionCount = tokenValidationRepo.fetchTransactionsCount(encryptedMobileNum);
		if (transactionCount >= maxTransactionLimitPerDay) {
			return true;
		}
		return false;
	}
	@Override
	protected boolean checkUniqueBCREFNO(Map<String, String> plainRequest) {
		int bcRefNoCount = tokenValidationRepo.fetchBCREFCount(plainRequest.get(BCConstants.REQ_PARAM_BC_REF_NUM));
		if (bcRefNoCount == 1) {
			return true;
		}
		return false;
	}
	@Override
	protected boolean checkOriginalBCREFNO(Map<String, String> plainRequest) {
		String originalbcrefnum = tokenValidationRepo.fetchOriginalBCREFNO(plainRequest.get(BCConstants.REQUEST_PARAM_ORIGINAL_EATM_REF_NUM));
		if (originalbcrefnum.equals(plainRequest.get(BCConstants.REQUEST_PARAM_ORIGINAL_BC_REF_NUM))) {
			return true;
		}
		return false;
	}

	@Override
	protected boolean checkInvalidOriginalBCREFNO(Map<String, String> plainRequest) {	
		int reqId = tokenValidationRepo.fetchBCREFNO(plainRequest.get(BCConstants.REQUEST_PARAM_ORIGINAL_BC_REF_NUM));	
		if(reqId==0) {			
			return true;
		}else if(reqId!=0) {
			int data = tokenValidationRepo.checkData(reqId);
			if(data==0) {
				return true;
			}else if(data!=0) {
				return true;
			}
		}
		
		return false;
	}
	
}
