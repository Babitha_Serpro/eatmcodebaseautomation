package com.cca.ob.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cca.ob.dto.TransactionDetails;
import com.cca.ob.enums.GatewayStatusMessage;
import com.cca.ob.repo.GatewayRepo;
import com.cca.ob.util.NumberGenerator;

@Service
public class OrderStatusService {
	@Autowired
	private GatewayRepo gatewayRepoImpl;
	@Autowired
	private CacheService cacheService;
	public TransactionDetails getOrderStatus(long obTransactionID) {
		/*
		 * TransactionDetails transDetails=
		 * gatewayRepoImpl.getTrasactionDetails(obTransactionID); if(transDetails!=null
		 * && transDetails.getOrderStatus()!=null
		 * &&"success".equalsIgnoreCase(GatewayStatusMessage.getStatusMessage(
		 * transDetails.getOrderStatus()).getStatus()) &&
		 * transDetails.getTokenRefNum()!=null) { long
		 * tokenHiddenKey=NumberGenerator.generate(6);
		 * cacheService.storeUserTokens(mkey,transDetails.getTokenRefNum(),Long.valueOf(
		 * tokenHiddenKey));
		 * 
		 * } return transDetails;
		 */
		return gatewayRepoImpl.getTrasactionDetails(obTransactionID);
	}
	
	public long generateHiddenTokenIndex(String mkey,Long tokenNumber) {
		long tokenHiddenKey=NumberGenerator.generate(6);
		cacheService.storeUserTokens(mkey,tokenNumber,Long.valueOf(
				  tokenHiddenKey));
		return tokenHiddenKey;
	}
}
