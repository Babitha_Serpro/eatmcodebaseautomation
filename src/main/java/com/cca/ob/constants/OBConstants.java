package com.cca.ob.constants;

public class OBConstants {
	public static final String AUTH_KEY="AUTH_KEY";
	public static final String MERCHANT_REFERENCE="MERCHANT_REFERENCE";
	public static final String CUSTOMER_INFO = "CUSTOMER_INFO";
	public static final String USER_CARDS = "USER_CARDS";
	public static final String USER_TOKENS = "USER_TOKENS";
	
	public static final String UTF_8="UTF-8";
	public static final String REDIRECT_URL = "REDIRECT_URL";
	public static final String CANCEL_URL = "CANCEL_URL";
	public static final Object ENCODED_RESPONSE = "encResp";
	public static final String YYYY_PREFIX = "20";
	
	public static final String TRANSACTION_REQUEST = "T";
	public static final String REVERSAL_REQUEST = "R";
	
}
