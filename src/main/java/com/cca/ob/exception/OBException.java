package com.cca.ob.exception;

public class OBException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public OBException(String msg){
		super(msg);
	}
	public OBException(String msg,Throwable cause){
		super(msg,cause);
	}
}
