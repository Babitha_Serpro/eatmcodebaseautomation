package com.cca.ob.secure;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import com.cca.ob.constants.OBConstants;

public class SecretKeyUtil {
	private static final String defaultEncryptionKeyString =  "pT*DV3060+r18%)#";
	private byte[] salt = new byte[16];
	private  String encryptionKeyString;
	public SecretKeyUtil() {
		this(defaultEncryptionKeyString);
	}
	public SecretKeyUtil(String secretKey) {
		SecureRandom random = new SecureRandom();
		random.nextBytes(salt);
		this.encryptionKeyString=secretKey;
	}
	public SecretKeyUtil(byte[] salt) {
		this(defaultEncryptionKeyString,salt);
	}
	public SecretKeyUtil(String secretKey,byte[] salt) {
		this.salt=salt;
		this.encryptionKeyString=secretKey;
	}
	public  SecretKey getSecretKey() throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeySpecException {
		KeySpec spec = new PBEKeySpec(encryptionKeyString.toCharArray(), salt, 65536, 256); // AES-256
		SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		byte[] key = f.generateSecret(spec).getEncoded();
		SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
		return keySpec;
	}
	
	public byte[] getSalt() {
		return salt;
	}
	
}
