package com.cca.ob.secure.ic;

import java.security.PrivateKey;
import java.security.PublicKey;

import com.cca.ob.secure.RSAPKCS1CryptoUtil;

public class ICICICryptoUtil {
	public static String encryptPayload(String payload) throws Exception {
		RSAPKCS1CryptoUtil util=new RSAPKCS1CryptoUtil();
		PublicKey publicKey = util.getPublicFromCertificate("iciciCert.cer");
		return util.encryptText(payload, publicKey);
	}
	
	public static String decryptResponse(String encryptedResp) throws Exception {
		RSAPKCS1CryptoUtil util=new RSAPKCS1CryptoUtil();
		PrivateKey pk=util.getPrivate("eatmPrivateKey.key");
		return util.decryptText(encryptedResp, pk);
	}
	public static String decryptStream(byte[] encryptedResp) throws Exception {
		RSAPKCS1CryptoUtil util=new RSAPKCS1CryptoUtil();
		PrivateKey pk=util.getPrivate("eatmPrivateKey.key");
		return util.decryptStream(encryptedResp, pk);
	}
	
	
}
