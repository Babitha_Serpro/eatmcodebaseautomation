package com.cca.ob.secure.cert;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.PublicKey;
import java.security.cert.CertificateFactory;

public class PublicKeyExtractor {
	public static PublicKey getPublicKey(File certFile) throws Exception {
		FileInputStream fin = new FileInputStream(certFile);
		return getPublicKey(fin);
	}
	public static PublicKey getPublicKey(InputStream stream) throws Exception {
		//FileInputStream fin = new FileInputStream(certFile);
		CertificateFactory f = CertificateFactory.getInstance("X.509");
		java.security.cert.Certificate certificate = f.generateCertificate(stream);
		return certificate.getPublicKey();
	}
}
