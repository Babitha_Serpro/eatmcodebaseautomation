package com.cca.ob.secure;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import com.cca.ob.constants.OBConstants;


public class KeyUtil {
	private static SecretKey secretKey=null;
	private static ReadWriteLock lock=new ReentrantReadWriteLock();
	
	public static SecretKey getSecretKey() throws NoSuchAlgorithmException, UnsupportedEncodingException {
		if(secretKey==null) {
			lock.writeLock().lock();
			try {
				if(secretKey==null) {
					String encryptionKeyString =  "Gh9NRU1c1AGZasHZ";
					secretKey = new SecretKeySpec(encryptionKeyString.getBytes(OBConstants.UTF_8), "AES");
				}
				
			}finally {
				lock.writeLock().unlock();
			}
			
		}
		return secretKey;
	}
	
	
}
