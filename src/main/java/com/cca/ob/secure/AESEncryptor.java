package com.cca.ob.secure;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;
import java.util.Base64.Encoder;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import com.cca.ob.constants.OBConstants;

public class AESEncryptor {

	public String encrypt(String plainText) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, InvalidKeySpecException {
		SecretKeyUtil keyUtl=new SecretKeyUtil();
		return encrypt(keyUtl.getSecretKey(), keyUtl.getSalt(), plainText);
	}
	public String encrypt(String plainText,String secretKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, InvalidKeySpecException {
		SecretKeyUtil keyUtl=new SecretKeyUtil(secretKey);
		return encrypt(keyUtl.getSecretKey(), keyUtl.getSalt(), plainText);
	}
	
	private  String encrypt(SecretKey keySpec,byte[] salt,String plainText) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		SecureRandom random = new SecureRandom();
		byte[] ivBytes = new byte[16];
		random.nextBytes(ivBytes);
		IvParameterSpec iv = new IvParameterSpec(ivBytes);

		Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
		c.init(Cipher.ENCRYPT_MODE, keySpec, iv);
		byte[] encValue = c.doFinal(plainText.getBytes());

		byte[] finalCiphertext = new byte[encValue.length+2*16];
		System.arraycopy(ivBytes, 0, finalCiphertext, 0, 16);
		System.arraycopy(salt, 0, finalCiphertext, 16, 16);
		System.arraycopy(encValue, 0, finalCiphertext, 32, encValue.length);

		Encoder encoder=Base64.getEncoder();
        return encoder.encodeToString(finalCiphertext);
		//return ;
	}
	public  String decrypt(String encryptedText) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, InvalidKeySpecException {
		return decrypt(encryptedText, null);
	}
	public  String decrypt(String encryptedText,String secretKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, InvalidKeySpecException {
		byte[] byteValue= Base64.getDecoder().decode(encryptedText);
		byte[] ivBytes = new byte[16];
		byte[] salt = new byte[16];
		byte[] actualEncryptedBytes=new byte[byteValue.length-(2*16)];
		
		System.arraycopy(byteValue, 0, ivBytes, 0, 16);
		System.arraycopy(byteValue, 16, salt, 0, 16);
		System.arraycopy(byteValue, 32, actualEncryptedBytes, 0, byteValue.length-32);
		
		SecretKeyUtil keyUtl=(secretKey==null ? new SecretKeyUtil(salt) : new SecretKeyUtil(secretKey,salt));
		return decrypt(keyUtl.getSecretKey(), salt,ivBytes, actualEncryptedBytes);
	}
	
	public  String decrypt(SecretKey keySpec,byte[] salt,byte[] ivbytes,byte[] actualEncryptedBytes) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		IvParameterSpec iv = new IvParameterSpec(ivbytes);

		Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
		c.init(Cipher.DECRYPT_MODE, keySpec, iv);
		byte[] result = c.doFinal(actualEncryptedBytes);

		//byte[] finalCiphertext = new byte[encValue.length+2*16];
		//System.arraycopy(ivBytes, 0, finalCiphertext, 0, 16);
		//System.arraycopy(salt, 0, finalCiphertext, 16, 16);
		//System.arraycopy(encValue, 0, finalCiphertext, 32, encValue.length);

		//System.out.println("finalCiphertext "+plainValue);
		return  new String(result,OBConstants.UTF_8);
	}
}
