package com.cca.ob.secure;

//--------------------------------------------------------------------------
/**
* This is an implementation of the Data Encryption Standard (DES) which is a
* block encryption scheme operating on 64bit blocks and 56bit keys. All four of
* the standard DES modes are implemented and so EBC, CBC, CFB and OFB
* encryption and decryption is possible. There are several caveats you need to
* keep in mind if you want to use this class :
* <UL>
* <LI>This isn't designed to be a fast implementation -- it is strictly
* written with understanding in mind. Therefore, where there is a slow <CODE>perm</CODE>
* or <CODE>rotl</CODE> call you need to keep in mind that a good deal of
* optimisation has been chopped out to expose the algorithm.
* <LI>You need to feed the functions with data blocks of 64bits in size. The
* class doesn't deal with short final blocks or the insertion of markers to
* tell how long the final block it. This feature is DIY.
* </UL>
* 
* @author <A HREF="http://www.phoo.org">Dan Page</A>
* @version $Id$
*/
//--------------------------------------------------------------------------
public class TrippleDESEncryotor {
	// {{{ members
	// --------------------------------
	// Define private member variables.
	// --------------------------------
	private long CBCBuffer = 0;

	private long CFBBuffer = 0;

	private long OFBBuffer = 0;

	// ----------------------------------
	// Define protected member variables.
	// ----------------------------------

	// -------------------------------
	// Define public member variables.
	// -------------------------------
	public static final int ERROR = -1;

	public static final int ENCRYPT = 0;

	public static final int DECRYPT = 1;

	public static final int ip[] = { 0x0039, 0x0031, 0x0029, 0x0021, 0x0019,
			0x0011, 0x0009, 0x0001, 0x003B, 0x0033, 0x002B, 0x0023, 0x001B,
			0x0013, 0x000B, 0x0003, 0x003D, 0x0035, 0x002D, 0x0025, 0x001D,
			0x0015, 0x000D, 0x0005, 0x003F, 0x0037, 0x002F, 0x0027, 0x001F,
			0x0017, 0x000F, 0x0007, 0x0038, 0x0030, 0x0028, 0x0020, 0x0018,
			0x0010, 0x0008, 0x0000, 0x003A, 0x0032, 0x002A, 0x0022, 0x001A,
			0x0012, 0x000A, 0x0002, 0x003C, 0x0034, 0x002C, 0x0024, 0x001C,
			0x0014, 0x000C, 0x0004, 0x003E, 0x0036, 0x002E, 0x0026, 0x001E,
			0x0016, 0x000E, 0x0006 };

	public static final int fp[] = { 0x0027, 0x0007, 0x002F, 0x000F, 0x0037,
			0x0017, 0x003F, 0x001F, 0x0026, 0x0006, 0x002E, 0x000E, 0x0036,
			0x0016, 0x003E, 0x001E, 0x0025, 0x0005, 0x002D, 0x000D, 0x0035,
			0x0015, 0x003D, 0x001D, 0x0024, 0x0004, 0x002C, 0x000C, 0x0034,
			0x0014, 0x003C, 0x001C, 0x0023, 0x0003, 0x002B, 0x000B, 0x0033,
			0x0013, 0x003B, 0x001B, 0x0022, 0x0002, 0x002A, 0x000A, 0x0032,
			0x0012, 0x003A, 0x001A, 0x0021, 0x0001, 0x0029, 0x0009, 0x0031,
			0x0011, 0x0039, 0x0019, 0x0020, 0x0000, 0x0028, 0x0008, 0x0030,
			0x0010, 0x0038, 0x0018 };

	public static final int ep[] = { 0x001F, 0x0000, 0x0001, 0x0002, 0x0003,
			0x0004, 0x0003, 0x0004, 0x0005, 0x0006, 0x0007, 0x0008, 0x0007,
			0x0008, 0x0009, 0x000A, 0x000B, 0x000C, 0x000B, 0x000C, 0x000D,
			0x000E, 0x000F, 0x0010, 0x000F, 0x0010, 0x0011, 0x0012, 0x0013,
			0x0014, 0x0013, 0x0014, 0x0015, 0x0016, 0x0017, 0x0018, 0x0017,
			0x0018, 0x0019, 0x001A, 0x001B, 0x001C, 0x001B, 0x001C, 0x001D,
			0x001E, 0x001F, 0x0000 };

	public static final int kp[] = { 0x0038, 0x0030, 0x0028, 0x0020, 0x0018,
			0x0010, 0x0008, 0x0000, 0x0039, 0x0031, 0x0029, 0x0021, 0x0019,
			0x0011, 0x0009, 0x0001, 0x003A, 0x0032, 0x002A, 0x0022, 0x001A,
			0x0012, 0x000A, 0x0002, 0x003B, 0x0033, 0x002B, 0x0023, 0x003E,
			0x0036, 0x002E, 0x0026, 0x001E, 0x0016, 0x000E, 0x0006, 0x003D,
			0x0035, 0x002D, 0x0025, 0x001D, 0x0015, 0x000D, 0x0005, 0x003C,
			0x0034, 0x002C, 0x0024, 0x001C, 0x0014, 0x000C, 0x0004, 0x001B,
			0x0013, 0x000B, 0x0003 };

	public static final int cp[] = { 0x000D, 0x0010, 0x000A, 0x0017, 0x0000,
			0x0004, 0x0002, 0x001B, 0x000E, 0x0005, 0x0014, 0x0009, 0x0016,
			0x0012, 0x000B, 0x0003, 0x0019, 0x0007, 0x000F, 0x0006, 0x001A,
			0x0013, 0x000C, 0x0001, 0x0028, 0x0033, 0x001E, 0x0024, 0x002E,
			0x0036, 0x001D, 0x0027, 0x0032, 0x002C, 0x0020, 0x002F, 0x002B,
			0x0030, 0x0026, 0x0037, 0x0021, 0x0034, 0x002D, 0x0029, 0x0031,
			0x0023, 0x001C, 0x001F };

	public static final int pp[] = { 0x000F, 0x0006, 0x0013, 0x0014, 0x001C,
			0x000B, 0x001B, 0x0010, 0x0000, 0x000E, 0x0016, 0x0019, 0x0004,
			0x0011, 0x001E, 0x0009, 0x0001, 0x0007, 0x0017, 0x000D, 0x001F,
			0x001A, 0x0002, 0x0008, 0x0012, 0x000C, 0x001D, 0x0005, 0x0015,
			0x000A, 0x0003, 0x0018 };

	public static final int rot[] = { 0x0001, 0x0001, 0x0002, 0x0002, 0x0002,
			0x0002, 0x0002, 0x0002, 0x0001, 0x0002, 0x0002, 0x0002, 0x0002,
			0x0002, 0x0002, 0x0001 };

	public static final int sb1[] = { 0x000E, 0x0000, 0x0004, 0x000F, 0x000D,
			0x0007, 0x0001, 0x0004, 0x0002, 0x000E, 0x000F, 0x0002, 0x000B,
			0x000D, 0x0008, 0x0001, 0x0003, 0x000A, 0x000A, 0x0006, 0x0006,
			0x000C, 0x000C, 0x000B, 0x0005, 0x0009, 0x0009, 0x0005, 0x0000,
			0x0003, 0x0007, 0x0008, 0x0004, 0x000F, 0x0001, 0x000C, 0x000E,
			0x0008, 0x0008, 0x0002, 0x000D, 0x0004, 0x0006, 0x0009, 0x0002,
			0x0001, 0x000B, 0x0007, 0x000F, 0x0005, 0x000C, 0x000B, 0x0009,
			0x0003, 0x0007, 0x000E, 0x0003, 0x000A, 0x000A, 0x0000, 0x0005,
			0x0006, 0x0000, 0x000D, };

	public static final int sb2[] = { 0x000F, 0x0003, 0x0001, 0x000D, 0x0008,
			0x0004, 0x000E, 0x0007, 0x0006, 0x000F, 0x000B, 0x0002, 0x0003,
			0x0008, 0x0004, 0x000E, 0x0009, 0x000C, 0x0007, 0x0000, 0x0002,
			0x0001, 0x000D, 0x000A, 0x000C, 0x0006, 0x0000, 0x0009, 0x0005,
			0x000B, 0x000A, 0x0005, 0x0000, 0x000D, 0x000E, 0x0008, 0x0007,
			0x000A, 0x000B, 0x0001, 0x000A, 0x0003, 0x0004, 0x000F, 0x000D,
			0x0004, 0x0001, 0x0002, 0x0005, 0x000B, 0x0008, 0x0006, 0x000C,
			0x0007, 0x0006, 0x000C, 0x0009, 0x0000, 0x0003, 0x0005, 0x0002,
			0x000E, 0x000F, 0x0009, };

	public static final int sb3[] = { 0x000A, 0x000D, 0x0000, 0x0007, 0x0009,
			0x0000, 0x000E, 0x0009, 0x0006, 0x0003, 0x0003, 0x0004, 0x000F,
			0x0006, 0x0005, 0x000A, 0x0001, 0x0002, 0x000D, 0x0008, 0x000C,
			0x0005, 0x0007, 0x000E, 0x000B, 0x000C, 0x0004, 0x000B, 0x0002,
			0x000F, 0x0008, 0x0001, 0x000D, 0x0001, 0x0006, 0x000A, 0x0004,
			0x000D, 0x0009, 0x0000, 0x0008, 0x0006, 0x000F, 0x0009, 0x0003,
			0x0008, 0x0000, 0x0007, 0x000B, 0x0004, 0x0001, 0x000F, 0x0002,
			0x000E, 0x000C, 0x0003, 0x0005, 0x000B, 0x000A, 0x0005, 0x000E,
			0x0002, 0x0007, 0x000C, };

	public static final int sb4[] = { 0x0007, 0x000D, 0x000D, 0x0008, 0x000E,
			0x000B, 0x0003, 0x0005, 0x0000, 0x0006, 0x0006, 0x000F, 0x0009,
			0x0000, 0x000A, 0x0003, 0x0001, 0x0004, 0x0002, 0x0007, 0x0008,
			0x0002, 0x0005, 0x000C, 0x000B, 0x0001, 0x000C, 0x000A, 0x0004,
			0x000E, 0x000F, 0x0009, 0x000A, 0x0003, 0x0006, 0x000F, 0x0009,
			0x0000, 0x0000, 0x0006, 0x000C, 0x000A, 0x000B, 0x0001, 0x0007,
			0x000D, 0x000D, 0x0008, 0x000F, 0x0009, 0x0001, 0x0004, 0x0003,
			0x0005, 0x000E, 0x000B, 0x0005, 0x000C, 0x0002, 0x0007, 0x0008,
			0x0002, 0x0004, 0x000E, };

	public static final int sb5[] = { 0x0002, 0x000E, 0x000C, 0x000B, 0x0004,
			0x0002, 0x0001, 0x000C, 0x0007, 0x0004, 0x000A, 0x0007, 0x000B,
			0x000D, 0x0006, 0x0001, 0x0008, 0x0005, 0x0005, 0x0000, 0x0003,
			0x000F, 0x000F, 0x000A, 0x000D, 0x0003, 0x0000, 0x0009, 0x000E,
			0x0008, 0x0009, 0x0006, 0x0004, 0x000B, 0x0002, 0x0008, 0x0001,
			0x000C, 0x000B, 0x0007, 0x000A, 0x0001, 0x000D, 0x000E, 0x0007,
			0x0002, 0x0008, 0x000D, 0x000F, 0x0006, 0x0009, 0x000F, 0x000C,
			0x0000, 0x0005, 0x0009, 0x0006, 0x000A, 0x0003, 0x0004, 0x0000,
			0x0005, 0x000E, 0x0003, };

	public static final int sb6[] = { 0x000C, 0x000A, 0x0001, 0x000F, 0x000A,
			0x0004, 0x000F, 0x0002, 0x0009, 0x0007, 0x0002, 0x000C, 0x0006,
			0x0009, 0x0008, 0x0005, 0x0000, 0x0006, 0x000D, 0x0001, 0x0003,
			0x000D, 0x0004, 0x000E, 0x000E, 0x0000, 0x0007, 0x000B, 0x0005,
			0x0003, 0x000B, 0x0008, 0x0009, 0x0004, 0x000E, 0x0003, 0x000F,
			0x0002, 0x0005, 0x000C, 0x0002, 0x0009, 0x0008, 0x0005, 0x000C,
			0x000F, 0x0003, 0x000A, 0x0007, 0x000B, 0x0000, 0x000E, 0x0004,
			0x0001, 0x000A, 0x0007, 0x0001, 0x0006, 0x000D, 0x0000, 0x000B,
			0x0008, 0x0006, 0x000D, };

	public static final int sb7[] = { 0x0004, 0x000D, 0x000B, 0x0000, 0x0002,
			0x000B, 0x000E, 0x0007, 0x000F, 0x0004, 0x0000, 0x0009, 0x0008,
			0x0001, 0x000D, 0x000A, 0x0003, 0x000E, 0x000C, 0x0003, 0x0009,
			0x0005, 0x0007, 0x000C, 0x0005, 0x0002, 0x000A, 0x000F, 0x0006,
			0x0008, 0x0001, 0x0006, 0x0001, 0x0006, 0x0004, 0x000B, 0x000B,
			0x000D, 0x000D, 0x0008, 0x000C, 0x0001, 0x0003, 0x0004, 0x0007,
			0x000A, 0x000E, 0x0007, 0x000A, 0x0009, 0x000F, 0x0005, 0x0006,
			0x0000, 0x0008, 0x000F, 0x0000, 0x000E, 0x0005, 0x0002, 0x0009,
			0x0003, 0x0002, 0x000C, };

	public static final int sb8[] = { 0x000D, 0x0001, 0x0002, 0x000F, 0x0008,
			0x000D, 0x0004, 0x0008, 0x0006, 0x000A, 0x000F, 0x0003, 0x000B,
			0x0007, 0x0001, 0x0004, 0x000A, 0x000C, 0x0009, 0x0005, 0x0003,
			0x0006, 0x000E, 0x000B, 0x0005, 0x0000, 0x0000, 0x000E, 0x000C,
			0x0009, 0x0007, 0x0002, 0x0007, 0x0002, 0x000B, 0x0001, 0x0004,
			0x000E, 0x0001, 0x0007, 0x0009, 0x0004, 0x000C, 0x000A, 0x000E,
			0x0008, 0x0002, 0x000D, 0x0000, 0x000F, 0x0006, 0x000C, 0x000A,
			0x0009, 0x000D, 0x0000, 0x000F, 0x0003, 0x0003, 0x0005, 0x0005,
			0x0006, 0x0008, 0x000B, };

	// }}}
	// {{{ constructor
	public TrippleDESEncryotor() {
		// Call super constructor.
		super();

		// Create member variables.
		CBCBuffer = 0;
		CFBBuffer = 0;
		OFBBuffer = 0;
	}

	// }}}
	// {{{ main
	public static void main(String arguments[]) {
		// Define local variables.
		TrippleDESEncryotor des = new TrippleDESEncryotor();
		String ret = "";
		try {
			// Perform tests for all DESC moDESC.
			ret = des.decMas(arguments[0]);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// }}}

	// {{{ getCBCBuffer
	public long getCBCBuffer() {
		return CBCBuffer;
	}

	// }}}
	// {{{ setCBCBuffer
	public void setCBCBuffer(int _CBCBuffer) {
		CBCBuffer = _CBCBuffer;
	}

	// }}}
	// {{{ getCFBBuffer
	public long getCFBBuffer() {
		return CFBBuffer;
	}

	// }}}
	// {{{ setCFBBuffer
	public void setCFBBuffer(int _CFBBuffer) {
		CFBBuffer = _CFBBuffer;
	}

	// }}}
	// {{{ getOFBBuffer
	public long getOFBBuffer() {
		return OFBBuffer;
	}

	// }}}
	// {{{ setOFBBuffer
	public void setOFBBuffer(int _OFBBuffer) {
		OFBBuffer = _OFBBuffer;
	}

	// }}}

	// {{{ ECBEncrypt
	public long ECBEncrypt(long data, long keys[]) {
		return rounds(ENCRYPT, data, keys);
	}

	// }}}
	// {{{ ECBDecrypt
	public long ECBDecrypt(long data, long keys[]) {
		return rounds(DECRYPT, data, keys);
	}

	// }}}
	// {{{ ECBTest
	public String decMas(String source) {
		// Define local variables.
		long[] keysE1 = new long[16];
		long[] keysD1 = new long[16];
		long[] keysE2 = new long[16];
		long[] keysD2 = new long[16];

		// long key1 = fill( new long[] { 0x23, 0xD3, 0xE1, 0x8F, 0x76, 0x9B,
		// 0x15, 0x80 } );

		long key1 = fill(new long[] { 0x6D, 0x80, 0xDC, 0xDF, 0x4C, 0x91, 0xFB,
				0x29 });
		// long key2 = fill( new long[] { 0x23, 0xD3, 0xE1, 0x8F, 0x76, 0x9B,
		// 0x15, 0x80 } );
		long key2 = fill(new long[] { 0xDE, 0x87, 0x91, 0x80, 0x5B, 0x6F, 0x13,
				0x32 });
		long ptext0 = 0;
		long ptext1 = 0;
		long[] ctextarr = new long[8];
		// String source="9AC1320D84D6A8DC";
		int j = 0;
		int i = 0;
		for (j = 0, i = 0; j < source.length(); i++) {
			// System.out.println("Key value is
			// "+Integer.parseInt(source.substring(j,j+2),16));
			ctextarr[i] = Long.parseLong(source.substring(j, j + 2), 16);
			j = j + 2;
		}
		long ctext0 = fill(ctextarr);
		long ctext1 = 0;
		long ctext2 = 0;

		// Construct the key schedules.
		keys(ENCRYPT, key1, keysE1);
		keys(DECRYPT, key1, keysD1);
		keys(ENCRYPT, key2, keysE2);
		keys(DECRYPT, key2, keysD2);

		// From the encryption, you should be expecting to get :
		//  
		// 0x4E6F772069732074 ==> 0xE5C7CDDE872BF27C
		// 0x68652074696D6520 ==> 0x43E934008C389C0F
		// 0x666F7220616C6C20 ==> 0x683788499A7C05F6
		String retstr = "";
		System.out.println("ECBDecrypt( ... )");
		System.out.println(strn(ctext0) + " ==> "
				+ strn(ptext0 = ECBDecrypt(ctext0, keysD1)));

		/***********************************************************************
		 * System.out.println( "ECBEncrypt( ... )" ); System.out.println( strn(
		 * ptext0 ) + " ==> " + strn( ctext0 = ECBEncrypt( ptext0, keysE2 ) ) );
		 * String retstr=""; System.out.println( "ECBDecrypt( ... )" );
		 * System.out.println( strn( ctext0 ) + " ==> " + strn(ptext1=
		 * ECBDecrypt( ctext0, keysD1 ) ) );
		 **********************************************************************/
		/***********************************************************************
		 * System.out.println( "ECBEncrypt( ... )" ); System.out.println( strn(
		 * ctext0 ) + " ==> " + strn( ptext0=ECBEncrypt( ctext0, keysE1 ) ) );
		 * System.out.println( "ECBDecrypt( ... )" ); System.out.println( strn(
		 * ptext0 ) + " ==> " + strn( ctext0 = ECBDecrypt( ptext0, keysD2 ) ) );
		 * String retstr=""; System.out.println( "ECBEncrypt( ... )" );
		 * System.out.println( strn( ctext0 ) + " ==> " + strn(ptext1=
		 * ECBEncrypt( ctext0, keysE1 ) ) );
		 **********************************************************************/
		String tmstr = strn(ptext0);
		retstr = tmstr.substring(2, tmstr.length());

		// retstr=Long.toHexString(ptext0);
		/** retstr=Long.toHexString(ptext0);* */
		System.out.println(retstr);
		return retstr.toUpperCase();

	}

	public String desDec(String source, String mkey) {
		// Define local variables.
		long[] keysE1 = new long[16];
		long[] keysD1 = new long[16];
		long[] keysE2 = new long[16];
		long[] keysD2 = new long[16];
		int mcnt = 0;
		long[] mkval = new long[8];
		int mkcnt = 0;
		while (mcnt < mkey.length()) {
			mkval[mkcnt] = Long.parseLong(mkey.substring(mcnt, mcnt + 2), 16);
			mkcnt++;
			mcnt += 2;
		}

		// long key1 = fill( new long[] { 0x23, 0xD3, 0xE1, 0x8F, 0x76, 0x9B,
		// 0x15, 0x80 } );
		long key1 = fill(mkval);
		// long key2 = fill( new long[] { 0x23, 0xD3, 0xE1, 0x8F, 0x76, 0x9B,
		// 0x15, 0x80 } );
		long key2 = fill(new long[] { 0xDE, 0x87, 0x91, 0x80, 0x5B, 0x6F, 0x13,
				0x32 });
		long ptext0 = 0;
		long ptext1 = 0;
		long[] ctextarr = new long[8];
		// String source="9AC1320D84D6A8DC";
		int j = 0;
		int i = 0;
		for (j = 0, i = 0; j < source.length(); i++) {
			// System.out.println("Key value is
			// "+Integer.parseInt(source.substring(j,j+2),16));
			ctextarr[i] = Long.parseLong(source.substring(j, j + 2), 16);
			j = j + 2;
		}
		long ctext0 = fill(ctextarr);
		long ctext1 = 0;
		long ctext2 = 0;

		// Construct the key schedules.
		keys(ENCRYPT, key1, keysE1);
		keys(DECRYPT, key1, keysD1);
		keys(ENCRYPT, key2, keysE2);
		keys(DECRYPT, key2, keysD2);

		// From the encryption, you should be expecting to get :
		//  
		// 0x4E6F772069732074 ==> 0xE5C7CDDE872BF27C
		// 0x68652074696D6520 ==> 0x43E934008C389C0F
		// 0x666F7220616C6C20 ==> 0x683788499A7C05F6

		System.out.println("ECBDecrypt( ... )");
		System.out.println(strn(ctext0) + " ==> "
				+ strn(ptext0 = ECBDecrypt(ctext0, keysD1)));

		// System.out.println( "ECBEncrypt( ... )" );
		// System.out.println( strn( ptext0 ) + " ==> " + strn( ptext0 =
		// ECBEncrypt( ctext0, keysE1 ) ) );

		String retstr = "";
		/***********************************************************************
		 * System.out.println( "ECBDecrypt( ... )" ); System.out.println( strn(
		 * ctext0 ) + " ==> " + strn(ptext1= ECBDecrypt( ctext0, keysD1 ) ) );
		 * System.out.println( "ECBEncrypt( ... )" ); System.out.println( strn(
		 * ctext0 ) + " ==> " + strn( ptext0=ECBEncrypt( ctext0, keysE1 ) ) );
		 * System.out.println( "ECBDecrypt( ... )" ); System.out.println( strn(
		 * ptext0 ) + " ==> " + strn( ctext0 = ECBDecrypt( ptext0, keysD2 ) ) );
		 * String retstr=""; System.out.println( "ECBEncrypt( ... )" );
		 * System.out.println( strn( ctext0 ) + " ==> " + strn(ptext1=
		 * ECBEncrypt( ctext0, keysE1 ) ) );
		 **********************************************************************/
		String tmstr = strn(ptext0);
		retstr = tmstr.substring(2, tmstr.length());

		// retstr=Long.toHexString(ptext0);
		System.out.println(retstr.toUpperCase());
		return retstr.toUpperCase();
	}

	public String desEnc(String source, String mkey) {
		// Define local variables.
		long[] keysE1 = new long[16];
		long[] keysD1 = new long[16];
		long[] keysE2 = new long[16];
		long[] keysD2 = new long[16];
		int mcnt = 0;
		long[] mkval = new long[8];
		int mkcnt = 0;
		while (mcnt < mkey.length()) {
			mkval[mkcnt] = Long.parseLong(mkey.substring(mcnt, mcnt + 2), 16);
			mkcnt++;
			mcnt += 2;
		}

		// long key1 = fill( new long[] { 0x23, 0xD3, 0xE1, 0x8F, 0x76, 0x9B,
		// 0x15, 0x80 } );
		long key1 = fill(mkval);
		// long key2 = fill( new long[] { 0x23, 0xD3, 0xE1, 0x8F, 0x76, 0x9B,
		// 0x15, 0x80 } );
		long key2 = fill(new long[] { 0xDE, 0x87, 0x91, 0x80, 0x5B, 0x6F, 0x13,
				0x32 });
		long ptext0 = 0;
		long ptext1 = 0;
		long[] ctextarr = new long[8];
		// String source="9AC1320D84D6A8DC";
		int j = 0;
		int i = 0;
		for (j = 0, i = 0; j < source.length(); i++) {
			// System.out.println("Key value is
			// "+Integer.parseInt(source.substring(j,j+2),16));
			ctextarr[i] = Long.parseLong(source.substring(j, j + 2), 16);
			j = j + 2;
		}
		long ctext0 = fill(ctextarr);
		long ctext1 = 0;
		long ctext2 = 0;

		// Construct the key schedules.
		keys(ENCRYPT, key1, keysE1);
		keys(DECRYPT, key1, keysD1);
		keys(ENCRYPT, key2, keysE2);
		keys(DECRYPT, key2, keysD2);

		// From the encryption, you should be expecting to get :
		//  
		// 0x4E6F772069732074 ==> 0xE5C7CDDE872BF27C
		// 0x68652074696D6520 ==> 0x43E934008C389C0F
		// 0x666F7220616C6C20 ==> 0x683788499A7C05F6

		// System.out.println( "ECBDecrypt( ... )" );
		// System.out.println( strn( ctext0 ) + " ==> " + strn(
		// ptext0=ECBDecrypt( ctext0, keysD1 ) ) );

		System.out.println("ECBEncrypt( ... )");
		System.out.println(strn(ptext0) + " ==> "
				+ strn(ptext0 = ECBEncrypt(ctext0, keysE1)));

		String retstr = "";
		/***********************************************************************
		 * System.out.println( "ECBDecrypt( ... )" ); System.out.println( strn(
		 * ctext0 ) + " ==> " + strn(ptext1= ECBDecrypt( ctext0, keysD1 ) ) );
		 * System.out.println( "ECBEncrypt( ... )" ); System.out.println( strn(
		 * ctext0 ) + " ==> " + strn( ptext0=ECBEncrypt( ctext0, keysE1 ) ) );
		 * System.out.println( "ECBDecrypt( ... )" ); System.out.println( strn(
		 * ptext0 ) + " ==> " + strn( ctext0 = ECBDecrypt( ptext0, keysD2 ) ) );
		 * String retstr=""; System.out.println( "ECBEncrypt( ... )" );
		 * System.out.println( strn( ctext0 ) + " ==> " + strn(ptext1=
		 * ECBEncrypt( ctext0, keysE1 ) ) );
		 **********************************************************************/

		// retstr=Long.toHexString(ptext0);
		String tmstr = strn(ptext0);
		retstr = tmstr.substring(2, tmstr.length());
		System.out.println(retstr.toUpperCase());
		return retstr.toUpperCase();
	}

	// }}}

	// {{{ CBCInit
	void CBCInit(long initVector) {
		// Setup the initialisation vector.
		CBCBuffer = initVector;
	}

	// }}}
	// {{{ CBCEncrypt
	public long CBCEncrypt(long data, long keys[]) {
		// Define local variables.
		long CBCEncrypt = 0;

		// Perform encryption and update buffer.
		CBCEncrypt = rounds(DECRYPT, CBCBuffer ^ data, keys);
		CBCBuffer = CBCEncrypt;

		// Return encrypted value.
		return CBCEncrypt;
	}

	// }}}
	// {{{ CBCDecrypt
	public long CBCDecrypt(long data, long keys[]) {
		// Define local variables.
		long CBCDecrypt = 0;

		// Perform decryption and update buffer.
		CBCDecrypt = CBCBuffer ^ rounds(DECRYPT, data, keys);
		CBCBuffer = data;

		// Return decrypted value.
		return CBCDecrypt;
	}

	// }}}
	// {{{ CBCTest
	public void CBCTest() {
		// Define local variables.
		long[] keysE = new long[16];
		long[] keysD = new long[16];

		long key = fill(new long[] { 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD,
				0xEF });
		long init = fill(new long[] { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD,
				0xEF });

		long ptext0 = fill(new long[] { 0x4E, 0x6F, 0x77, 0x20, 0x69, 0x73,
				0x20, 0x74 });
		long ptext1 = fill(new long[] { 0x68, 0x65, 0x20, 0x74, 0x69, 0x6D,
				0x65, 0x20 });
		long ptext2 = fill(new long[] { 0x66, 0x6F, 0x72, 0x20, 0x61, 0x6C,
				0x6C, 0x20 });

		long ctext0 = 0;
		long ctext1 = 0;
		long ctext2 = 0;

		// Construct the key schedules.
		keys(ENCRYPT, key, keysE);
		keys(DECRYPT, key, keysD);

		// From the encryption, you should be expecting to get :
		//  
		// 0x4E6F772069732074 ==> 0xE5C7CDDE872BF27C
		// 0x68652074696D6520 ==> 0x43E934008C389C0F
		// 0x666F7220616C6C20 ==> 0x683788499A7C05F6

		CBCInit(init);

		System.out.println("CBCEncrypt( ... )");
		System.out.println(strn(ptext0) + " ==> "
				+ strn(ctext0 = CBCEncrypt(ptext0, keysE)));
		System.out.println(strn(ptext1) + " ==> "
				+ strn(ctext1 = CBCEncrypt(ptext1, keysE)));
		System.out.println(strn(ptext2) + " ==> "
				+ strn(ctext2 = CBCEncrypt(ptext2, keysE)));

		// From the decryption, you should be expecting to get :
		//  
		// 0xE5C7CDDE872BF27C ==> 0x4E6F772069732074
		// 0x43E934008C389C0F ==> 0x68652074696D6520
		// 0x683788499A7C05F6 ==> 0x666F7220616C6C20

		CBCInit(init);

		System.out.println("CBCDecrypt( ... )");
		System.out.println(strn(ctext0) + " ==> "
				+ strn(CBCDecrypt(ctext0, keysD)));
		System.out.println(strn(ctext1) + " ==> "
				+ strn(CBCDecrypt(ctext1, keysD)));
		System.out.println(strn(ctext2) + " ==> "
				+ strn(CBCDecrypt(ctext2, keysD)));
	}

	// }}}

	// {{{ CFBInit
	void CFBInit(long initVector) {
		// Setup the initialisation vector.
		CFBBuffer = initVector;
	}

	// }}}
	// {{{ CFBEncrypt
	public long CFBEncrypt(long data, long keys[], int size) {
		// Define local variables.
		long CFBEncrypt = 0;

		// Perform encryption and update buffer.
		CFBEncrypt = data ^ (rounds(ENCRYPT, CFBBuffer, keys) >>> (64 - size));
		CFBBuffer = (CFBBuffer << size) | CFBEncrypt;

		// Return encrypted value.
		return CFBEncrypt;
	}

	// }}}
	// {{{ CFBDecrypt
	public long CFBDecrypt(long data, long keys[], int size) {
		// Define local variables.
		long CFBDecrypt = 0;

		// Perform decryption and update buffer.
		CFBDecrypt = data ^ (rounds(DECRYPT, CFBBuffer, keys) >>> (64 - size));
		CFBBuffer = (CFBBuffer << size) | data;

		// Return decrypted value.
		return CFBDecrypt;
	}

	// }}}
	// {{{ CFBTest
	public void CFBTest() {
		// Define local variables.
		long[] keysE = new long[16];
		long[] keysD = new long[16];

		long key = fill(new long[] { 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD,
				0xEF });
		long init = fill(new long[] { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD,
				0xEF });

		long ptext0 = 0x4E;
		long ptext1 = 0x6F;
		long ptext2 = 0x77;
		long ptext3 = 0x20;
		long ptext4 = 0x69;
		long ptext5 = 0x73;
		long ptext6 = 0x20;
		long ptext7 = 0x74;

		long ctext0 = 0;
		long ctext1 = 0;
		long ctext2 = 0;
		long ctext3 = 0;
		long ctext4 = 0;
		long ctext5 = 0;
		long ctext6 = 0;
		long ctext7 = 0;

		// Construct the key schedules.
		keys(ENCRYPT, key, keysE);
		keys(DECRYPT, key, keysD);

		// From the encryption, you should be expecting to get :
		//  
		// 0x000000000000004E ==> 0x00000000000000F3
		// 0x000000000000006F ==> 0x000000000000001F
		// 0x0000000000000077 ==> 0x00000000000000DA
		// 0x0000000000000020 ==> 0x0000000000000007
		// 0x0000000000000069 ==> 0x0000000000000001
		// 0x0000000000000073 ==> 0x0000000000000014
		// 0x0000000000000020 ==> 0x0000000000000062
		// 0x0000000000000074 ==> 0x00000000000000EE

		CFBInit(init);

		System.out.println("CFBEncrypt( ... )");
		System.out.println(strn(ptext0) + " ==> "
				+ strn(ctext0 = CFBEncrypt(ptext0, keysE, 8)));
		System.out.println(strn(ptext1) + " ==> "
				+ strn(ctext1 = CFBEncrypt(ptext1, keysE, 8)));
		System.out.println(strn(ptext2) + " ==> "
				+ strn(ctext2 = CFBEncrypt(ptext2, keysE, 8)));
		System.out.println(strn(ptext3) + " ==> "
				+ strn(ctext3 = CFBEncrypt(ptext3, keysE, 8)));
		System.out.println(strn(ptext4) + " ==> "
				+ strn(ctext4 = CFBEncrypt(ptext4, keysE, 8)));
		System.out.println(strn(ptext5) + " ==> "
				+ strn(ctext5 = CFBEncrypt(ptext5, keysE, 8)));
		System.out.println(strn(ptext6) + " ==> "
				+ strn(ctext6 = CFBEncrypt(ptext6, keysE, 8)));
		System.out.println(strn(ptext7) + " ==> "
				+ strn(ctext7 = CFBEncrypt(ptext7, keysE, 8)));

		// From the decryption, you should be expecting to get :
		//  
		// 0x00000000000000F3 ==> 0x000000000000004E
		// 0x000000000000001F ==> 0x000000000000006F
		// 0x00000000000000DA ==> 0x0000000000000077
		// 0x0000000000000007 ==> 0x0000000000000020
		// 0x0000000000000001 ==> 0x0000000000000069
		// 0x0000000000000014 ==> 0x0000000000000073
		// 0x0000000000000062 ==> 0x0000000000000020
		// 0x00000000000000EE ==> 0x0000000000000074

		CFBInit(init);

		System.out.println("CFBEncrypt( ... )");
		System.out.println(strn(ctext0) + " ==> "
				+ strn(CFBDecrypt(ctext0, keysE, 8)));
		System.out.println(strn(ctext1) + " ==> "
				+ strn(CFBDecrypt(ctext1, keysE, 8)));
		System.out.println(strn(ctext2) + " ==> "
				+ strn(CFBDecrypt(ctext2, keysE, 8)));
		System.out.println(strn(ctext3) + " ==> "
				+ strn(CFBDecrypt(ctext3, keysE, 8)));
		System.out.println(strn(ctext4) + " ==> "
				+ strn(CFBDecrypt(ctext4, keysE, 8)));
		System.out.println(strn(ctext5) + " ==> "
				+ strn(CFBDecrypt(ctext5, keysE, 8)));
		System.out.println(strn(ctext6) + " ==> "
				+ strn(CFBDecrypt(ctext6, keysE, 8)));
		System.out.println(strn(ctext7) + " ==> "
				+ strn(CFBDecrypt(ctext7, keysE, 8)));
	}

	// }}}

	// {{{ OFBInit
	void OFBInit(long initVector) {
		// Setup the initialisation vector.
		OFBBuffer = initVector;
	}

	// }}}
	// {{{ OFBEncrypt
	public long OFBEncrypt(long data, long keys[], int size) {
		// Define local variables.
		long OFBEncrypt = 0;

		// Perform encryption and update buffer.
		OFBEncrypt = rounds(ENCRYPT, OFBBuffer, keys) >>> (64 - size);
		OFBBuffer = (OFBBuffer << size) | OFBEncrypt;

		// Return encrypted value.
		return data ^ OFBEncrypt;
	}

	// }}}
	// {{{ OFBDecrypt
	public long OFBDecrypt(long data, long keys[], int size) {
		// Define local variables.
		long OFBDecrypt = 0;

		// Perform decryption and update buffer.
		OFBDecrypt = rounds(DECRYPT, OFBBuffer, keys) >>> (64 - size);
		OFBBuffer = (OFBBuffer << size) | OFBDecrypt;

		// Return decrypted value.
		return data ^ OFBDecrypt;
	}

	// }}}
	// {{{ OFBTest
	void OFBTest() {
		// Define local variables.
		long[] keysE = new long[16];
		long[] keysD = new long[16];

		long key = fill(new long[] { 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD,
				0xEF });
		long init = fill(new long[] { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD,
				0xEF });

		long ptext0 = 0x4E;
		long ptext1 = 0x6F;
		long ptext2 = 0x77;
		long ptext3 = 0x20;
		long ptext4 = 0x69;
		long ptext5 = 0x73;
		long ptext6 = 0x20;
		long ptext7 = 0x74;

		long ctext0 = 0;
		long ctext1 = 0;
		long ctext2 = 0;
		long ctext3 = 0;
		long ctext4 = 0;
		long ctext5 = 0;
		long ctext6 = 0;
		long ctext7 = 0;

		// Construct the key schedules.
		keys(ENCRYPT, key, keysE);
		keys(DECRYPT, key, keysD);

		// From the encryption, you should be expecting to get :
		//  
		// 0x000000000000004E ==> 0x00000000000000F3
		// 0x000000000000006F ==> 0x000000000000004A
		// 0x0000000000000077 ==> 0x0000000000000028
		// 0x0000000000000020 ==> 0x0000000000000050
		// 0x0000000000000069 ==> 0x00000000000000C9
		// 0x0000000000000073 ==> 0x00000000000000C6
		// 0x0000000000000020 ==> 0x0000000000000049
		// 0x0000000000000074 ==> 0x0000000000000085

		OFBInit(init);

		System.out.println("OFBEncrypt( ... )");
		System.out.println(strn(ptext0) + " ==> "
				+ strn(ctext0 = OFBEncrypt(ptext0, keysE, 8)));
		System.out.println(strn(ptext1) + " ==> "
				+ strn(ctext1 = OFBEncrypt(ptext1, keysE, 8)));
		System.out.println(strn(ptext2) + " ==> "
				+ strn(ctext2 = OFBEncrypt(ptext2, keysE, 8)));
		System.out.println(strn(ptext3) + " ==> "
				+ strn(ctext3 = OFBEncrypt(ptext3, keysE, 8)));
		System.out.println(strn(ptext4) + " ==> "
				+ strn(ctext4 = OFBEncrypt(ptext4, keysE, 8)));
		System.out.println(strn(ptext5) + " ==> "
				+ strn(ctext5 = OFBEncrypt(ptext5, keysE, 8)));
		System.out.println(strn(ptext6) + " ==> "
				+ strn(ctext6 = OFBEncrypt(ptext6, keysE, 8)));
		System.out.println(strn(ptext7) + " ==> "
				+ strn(ctext7 = OFBEncrypt(ptext7, keysE, 8)));

		// From the decryption, you should be expecting to get :
		//  
		// 0x00000000000000F3 ==> 0x000000000000004E
		// 0x000000000000004A ==> 0x000000000000006F
		// 0x0000000000000028 ==> 0x0000000000000077
		// 0x0000000000000050 ==> 0x0000000000000020
		// 0x00000000000000C9 ==> 0x0000000000000069
		// 0x00000000000000C6 ==> 0x0000000000000073
		// 0x0000000000000049 ==> 0x0000000000000020
		// 0x0000000000000085 ==> 0x0000000000000074

		OFBInit(init);

		System.out.println("CFBEncrypt( ... )");
		System.out.println(strn(ctext0) + " ==> "
				+ strn(OFBDecrypt(ctext0, keysE, 8)));
		System.out.println(strn(ctext1) + " ==> "
				+ strn(OFBDecrypt(ctext1, keysE, 8)));
		System.out.println(strn(ctext2) + " ==> "
				+ strn(OFBDecrypt(ctext2, keysE, 8)));
		System.out.println(strn(ctext3) + " ==> "
				+ strn(OFBDecrypt(ctext3, keysE, 8)));
		System.out.println(strn(ctext4) + " ==> "
				+ strn(OFBDecrypt(ctext4, keysE, 8)));
		System.out.println(strn(ctext5) + " ==> "
				+ strn(OFBDecrypt(ctext5, keysE, 8)));
		System.out.println(strn(ctext6) + " ==> "
				+ strn(OFBDecrypt(ctext6, keysE, 8)));
		System.out.println(strn(ctext7) + " ==> "
				+ strn(OFBDecrypt(ctext7, keysE, 8)));
	}

	// }}}

	// {{{ rounds
	public long rounds(int mode, long data, long[] keys) {
		// Define local variables.
		long dataL = 0;
		long dataR = 0;

		long prevL = 0;
		long prevR = 0;

		int round = 0;

		// Perform the initial permutation.
		data = perm(data, 64, ip, 64);

		// Extract the left and right hand sides of the data block. Note that
		// this
		// yeilds two 32bit sub-blocks.
		prevL = (data >> 32) & 0xFFFFFFFFL;
		prevR = (data >> 0) & 0xFFFFFFFFL;

		// Perform sixteen rounds of expansion and sbox operations to calculate
		// the
		// final left and right hand portions of the data block.
		for (round = 0; round < 16; round++) {
			// Swap over the left and right hand sizes from the previous
			// iteration and
			// perform the expansion permutation on the right hand side. Note
			// that
			// this yeilds a 48bit expanded right hand side of the sub-block.
			dataL = prevR;
			dataR = perm(prevR, 32, ep, 48);

			// XOR the right hand side of the data block with the key for this
			// round.
			dataR ^= keys[round];

			// Perform the sbox substitutions by splitting the 48bit right hand
			// sub-block
			// into 6bit parts and using them to index the sboxes. Each sbox
			// index gives
			// a 4bit resultant value which all make up the new 32bit right hand
			// side.
			// Note that the sboxes have been rearranged so that they can be
			// indexed like
			// an array rather than using the wierd bit concatination scheme.
			dataR = sb1[(int) ((dataR >> 42) & 0x003F)] << 28
					| sb2[(int) ((dataR >> 36) & 0x003F)] << 24
					| sb3[(int) ((dataR >> 30) & 0x003F)] << 20
					| sb4[(int) ((dataR >> 24) & 0x003F)] << 16
					| sb5[(int) ((dataR >> 18) & 0x003F)] << 12
					| sb6[(int) ((dataR >> 12) & 0x003F)] << 8
					| sb7[(int) ((dataR >> 6) & 0x003F)] << 4
					| sb8[(int) ((dataR >> 0) & 0x003F)] << 0;

			// Perform the pbox permutation which permutes the 32bit right hand
			// sub-block
			// to give another 32bit sub-block.
			dataR = perm(dataR, 32, pp, 32);

			// XOR the right hand side of the block with the previous left hand
			// side to
			// make the final value for the new right hand side.
			dataR = prevL ^ dataR;

			// Store the previous values of the left and right hand blocks.
			prevL = dataL;
			prevR = dataR;
		}

		// Perform the initial permutation and return the value as the encrypted
		// block.
		// Note that the left and right hand sub-blocks are swapped over so as
		// to counter
		// act the swapping performed by round 16.
		return perm((dataR << 32) | (dataL << 0), 64, fp, 64);
	}

	// }}}
	// {{{ keys
	public void keys(int mode, long key, long[] keys) {
		// Define local variables.
		long keyP = 0;

		long keyL = 0;
		long keyR = 0;

		int round = 0;
		int index = 0;

		// Permute the key. Note that as well as the permutation operation, this
		// crunches the 64bit input key into a 56bit input key by ignoring the
		// least
		// significant bit [the parity bit] of each 8bit key chunk.
		keyP = perm(key, 64, kp, 56);

		// Extract the left and right sides of the key. Note that this yields
		// two
		// 28bit sub-keys.
		keyL = (keyP >> 28) & 0x0FFFFFFFL;
		keyR = (keyP >> 0) & 0x0FFFFFFFL;

		// Calculate all 16 of the sub-keys from the permuted input key by
		// rotating
		// each side by the right amount and applying a second permutation.
		for (round = 0; round < 16; round++) {
			// Calculate the index based on which mode we are calculating the
			// keys
			// for. Encryption does keys in normal order, decryption in reverse
			// order.
			switch (mode) {
			case ENCRYPT: {
				// Calculate the key index for this round.
				index = round;

				// Break from switch.
				break;
			}

			case DECRYPT: {
				// Calculate the key index for this round.
				index = 16 - 1 - round;

				// Break from switch.
				break;
			}

			default: {
				// Break from switch.
				break;
			}
			}

			// Rotate the left and right sides of the key to the left by
			// distance
			// specified by the rotation table.
			keyL = rotl(keyL, 28, rot[round]) & 0x0FFFFFFFL;
			keyR = rotl(keyR, 28, rot[round]) & 0x0FFFFFFFL;

			// Permute the key. Note that we well as the permutation operation,
			// this
			// crunches the 56bit sub-key, constructed from the two 28bit
			// halves, into
			// a 48bit output key which is ready for use.
			keys[index] = perm((keyL << 28) | (keyR << 0), 56, cp, 48);
		}
	}

	// }}}

	// {{{ strn
	private String strn(long value) {
		// Define local variables.
		String pad = "0000000000000000";
		String string = "";

		// Construct the string.
		string = padd(Integer.toString((int) (value >> 56) & 0xFF, 16))
				+ padd(Integer.toString((int) (value >> 48) & 0xFF, 16))
				+ padd(Integer.toString((int) (value >> 40) & 0xFF, 16))
				+ padd(Integer.toString((int) (value >> 32) & 0xFF, 16))
				+ padd(Integer.toString((int) (value >> 24) & 0xFF, 16))
				+ padd(Integer.toString((int) (value >> 16) & 0xFF, 16))
				+ padd(Integer.toString((int) (value >> 8) & 0xFF, 16))
				+ padd(Integer.toString((int) (value >> 0) & 0xFF, 16));

		// Convert the whole string to upper case.
		string = string.toUpperCase();

		// Return the padded value.
		return "0x" + string;
	}

	// }}}
	// {{{ padd
	private String padd(String string) {
		// Check if the string needs padding.
		if ((string.length() % 2) != 0) {
			string = "0" + string;
		}

		// Return the new string.
		return string;
	}

	// }}}

	// {{{ fill
	private long fill(long[] values) {
		// Fill the output value with the input parts.
		return values[0] << 56 | values[1] << 48 | values[2] << 40
				| values[3] << 32 | values[4] << 24 | values[5] << 16
				| values[6] << 8 | values[7] << 0;
	}

	// }}}
	// {{{ perm
	private long perm(long value, int valueSize, int[] map, int mapSize) {
		// Define local variables.
		long srcBit = 0;
		long dstBit = 0;

		long result = 0;

		int src = 0;
		int dst = 0;

		// Loop through the whole permutation map.
		for (dst = 0; dst < mapSize; dst++) {
			// Work out the source bit location from the destination bit
			// location
			// by indexing the permutation map.
			src = map[dst];

			// Extract the source bit from the value and construct a mask by
			// moving
			// the source bit to the destination bit location.
			srcBit = ((long) (1) << (valueSize - 1 - src) & value) != 0 ? 1 : 0;
			dstBit = srcBit << (mapSize - 1 - dst);

			// Update the result using the mask.
			result |= dstBit;
		}

		// Return the permuted result.
		return result;
	}

	// }}}
	// {{{ rotl
	private long rotl(long value, int valueSize, int distance) {
		// Define local variables.
		long srcBit = 0;
		long dstBit = 0;

		long result = 0;

		int round = 0;

		// Start the rotated value at the same as the incomming value.
		result = value;

		// Loop through the whole rotation distance.
		for (round = 0; round < distance; round++) {
			// Extract the source bit from the rotated value.
			srcBit = ((long) (1) << (valueSize - 1) & result) != 0 ? 1 : 0;

			// Update the result by shifting left and inserting the source bit.
			result = (result << 1) | srcBit;
		}

		// Return the rotated result.
		return result;
	}
	// }}}
}
