package com.cca.ob.secure;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Base64.Encoder;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.core.io.ClassPathResource;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.ResourceUtils;

import com.cca.ob.secure.cert.PublicKeyExtractor;

public class RSAPKCS1CryptoUtil {
	private Cipher cipher;

	public RSAPKCS1CryptoUtil() throws NoSuchAlgorithmException, NoSuchPaddingException {
		this.cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
	}

	// https://docs.oracle.com/javase/8/docs/api/java/security/spec/PKCS8EncodedKeySpec.html
	public PrivateKey getPrivate(String filename) throws Exception {
		byte[] keyBytes=readResourceFile(filename);
		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePrivate(spec);
	}

	// https://docs.oracle.com/javase/8/docs/api/java/security/spec/X509EncodedKeySpec.html
	public PublicKey getPublic(String filename) throws Exception {
		//byte[] keyBytes = Files.readAllBytes(new File(filename).toPath());
		byte[] keyBytes=readResourceFile(filename);
		X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePublic(spec);
	}
	
	public PublicKey getPublicFromCertificate(String pathToCertificate) throws Exception {
		/*
		 * File certificateFile = new File(
		 * getClass().getClassLoader().getResource(pathToCertificate).getFile() );
		 */
		InputStream stream=getResourceAsStream(pathToCertificate);
		return PublicKeyExtractor.getPublicKey(stream);
	}

	private byte[] readResourceFile(String filename) throws Exception{
		//File privateKeyFile = new File(
		//		getClass().getClassLoader().getResource(filename).getFile()
		//	);
		InputStream stream=getResourceAsStream(filename);
		    return FileCopyUtils.copyToByteArray(stream);
		    
	}
	
	private InputStream getResourceAsStream(String filename) throws IOException {
		return new ClassPathResource(
				filename).getInputStream();
	}
	

	public String encryptText(String msg, PublicKey key)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			UnsupportedEncodingException, IllegalBlockSizeException,
			BadPaddingException, InvalidKeyException {
		this.cipher.init(Cipher.ENCRYPT_MODE, key);
		//return Base64.encodeBase64String(cipher.doFinal(msg.getBytes("UTF-8")));
		Encoder encoder=Base64.getEncoder();
        return encoder.encodeToString(cipher.doFinal(msg.getBytes("UTF-8")));
	}

	public String decryptText(String msg, PrivateKey key)
			throws InvalidKeyException, UnsupportedEncodingException,
			IllegalBlockSizeException, BadPaddingException {
		this.cipher.init(Cipher.DECRYPT_MODE, key);
		//return new String(cipher.doFinal(Base64.decodeBase64(msg)), "UTF-8");
		return new String(cipher.doFinal(Base64.getDecoder().decode(msg)), "UTF-8");
	}

	public String decryptStream(byte[] msg, PrivateKey key)
			throws InvalidKeyException, UnsupportedEncodingException,
			IllegalBlockSizeException, BadPaddingException {
		this.cipher.init(Cipher.DECRYPT_MODE, key);
		return new String(cipher.doFinal(Base64.getDecoder().decode(msg)), "UTF-8");
	}

	public static void main(String[] args) throws Exception {
		
		

	}
}
