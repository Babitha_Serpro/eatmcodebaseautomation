package com.cca.ob.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cca.ob.constants.OBConstants;
import com.cca.ob.dto.CCACallBackResponseDTO;
import com.cca.ob.dto.CustomerInfo;
import com.cca.ob.dto.TransactionDetails;
import com.cca.ob.enums.GatewayStatusMessage;
import com.cca.ob.repo.PGTransactionRepo;
import com.cca.ob.repo.RegistrationRepository;
import com.cca.ob.service.CCACallBackService;
import com.cca.ob.service.SNSService;
import com.cca.ob.service.TokenGeneratorService;
import com.cca.ob.service.pg.OrderStatusConfirmationService;
import com.cca.ob.util.TokenUtils;

@Controller
@RequestMapping("/api/v1")
public class CCACallBackController {
	@Autowired
	private CCACallBackService cCACallBackService;
	private static Logger logger=LogManager.getLogger(CCACallBackController.class);
	
	@Autowired
	private OrderStatusConfirmationService orderStatusConfirmationService;
	@Autowired
	private TokenGeneratorService tokenGenerator;
	@Autowired
	private PGTransactionRepo pGTransactionRepoImpl;
	@Autowired
	private SNSService sNSService;
	@Autowired
	private RegistrationRepository registrationRepositoryImpl;
	
	@Value("${token.message.prefix1}")
	private String smsPref1;
	@Value("${token.message.prefix2}")
	private String smsPref2;
	@Value("${token.message.suffix}")
	private String smsSuffix;	
	
	@Value("${otp.message.senderid}")
	private String otpMsgSenderId;
	
	@PostMapping(path = "/PG/Redirect")
	@ResponseBody
	public String  redirectHandler(@RequestParam Map<String, String> body, HttpServletRequest request) {
		TransactionDetails tDetails=cCACallBackService.calledBack(body,OBConstants.REDIRECT_URL);
		String status=tDetails.getOrderStatus();
		long obTransactId=tDetails.getTransactionId();
		double amount=tDetails.getAmount();
		
		logger.debug("Status is "+status+"\t Enum is ="+GatewayStatusMessage.getStatusMessage(status));
		
		
		orderStatusConfirmationService.confirmStatus(Long.valueOf(obTransactId));
		// If requested Amount and Currency does not match the
		// Response AMount and Currency
		
		if(status!=null && "success".equalsIgnoreCase(GatewayStatusMessage.getStatusMessage(status).getStatus())) {
			//resp.setOrderSuccess(true);
			//System.out.println("Last order "+obTransactId+" was success");
			if(tDetails.isPennyTransaction()) {
				// Initiate a reversal ifPennyTransacted
			}else {
				long token=tokenGenerator.generateToken(obTransactId);
				sendSMS(obTransactId, token, amount);
			}
		}else {
			//resp.setOrderSuccess(false);
		}
		
		return "Please Wait While You Are Being Redirected...";
	}
	
	@PostMapping(path = "/PG/Cancel")
	public ResponseEntity<CCACallBackResponseDTO > cancelHandler(@RequestParam Map<String, String> body, HttpServletRequest request) {
		CCACallBackResponseDTO resp=new CCACallBackResponseDTO();
		TransactionDetails parser=cCACallBackService.calledBack(body,OBConstants.CANCEL_URL);
		
		/*
		 * String status=parser.getValue(CCAvenueResponseParser.ORDER_STATUS);
		 * logger.debug("Status is "+status+"\t Enum is ="+GatewayStatusMessage.
		 * getStatusMessage(status));
		 * resp.setStatusMessage(GatewayStatusMessage.getStatusMessage(status).getStatus
		 * ());
		 * resp.setBankReference(parser.getValue(CCAvenueResponseParser.BANK_REF_NO));
		 * resp.setObTransactionReference(parser.getValue(CCAvenueResponseParser.
		 * ORDER_ID));
		 */
		return ResponseEntity.ok(resp);
	}
	
	private void sendSMS(long transId,long token,double amount) {
		new Thread(()->{
			
			CustomerInfo info=pGTransactionRepoImpl.getCustomerForTransaction(transId);
			if(info!=null && info.getCountryCode()!=null && info.getmNo()!=null) {
				String qualifiedPhone=info.getCountryCode()+info.getmNo();
				StringBuilder smsMsg=new StringBuilder();
				smsMsg.append(smsPref1)
				.append(amount)
				.append(" ")
				.append(smsPref2)
				.append(TokenUtils.getZeroPaddedToken(token))
				.append(" ")
				.append(smsSuffix);
				
				String confirmationId=sNSService.sendSMSMessage(qualifiedPhone,smsMsg.toString(),otpMsgSenderId);
				registrationRepositoryImpl.insertSMSConfirmation(qualifiedPhone, "AWS_SNS", confirmationId);
			}else {
				logger.error("Token could not be sent as SMS");
			}
			
			
		}) .start();
	}
}
